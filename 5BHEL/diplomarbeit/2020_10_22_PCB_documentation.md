# PCB Dokumentation

## Schaltung

Die Microcontroller Schaltung basiert auf einem Arduino Nano. 

### Pinout

| Funktion             | Arduino Pin | ATMega Pin |
| -------------------- | ----------- | ---------- |
| Ventil 1             | 2           | PD2        |
| Ventil 2             | 3           | PD3        |
| Ventil 3             | 4           | PD4        |
| Ventil 4             | 5           | PD5        |
| Ventil 5             | 6           | PD6        |
| Ventil 6             | 7           | PD7        |
| Ventil 7             | 8           | PB0        |
| Ventil 8             | 9           | PB1        |
| Ventil 9             | 10          | PB2        |
| Ventil 10            | 14/A0       | PC0        |
| Ventil 11            | 15/A1       | PC1        |
| Ventil 12            | 16/A2       | PC2        |
| Motor Relais         | 18/A4       | PC4        |
| Ultraschall Sensor 1 | A6          | ADC6       |
| Ultraschall Sensor 2 | A7          | ADC7       |
| Ultraschall Sensor 3 | A5/19       | PC5        |
| Druck Sensor         | A3/17       | PC3        |















## Layout

### Top

![top](images/top.png)

### Bottom

![bottom](images/bottom.png)

## BOM

| Qty  | Value  | Device         | Package    | Parts                                                        |
| ---- | ------ | -------------- | ---------- | ------------------------------------------------------------ |
| 1    |        | FAN            | FAN        | FAN                                                          |
| 1    |        | Pinheader      | 2X20       | JP1                                                          |
| 1    |        | Pinheader      | 2X03       | JP2                                                          |
| 13   | 100Ω   | Resistor       | R0805      | R1, R6, R8, R10, R12, R14, R16, R18, R20, R22, R24, R26, R30 |
| 5    | 100nF  | Capacitor      | C0805      | C1, C2, C4, C5, C9                                           |
| 1    | 10kΩ   | Resistor       | R0805      | R2                                                           |
| 1    | 10µF   | Capacitor Pol  | E5-4       | C6                                                           |
| 1    | 16MHz  | Crystal HC-49S | 11.4X4.5   | X1                                                           |
| 12   |        | 1N4148         | SOD123FL   | D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12            |
| 9    | 1kΩ    | Resistor       | R0805      | R3, R4, R31, R32, R33, R34, R35, R36, R37                    |
| 1    | 1,5kΩ  | Resistor       | R0805      | R38                                                          |
| 13   | 200Ω   | Resistor       | R0805      | R5, R7, R9, R11, R13, R15, R17, R19, R21, R23, R25, R27, R29 |
| 2    | 20pF   | Capacitor      | C0805      | C7, C8                                                       |
| 1    |        | 22-27-2021-02  | 6410-02    | J1                                                           |
| 5    |        | 22-27-2041-04  | 6410-04    | J2, J3, J4, J5, J11                                          |
| 4    |        | 22-27-2061-06  | 6410-06    | J7, J8, J9, J10                                              |
| 1    |        | 22-27-2081-08  | 6410-08    | J6                                                           |
| 1    | 680Ω   | Resistor       | R0805      | R28                                                          |
| 1    |        | ATMEGA328P-AU  | QFP32      | U1                                                           |
| 1    | Green  | LED            | 1206       | D28                                                          |
| 4    |        | MOUNT-HOLE2.8  | 2,8        | H1, H2, H3, H4                                               |
| 6    |        | MOUNT-HOLE3.2  | 3,2        | H5, H6, H7, H8, H9, H10                                      |
| 1    | Orange | LED            | 1206       | D25                                                          |
| 1    | Red    | LED            | 1206       | D26                                                          |
| 12   |        | SI2300DS       | SOT23      | FET1, FET2, FET3, FET4, FET5, FET6, FET7, FET8, FET9, FET10, FET11, FET12 |
| 1    |        | SMD_BUTTON     | SMD_BUTTON | S1                                                           |
| 1    |        | TXB0104        | SOIC14     | U2                                                           |
| 13   | Yellow | LED            | 1206       | D13, D14, D15, D16, D17, D18, D19, D20, D21, D22, D23, D24, D27 |


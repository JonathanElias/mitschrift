# Diplomarbeit HAT Pinout

| Funktion     | Pin on Arduino Nano |
| ------------ | ------------------- |
| Ventil 1     | D2                  |
| Ventil 2     | D3                  |
| Ventil 3     | D4                  |
| Ventil 4     | D5                  |
| Ventil 5     | D6                  |
| Ventil 6     | D7                  |
| Ventil 7     | D8                  |
| Ventil 8     | D9                  |
| Ventil 9     | D10                 |
| Ventil 10    | D14 / A1            |
| Ventil 11    | D15 / A2            |
| Ventil 12    | D16 / A3            |
| Sensor 1     | A6                  |
| Sensor 2     | A7                  |
| Sensor 3     | A5 / D19            |
| Druck Sensor | A3 / D17            |
| Motor Relais | D18 / A4            |


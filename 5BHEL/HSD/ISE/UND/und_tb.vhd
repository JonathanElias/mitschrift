--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:13:19 09/29/2020
-- Design Name:   
-- Module Name:   /home/jonathan/Documents/schule/5BHEL/HSD/ISE/UND/und_tb.vhd
-- Project Name:  UND
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: und
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY und_tb IS
END und_tb;
 
ARCHITECTURE behavior OF und_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT und
    PORT(
         a_i : IN  std_ulogic;
         b_i : IN  std_ulogic;
         q_o : OUT  std_ulogic
        );
    END COMPONENT;
    

   --Inputs
   signal a_i : std_ulogic := '0';
   signal b_i : std_ulogic := '0';

 	--Outputs
   signal q_o : std_ulogic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: und PORT MAP (
          a_i => a_i,
          b_i => b_i,
          q_o => q_o
        );

	a_i <= '0' after 0 ns,
			 '1' after 10 ns,
			 '0' after 20 ns,
			 '1' after 30 ns,
			 '0' after 40 ns;
			
	b_i <= '0' after 0 ns,
			 '1' after 19 ns;
END;

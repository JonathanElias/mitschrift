## DIC 2020 Einführung

## Was muss ich zur DIC Matura 2021 können?

### Synchrone und Asynchrone Schaltwerke

Unterschied synchron/asynchron: Zähler, Frequenzteiler, State Machine

### Prozessorschnittstellen

- I2C
- SPI
- 1-Wire
- RS232
- RS485
- CAN-Bus
- Automotive-Bus

### Peripheriebausteine

- Portexpander PCA9555
- Temperatursensor TMP100/101

### Echtzeitprogrammierung

- Funktion Interrupt
- Reationszeit des µC auf Interrupt
- Polling / Interrupt
- TMP100/101 mit Interrupt-Ausgang

### Programmierung von µC Peripheriebausteinen

In Datenblättern ist z.T. Code vorhanden

### Aufbau und Arbeitsweise von Mikrocontroller

- Architektur
- ALU
- RISC / CISC
- Pipelining
- Leistungssteigerung
- Multithreading
- ...

### Hardwarebeschreibungssprachen

- Grundbegriffe
- Entwurf (komplexer) digitaler Systeme mit VHDL/Verilog

### digitale Signalverarbeitung

- dig. Filter FIR IIR

### EMV-konformer Aufbau



## Benotungskriterien DIC

- 1 Test 50%
- Referat + Mitarbeit 50%
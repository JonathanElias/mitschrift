# HDMI

# Basics

[Wikipedia HDMI](https://en.wikipedia.org/wiki/HDMI)

- Dezember 2002 designed
- Audio und Video Interface (gibt aber auch andere Sachen die durch können)
- basiert auf EIA/CEA-861
- Elektrisch Kompatibel zu DVI
  - Adapter gehen in beide Richtungen
  - Spezielle Funktionen von HDMI (zB Audio, HDCP (High bandwidth Digital Content Protection)) sind mit DVI nicht verfügbar
- HDCP High-bandwidth Digital Content Protection
  - von Intel erstellt :roll_eyes:
  - kann das Signal verschlüsseln
- Stecker
  - Type A: 19 Pins
  - Dual-Link (B): für höhere Auflösungen gedacht, wird aber nicht verwendet
  - Mini HDMI (C): 19 Pins für mobile Geräte gedacht (wird zB im Raspberry Pi Zero verwendet)
  - Micro HDMI (D): 19 Pins ca. so groß wie micro USB (wird zB im Raspberry Pi 4 verwendet)
  - Type E: Automotive; hat eine Verrigelungslasche; gegen Staub und Feuchtigkeit abgedichtet
  - USB-C: HDMI Alternate Mode: ist nach DP Alt mode gekommen

## Wie funktioniert HDMI?

[Video](https://www.youtube.com/watch?v=5acgSK0kWTE&list=WL&index=8)

## Was wird übertragen?

[https://blogs.synopsys.com/vip-central/2018/03/20/hdmi-2-1-how-it-became-the-most-popular-display-interface/](https://blogs.synopsys.com/vip-central/2018/03/20/hdmi-2-1-how-it-became-the-most-popular-display-interface/)

![2020_10_13_HDMI_communication_channels](images/2020_10_13_HDMI_communication_channels.jpg)

### DDC Display Data Channel

[https://en.wikipedia.org/wiki/Display_Data_Channel](https://en.wikipedia.org/wiki/Display_Data_Channel)

Erste Version von VGA hat keine Kommunikation zwischen Monitor und Grafikkarte bis auf verschiedene Pins auf Ground zu setzen, war aber nicht standartisiert
DDC2: Basiert auf I²C ist aber nur unidirektional
DDC2Ab: bidirektional mit Support für Mäuse und Tastaturen
DDC2B+ & DDC2Bi: wie DDC2Ab aber ohne Maus und Tastatur Support; wird in DVI und HDMI verwendet

Es gibt auch eine DDC1 Version, die aber nie populär wurde

#### EDID Extended Display Identification Data

[https://en.wikipedia.org/wiki/Extended_Display_Identification_Data](https://en.wikipedia.org/wiki/Extended_Display_Identification_Data)

Was wird vom Monitor zur Grafikkarte übertragen? EDID
128Bytes die über DDC an die Grafikkarte übertragen werden

### TMDS Transistion Minimized Differential Signaling

![Blanking](![https://forums.parallax.com/discussion/download/128579/HdmiFrame.PNG](https://forums.parallax.com/discussion/download/128579/HdmiFrame.PNG)

[TMDS](https://en.wikipedia.org/wiki/Transition-minimized_differential_signaling) 

![TMDS](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Schematic_TMDS_link.svg/1024px-Schematic_TMDS_link.svg.png)

TMDS ist ein Algorithmus der Elektromagnetische Störung reduziert und so wird es höhere Kabellängen zuzulassen. Jeweils acht Bit die für einen Pixel die Intensität einer Farbe darstellen werden in einem zweistufigen Verfahren in 10 Bit encoded. Das nennt man 8b/10b encoding. Im ersten schritt wird, abhängig von der Anzahl der Einsen in den 8 Bits, ein Bit mit dem vorigen XOR beziehungsweise XNOR verknüpft. Ein neuntes Bit wird verwendet um zu speichern, ob XNOR oder XOR angewendet wurde. Das reduziert die Anzahl der Übergänge zwischen 1 und 0. Das zweite Ziel von TMDS ist es, die Anzahl der Einsen und Nullen gleich zu halten. Das nennt man Gleichspannungsfreiheit und wenn das nicht gegeben ist könnte es sein, dass zum Beispiel eine Eins als Null interpretiert wird. Über die TMDS Leitungen wird im nicht-sichtbaren Bereiche auch HSYNC und VSYNC übertragen.  Des weiteren wird auch Audio über diese Leitungen digital übertragen

### CEC Consmer Electronics Control

Ermöglicht das steuern von einem Gerät über ein anderes Gerät. ZB: Playstation mit TV Fernbedienung steuern.

### ARC Audio Return Channel

Soll Audiokabel zwischen Anzeigegerät und Lautsprecher ersetzten. zB: zwischen TV und Soundbar. Audio geht in die andere Richtung wie Video 

### HEC HDMI Ethernet Channel

Ermöglicht Ethernet zwischen Geräten bis zu Fast Ethernet (100Mb/s). Das bedeutet, dass zum Beispiel nur der TV ans Internet verbunden werden muss und die anderen Geräte die an den TV angeschlossen sind, haben auch Internet Zugang.

# Zukunft von HDMI

Wegen Funktionen wie CEC, ARC wird HDMI weiterhin der Standard für Konsolen und Fernseher bleiben. Bei Desktop PCs wird glaube ich Displayport eine größere Rolle einnehmen. Grafikkarten werden heute meist mit 3 DP Anschlüssen und nur einem HDMI anschluss gebaut und DP hat einige Vorteile gegen HDMI (USB, bis zu 16k Auflösung, kann HDMI ohne aktive Adapter ausgeben).  Bei Laptops werden HDMI Stecker immer öfter duch USB-C Stecker ersetzt, und obwohl es einen USB-C HDMI Alternate Mode gibt, wird normalerweise der USB-C Displayport Alternate Mode eingesetzt.
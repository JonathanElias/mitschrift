# Realismus

- Was man im 19. Jh oft unter Wirklichkeit verstanden hat
- grenzten sich von jeglichem übernatürlichen ab
- Positivismus: tatsächlich/belegt

# Naturalismus

- große Fortschritte in der Wissenschaft
- radikaler als Realismus
- Hippolyte Taine: der Mensch ist determiniert von Vererbung, Milieu und historischer Situtation
- Demographie: beschäftigt sich mit der Entwicklung des Volkes
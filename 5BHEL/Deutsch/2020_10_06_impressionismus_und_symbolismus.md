# Impressionismus und Symbolismus (1883-1923)

- Momentaufnahme
  - Schnelle Pinselstriche
- Ablehnung des Naturalismus (1:1 Darstellung wird abgelehnt)
- Zeitalter der Jahrhunderts wende nennt man auch Decadence
- Darstellung subjektiver Eindrücke ist wichtig
- Interesse auf:
  - individuelle Eindrücke
    - innerer Monolog
  - detaillierte Sinneswahrnehmung
  - zufällige Begegnungen
- Bevorzugte Gattung
  - Lyrik
  - kürzere Prosatexte
  - zum Teil auch Romane
- Traum und Phantasie

- Wichtige Leute
  - Hugo von Hofmannsthal
  - Thomas Mann
  - Rainer Maria Rilke
  - Arthur Schnitzler
  - International
    - Marcel Proustt
    - Oscar Wilde
    - Joseph Conrad
- Wiener Cafehausliterattur
  - Peter Altenberg
  - Arthur Schnitzler
  - Alfred Polgar


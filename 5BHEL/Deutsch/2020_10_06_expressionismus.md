# Expressionismus

- von lat "expressio" = Ausdruck
- einen Hang zur Übertreibung und zu einer revolutionären Veränderung der Kunst
- Aktivismus (Die Aktion an sich ist das Ziel)
- Die Expressionisten waren davon überzeugt, dass die Entwicklung der Menschheit chaotisch verlaufe und die Welt amoralisch sei
- im Expressionismus braucht es keine konkreten Ziele, es muss sich nur was verändern
- Merkmale Expressionistischer Literatur
  - Sprachverknappung
  - Ausfall der Füllwörter, Artikel und Präpositionen
  - Worthäufung
  - nominale Wortballungen
  - Betonung des Verses
  - Wortneubildung und Syntaxformung
  - Bevorzugung kleiner Erzählformen, zB Essays
- Vertreter
  - Gottfried Ben
  - Alfred Döblin
  - Georg Heym
  - Franz Kafka
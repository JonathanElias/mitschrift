#include "mbedappboard.h"

// setting PWM properties for leds dimming
const int freq = 500;
const int freq_3 = 1;
int ledChannel_12 = 0;
int ledChannel_3 = 3;
int ledChannel_4 = 4;
const int resolution = 8; // 0 - 255
const int resolution_4 = 4; // 0 - 15
int dutyCycleLed_12 = 200;
uint8_t dutyCycleLed_3 = 220;
int dutyCycleLed_4 = 14;

void dimmLedsSetup(){
  // configure LED PWM functionalitites
  ledcSetup(ledChannel_12, freq, resolution);
  ledcSetup(ledChannel_3, freq_3, resolution);
  ledcSetup(ledChannel_4, freq, resolution_4);
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(led1, ledChannel_12);
  ledcAttachPin(led2, ledChannel_12);
  ledcWrite(ledChannel_12, dutyCycleLed_12);
  ledcAttachPin(led3, ledChannel_3);
  ledcWrite(ledChannel_3, dutyCycleLed_3);
  ledcAttachPin(led4, ledChannel_4);
  ledcWrite(ledChannel_4, dutyCycleLed_4);
}

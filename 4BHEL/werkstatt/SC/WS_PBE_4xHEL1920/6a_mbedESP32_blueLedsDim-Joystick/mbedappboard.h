#include <Arduino.h>
#include <Ticker.h>
// blue leds
#define led1 19       // GPIO19
#define led2 17       // GPIO17
#define led3 16       // GPIO16
#define led4 4        // GPIO04
// joystick
#define INPUT_PULLDOWN 0x09 // https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-gpio.h
const byte exIntDown = 27;  // esp32->GPIO27; mbed->p12 / JoyStick Down
const byte exIntLeft = 26;  // esp32->GPIO26; mbed->p13 / JoyStick Left
const byte exIntCenter = 25;  // esp32->GPIO25; mbed->p14 / JoyStick Center
const byte exIntUp = 33;  // esp32->GPIO33; mbed->p15 / JoyStick Up
const byte exIntRight = 32;  // esp32->GPIO32; mbed->p16 / JoyStick Right
#define jsUP 1
#define jsDOWN 2
#define jsLEFT 3
#define jsRIGHT 4
#define jsCENTER 5
#define jsRELEASED 0
extern volatile int interruptJoystickState;

// prototypes
// blue leds
void blueLedsSetup();
void blueLedsTickerSetup();
void blueLedsBlinkSetup(int msecOnOffTime);
void runBlueLedsWait(int wait1, int wait2);
void setPin(int state);
void blinkLeds();
void stopBlueLedsBlinking();
void dimmLedsSetup();
// joystick
void joystickInterruptSetup();
void releaseJoystick();

#include "mbedappboard.h"

portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

volatile int interruptJoystickState = jsRELEASED;
 
void IRAM_ATTR handleExIntDown() {
  portENTER_CRITICAL_ISR(&mux);
  interruptJoystickState = jsDOWN;
  portEXIT_CRITICAL_ISR(&mux);
}

 void IRAM_ATTR handleExIntUp() {
  portENTER_CRITICAL_ISR(&mux);
  interruptJoystickState = jsUP;
  portEXIT_CRITICAL_ISR(&mux);
}

 void IRAM_ATTR handleExIntLeft() {
  portENTER_CRITICAL_ISR(&mux);
  interruptJoystickState = jsLEFT;
  portEXIT_CRITICAL_ISR(&mux);
}

 void IRAM_ATTR handleExIntRight() {
  portENTER_CRITICAL_ISR(&mux);
  interruptJoystickState = jsRIGHT;
  portEXIT_CRITICAL_ISR(&mux);
}

 void IRAM_ATTR handleExIntCenter() {
  portENTER_CRITICAL_ISR(&mux);
  interruptJoystickState = jsCENTER;
  portEXIT_CRITICAL_ISR(&mux);
}

void releaseJoystick() {
  portENTER_CRITICAL(&mux);
  interruptJoystickState = jsRELEASED;  
  portEXIT_CRITICAL(&mux);
	
}

void joystickInterruptSetup() {
  pinMode(exIntDown, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntDown), handleExIntDown, RISING);
  pinMode(exIntUp, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntUp), handleExIntUp, RISING);
  pinMode(exIntLeft, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntLeft), handleExIntLeft, RISING);
  pinMode(exIntRight, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntRight), handleExIntRight, RISING);
  pinMode(exIntCenter, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntCenter), handleExIntCenter, RISING);
}

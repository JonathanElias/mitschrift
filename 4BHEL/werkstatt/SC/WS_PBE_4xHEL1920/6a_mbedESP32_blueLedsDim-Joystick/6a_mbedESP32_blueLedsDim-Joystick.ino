#include "mbedappboard.h"

extern int ledChannel_12;
extern int ledChannel_3;
extern int ledChannel_4;
extern int dutyCycleLed_12;
extern uint8_t dutyCycleLed_3;
extern int dutyCycleLed_4;

void serialSetup() {
  Serial.begin(115200);       
  Serial.println("\n== Blue Leds on ESP32S board with Joystick on mbed board ==="); 
}

void infoJoystickFunctions() {
  Serial.println("\n=== Dimming Blue Leds with Joystick on mbed board ==="); 
  Serial.println("    Js UP --> Dim LED 1 and LED 2 brighter;  freq = 500 Hz; resolution 256 Bit");
  Serial.println("    Js DOWN --> Dim LED 1 and LED 2 darker");
  Serial.println("    Js LEFT --> Dim LED 4 brighter;  freq = 500 Hz; resolution 16 Bit");
  Serial.println("    Js RIGHT --> Dim LED 4 darker");
  Serial.println("    Js CENTER --> Dim LED 3 brighter and darker;  freq = 1 Hz; resolution 256 Bit");
}

void setup(){
  serialSetup();
  dimmLedsSetup();
  joystickInterruptSetup();
  infoJoystickFunctions();
} // setup

void runJoystick() {
  static bool blinckLedsActiv = false;
  
  switch (interruptJoystickState) {
    case jsUP:
      while (digitalRead(exIntUp) == 1) {
        // increase the LED brightness
        if (dutyCycleLed_12 > 0) // neg. logic of leds
          dutyCycleLed_12--; 
        // changing the LED brightness with PWM
        ledcWrite(ledChannel_12, dutyCycleLed_12);
        delay(15);    
      }
      break;
    case jsDOWN:
      while (digitalRead(exIntDown) == 1) {
        // decrease the LED brightness
        if (dutyCycleLed_12 <= 255) // neg. logic of leds
          dutyCycleLed_12++; 
        // changing the LED brightness with PWM
        ledcWrite(ledChannel_12, dutyCycleLed_12);
        delay(15);
      }
      break;
    case jsLEFT:
      while (digitalRead(exIntLeft) == 1) {
        // increase the LED brightness
        if (dutyCycleLed_4 > 0)
          dutyCycleLed_4--; 
        // changing the LED brightness with PWM
        ledcWrite(ledChannel_4, dutyCycleLed_4);
        delay(100);
      }
      break;
    case jsRIGHT:
      while (digitalRead(exIntRight) == 1) {
        // decrease the LED brightness
        if (dutyCycleLed_4 <= 15)
          dutyCycleLed_4++; 
        // changing the LED brightness with PWM
        ledcWrite(ledChannel_4, dutyCycleLed_4);
        delay(100);
      }
      break;
    case jsCENTER:
      while (digitalRead(exIntCenter) == 1) {
        // decrease the LED brightness
        dutyCycleLed_3++; 
        // changing the LED brightness with PWM
        ledcWrite(ledChannel_3, dutyCycleLed_3);
        delay(30);
      }
      break;
    case jsRELEASED:
      break;
    default:
      break;
  }
  if (interruptJoystickState != jsRELEASED) {
    releaseJoystick();
    infoJoystickFunctions();
  }
}

void loop(){
  runJoystick(); 
}

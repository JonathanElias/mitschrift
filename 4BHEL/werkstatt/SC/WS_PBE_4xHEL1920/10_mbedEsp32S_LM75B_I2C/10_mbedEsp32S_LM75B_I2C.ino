// LM75B_breakout
//
// Public domain 2014 Switch Science, Inc.
// https://github.com/SWITCHSCIENCE/samplecodes/blob/master/LM75B_breakout/Arduino/LM75B_breakout/LM75B_breakout.ino
// Wire lib: https://www.arduino.cc/en/Reference/Wire
// Configuration
// OS fault queue programming : queue value = 1
// OS polarity selection      : OS active LOW
// OS operation mode          : OS comparator
// device oparation mode      : normal
 
 
#include <Wire.h>
#include <stdio.h>
#define LM75B_address 0x48          // A0=A1=A2=Low
#define temp_reg      0x00          //Temperture register
#define conf_reg      0x01          //Configuration register
#define thyst_reg     0x02          //Hysterisis register
#define tos_reg       0x03          //Overtemperature shutdown register

double tos = 30.0; 
double thyst = 28.0; 
signed int tos_data = (signed int)(tos / 0.5) << 7; 
signed int thyst_data = (signed int)(thyst / 0.5) << 7;
//int osPin = 2;

signed int temp_data = 0; 
double temp = 0.0;
 
void setup()
{   
//  pinMode(osPin,INPUT_PULLUP);
//  attachInterrupt(0,temp_interrupt,CHANGE); 
  Wire.begin();
  Serial.begin(115200);
/*  
  Wire.beginTransmission(LM75B_address);
  Wire.write(tos_reg);                            
  Wire.write(tos_data >> 8);
  Wire.write(tos_data);
  Wire.endTransmission();
  Wire.beginTransmission(LM75B_address);
  Wire.write(thyst_reg);
  Wire.write(thyst_data >> 8);
  Wire.write(thyst_data);
  Wire.endTransmission();
*/
  Wire.beginTransmission(LM75B_address);
  Wire.write(temp_reg); 
  Wire.endTransmission();
}
 
void loop()
{
  Wire.requestFrom(LM75B_address,2);           
  while(Wire.available()){
    temp_data = (Wire.read() << 8);
    temp_data |= Wire.read(); 
  }
 
  temp = (temp_data >> 5) * 0.125;
 
  Serial.print(temp);
  Serial.println(" grdC");
  delay(500);
}

/* 
void temp_interrupt()  
{                                                
  if(digitalRead(osPin) == 0){ 
    Serial.print("TEMP : OVER ");
    Serial.print(tos);
    Serial.println(" deg");
  }
  else{                             
    Serial.print("TEMP : UNDER ");
    Serial.print(thyst);
    Serial.println(" deg");  
  }
}
*/

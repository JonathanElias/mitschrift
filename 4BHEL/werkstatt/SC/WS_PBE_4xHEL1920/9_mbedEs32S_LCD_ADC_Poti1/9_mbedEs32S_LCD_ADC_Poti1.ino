#include <Arduino.h>
#include <U8g2lib.h>
#include <stdio.h>

#define BUFF_LEN 18
uint8_t ADC7 = 35; 

//U8G2_ST7565_NHD_C12832_F_4W_HW_SPI u8g2(rotation, cs, dc [, reset])
// SPI-CS=GPIO05, SPI-DC(A0)=GPIO14, SPI-Reset=GPIO13
U8G2_ST7565_NHD_C12832_F_4W_HW_SPI u8g2(U8G2_R2, 5, 14, 13); // rotation = 180° = U8G2_R2 // VSPImosi(GPIO23), VSPIclk(GPIO18)

void setup(void) {
  Serial.begin(115200);
  Serial.printf("\nAnalogwert am LCD:\n");
  u8g2.begin();
}

void loop(void) {
  int AnalogIn = analogRead(ADC7);
  char buff[BUFF_LEN];
  convertA0(buff, AnalogIn);
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_ncenB14_tr);
  u8g2.drawStr(0, 20, buff);
  u8g2.sendBuffer();
//  delay(500);
}

void convertA0(char myBuff[], int A0In) {
  float aInFloat = (A0In/4096.0)*3.3;
  String stringOne = String(aInFloat, 3);
  Serial.print("Analogwert A0 = ");
  Serial.print(A0In);
  Serial.println(" ;  Spannung = " + stringOne + " V");
  //sprintf(buff, "A0 = %d", AnalogIn);
  stringOne = "Up1 = " + stringOne + " V";
  stringOne.toCharArray(myBuff, BUFF_LEN);
}


#include <Arduino.h>
// blue leds
#define led1 19       // GPIO19
#define led2 17       // GPIO17
#define led3 16       // GPIO16
#define led4 4        // GPIO04
#define redLed 0		  // GPIO00
#define greenLed 2    // GPIO02	
#define blueLed 15  	// GPIO15
const int freq = 500;
const int resolution = 8; // 0 - 255
#define MAX_PWM_VALUE_RES8BIT 256

// prototypes
// dim rgb + blue leds
void dimmLedsSetup(int16_t setInit);

/* Access Point, Web-Sever (async), Websocket-Server (sync)
 * with Websocket Client in Java Script
*/
#include <WiFi.h>
#include <SPIFFS.h>
#include <ESPAsyncWebServer.h>
#include <WebSocketsServer.h>
#include "mbedapbdDim.h"    // for led dimming

#define INIT_RGB "?Red=000&Green=000&Blue=000"
 
// Constants
const char *ssid = "scESP32-AP";
const char *password =  "08154711";
const int http_port = 80;
const int ws_port = 1337;
 
// Globals
AsyncWebServer server(http_port);
WebSocketsServer webSocket = WebSocketsServer(ws_port);
char msg_buf_init[40] = INIT_RGB;
extern int ledChannel_red;
extern int ledChannel_green;
extern int ledChannel_blue;
extern int16_t dutyCycleLed_red;
extern int16_t dutyCycleLed_green;
extern int16_t dutyCycleLed_blue;

/***********************************************************
 * Functions
*/
void setRGBLeds(char theStr[]) {
  // "?Red=0&Green=0&Blue=0"
  int pos1, pos2;
  int pwmVal = 0;
  String newStr;
  String myStr(theStr);
  Serial.printf("setRGBLeds: theStr: '%s', myStr: '%s', len = %i\n", theStr, myStr.c_str(), myStr.length());

  switch (theStr[0]) {
    case 'r':
      pos1 = myStr.indexOf('=');
      pos2 = myStr.indexOf('&');
      Serial.printf("RED: pos1 = %i, pos2 = %i, Rot = '%s'\n", pos1, pos2, myStr.c_str());
      if ((pos2 >= 0) && (pos1>=0)) {
        newStr = myStr.substring(pos1+1, pos2);
        Serial.print("RED --> newStr = " + newStr + "; pwmVal = ");
        pwmVal = newStr.toInt();
        Serial.print(pwmVal);
        dutyCycleLed_red = MAX_PWM_VALUE_RES8BIT - pwmVal;
        Serial.print("; dutyCycleLed_red = "); 
        Serial.println(dutyCycleLed_red);
        ledcWrite(ledChannel_red, dutyCycleLed_red);
      }
      break;
      
    case 'g':
      pos1 = myStr.indexOf("Green=");
      pos2 = myStr.indexOf("&Blue");
      Serial.printf("GREEN: pos1 = %i, pos2 = %i, Gruen = '%s'\n", pos1, pos2, myStr.c_str());
      if ((pos2 >= 0) && (pos1>=0)) {
        newStr = myStr.substring(pos1+6, pos2);
        pwmVal = newStr.toInt();
        ledcWrite(ledChannel_green, MAX_PWM_VALUE_RES8BIT - pwmVal);
      }
      break;
      
    case 'b':
      pos1 = myStr.lastIndexOf('=');
      Serial.printf("BLUE: pos1 = %i, Blau = '%s'\n", pos1, myStr.c_str());
      if (pos2 >= 0) {
        newStr = myStr.substring(pos1+1);
        pwmVal = newStr.toInt();
        ledcWrite(ledChannel_blue, MAX_PWM_VALUE_RES8BIT - newStr.toInt());
      }
       break;
    default: break;  
  }
  Serial.printf("setRGBLeds '%s': extracted text= '%s'; pwm-value = %u\n", myStr.c_str(), newStr.c_str(), pwmVal);
}

int interpretSetRGB(char recStr[], uint8_t clientNo) {
  Serial.printf("interpretSetRGB(a): Received text: '%s' from client[%u]\n", recStr, clientNo);
  if (strcmp(recStr, "getRGBState") != 0) {
    Serial.printf("  -- 1\n");
    setRGBLeds(recStr);
    recStr[0] = '?';
    String myStr(recStr);
    myStr += ";[" + String(clientNo, DEC) + "]";
    int cx = snprintf(msg_buf_init , myStr.length()+1, "%s",  myStr.c_str()); 
    //webSocket.broadcastTXT(msg_buf_init);
    webSocket.broadcastTXT(myStr);
    if (cx<0 && cx>=strlen(msg_buf_init)) 
      snprintf(msg_buf_init, strlen(msg_buf_init), "%s", INIT_RGB);  
   Serial.printf("interpretSetRGB(b): msg_buf_init: '%s'\n", msg_buf_init);
   return 1;
  }
  else if (strcmp(recStr, "getRGBState") == 0) {
    webSocket.sendTXT(clientNo, msg_buf_init);    
    return 2;   
  }
  else
    return 3;
}
 
// Callback: receiving any WebSocket message; syncronous call: see webSocket.loop(); in loop
void onWebSocketEvent(uint8_t client_num,
                      WStype_t type,
                      uint8_t *payload,
                      size_t length)
{
  int retVal=0;
  // Figure out the type of WebSocket event
  switch(type) {
    // Client has disconnected
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Disconnected!\n", client_num);
      break;
    // New client has connected
    case WStype_CONNECTED:
      {
        IPAddress ip = webSocket.remoteIP(client_num);
        Serial.printf("[clientNo = %u] Connection from ", client_num);
        Serial.println(ip.toString());
        IPAddress ipServer = WiFi.softAPIP();
        webSocket.sendTXT(client_num, "Server: " + ipServer.toString() + "; Client [" + String(client_num) + "]: " + ip.toString());
      }
      break;
    // Handle text messages from client
    case WStype_TEXT: {
      // Print out raw message
      Serial.printf("\n[clientNo = %u] Received text: %s\n", client_num, payload);
      // interpret received string an set rgb-led
      retVal = interpretSetRGB((char *)payload, client_num);
      if (retVal == 1) {
      }
      else if (retVal == 2) {
      }
      // Message not recognized
      else {
        Serial.printf("[Client: %u] Message (%s) not recognized, returnValue = %i\n", client_num, (char *)payload, retVal);
        webSocket.sendTXT(client_num, "ERRORR");
      }
      break; }
    // For everything else: do nothing
    case WStype_BIN:
    case WStype_ERROR:
    case WStype_FRAGMENT_TEXT_START:
    case WStype_FRAGMENT_BIN_START:
    case WStype_FRAGMENT:
    case WStype_FRAGMENT_FIN:
    default:
      break;
  }
}
 
// Callback: send homepage
void onIndexRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/index.html", "text/html");
}
 
// Callback: send style sheet
void onCSSRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/style.css", "text/css");
}

// Callback: send favicon.ico
void onFaviconRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/favicon.ico", "image/x-icon");
}
 
// Callback: send 404 if requested file does not exist
void onPageNotFound(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(404, "text/plain", "Not found");
}

void initLeds() {
  pinMode(led4, OUTPUT);
  digitalWrite(led4, 1);  // off: negative logic
  dimmLedsSetup(-1);       // setup for led dimming  
}

/***********************************************************
 * Main
*/
void setup() {
  // Start Serial port
  Serial.begin(115200);
  initLeds();
  // Make sure we can read the file system
  if( !SPIFFS.begin()){
    Serial.println("Error mounting SPIFFS");
    while(1);
  }
  // Start access point
  WiFi.softAP(ssid, password); 
  // Print our IP address
  Serial.println();
  Serial.println("AP running");
  Serial.print("My IP address: ");
  Serial.println(WiFi.softAPIP());
  // On HTTP request for root, provide index.html file
  server.on("/", HTTP_GET, onIndexRequest);
  // On HTTP request for style sheet, provide style.css
  server.on("/style.css", HTTP_GET, onCSSRequest);
  // On favicon request, provide favicon.ico file
  server.on("/favicon.ico", HTTP_GET, onFaviconRequest);
  // Handle requests for pages that do not exist
  server.onNotFound(onPageNotFound);
  // Start web server
  server.begin();
  // Start WebSocket server and assign callback
  webSocket.begin();
  webSocket.onEvent(onWebSocketEvent);
}
 
void loop() {
  // Look for and handle WebSocket data
  webSocket.loop();
}

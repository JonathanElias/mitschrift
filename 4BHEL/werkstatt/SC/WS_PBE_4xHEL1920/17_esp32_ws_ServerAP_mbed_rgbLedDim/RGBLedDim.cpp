#include "mbedapbdDim.h"
// setting PWM properties for leds dimming
int ledChannel_red = 0;
int ledChannel_green = 1;
int ledChannel_blue = 2;
int16_t dutyCycleLed_red=MAX_PWM_VALUE_RES8BIT;
int16_t dutyCycleLed_green=MAX_PWM_VALUE_RES8BIT;
int16_t dutyCycleLed_blue=MAX_PWM_VALUE_RES8BIT;

void dimmLedsSetup(int16_t setInit){
  // configure LED PWM functionalitites
  ledcSetup(ledChannel_red, freq, resolution);
  ledcSetup(ledChannel_green, freq, resolution);
  ledcSetup(ledChannel_blue, freq, resolution);
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(redLed, ledChannel_red);
  ledcAttachPin(led1, ledChannel_red);
  ledcAttachPin(greenLed, ledChannel_green);
  ledcAttachPin(led2, ledChannel_green);
  ledcAttachPin(blueLed, ledChannel_blue);
  ledcAttachPin(led3, ledChannel_blue);
  if (setInit == -1) {
	  ledcWrite(ledChannel_red, dutyCycleLed_red);
	  ledcWrite(ledChannel_green, dutyCycleLed_green);
	  ledcWrite(ledChannel_blue, dutyCycleLed_blue);
  }
  else if ((setInit >= 0) && (setInit<=MAX_PWM_VALUE_RES8BIT)) {
	  ledcWrite(ledChannel_red, setInit);
	  ledcWrite(ledChannel_green, setInit);
	  ledcWrite(ledChannel_blue, setInit);	  
  }
}

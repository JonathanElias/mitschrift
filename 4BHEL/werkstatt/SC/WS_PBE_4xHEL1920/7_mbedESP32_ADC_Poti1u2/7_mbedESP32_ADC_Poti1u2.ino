#define blueLED 2

uint8_t ADC6 = 34; 
uint8_t ADC7 = 35; 
uint16_t adcPoti2, adcPoti1; 

void setup() {   
  pinMode(blueLED, OUTPUT);
  Serial.begin(115200);       
  Serial.println("\n DAC\mbed\tPoti1->ADC5(P33)\tPoti2->ADC4(P32)"); 
} 

void loop() {    
  if (adcPoti1 > 2000)
    digitalWrite(blueLED, HIGH);          
  delay(100);          
  adcPoti2 = analogRead(ADC6);     
  adcPoti1 = analogRead(ADC7);     
  Serial.printf("ADC: Poti1=%04d\tPoti2=%4d\n", adcPoti1, adcPoti2); 
  if (adcPoti2 < 1000)
    digitalWrite(blueLED, LOW);      
  delay(100); 
}

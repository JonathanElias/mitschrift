#define mbedLED_R 0   // GPIO00
#define mbedLED_G 2   // GPIO02
#define mbedLED_B 15  // GPIO15
//#define led1 19       // GPIO19
#define led2 17       // GPIO17
//#define led3 16       // GPIO16
//#define led4 4        // GPIO04

void setup(){
  pinMode(mbedLED_R, OUTPUT);
  pinMode(mbedLED_G, OUTPUT);
  pinMode(mbedLED_B, OUTPUT);
  pinMode(led2, OUTPUT);
} // setup
 
void loop(){
  digitalWrite(mbedLED_R, !digitalRead(mbedLED_R));
  digitalWrite(led2, !digitalRead(led2));
  delay(1000);
  digitalWrite(mbedLED_G, !digitalRead(mbedLED_G));
  digitalWrite(led2, !digitalRead(led2));
  delay(1000);
  digitalWrite(mbedLED_B, !digitalRead(mbedLED_B));
  digitalWrite(led2, !digitalRead(led2));
  delay(1000);
}


#include <Arduino.h>
#include <U8g2lib.h>
#include <stdio.h>

//https://github.com/olikraus/U8g2_Arduino/blob/master/examples/page_buffer/GraphicsTest/GraphicsTest.ino
//https://github.com/olikraus/u8g2/wiki/u8g2reference
//U8G2_ST7565_NHD_C12832_1_4W_HW_SPI u8g2(rotation, cs, dc [, reset])
U8G2_ST7565_NHD_C12832_1_4W_HW_SPI u8g2(U8G2_R2, 5, 14, 13); // rotation = 180° = U8G2_R2 // VSPImosi(GPIO23), VSPIclk(GPIO18)

int lpCount = 0;
char buff[20] = {'x', ' ', '=', ' '};

void setup(void) {
  Serial.begin(115200);
  Serial.printf("\nsetup 1: %d\n", lpCount++);
  u8g2.begin();
  Serial.printf("setup 2: %d\n", lpCount++);
}

void loop(void) {
  Serial.printf("\n--> loop: %d", lpCount++);
  u8g2.firstPage();
  do {
    u8g2.setFont(u8g2_font_ncenB14_tr);
    u8g2.drawStr(0,24,"Hallo 4CHEL!");
  } while ( u8g2.nextPage() );
  delay(2000);
  
  u8g2.firstPage();
  sprintf(buff, "x = %d", lpCount%0xFFFF);
  do {
    u8g2.setFont(u8g2_font_ncenB14_tr);
    u8g2.drawStr(0, 24, buff);
  } while ( u8g2.nextPage() );
  delay(2000);
}

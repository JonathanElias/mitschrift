#include "mbedappboard.h"

Ticker tickerSetHigh;
Ticker tickerSetLow;
Ticker tickerBlink;

void blueLedsSetup() {
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);  
  digitalWrite(led1, 1);
  digitalWrite(led2, 1);
  digitalWrite(led3, 1);
  digitalWrite(led4, 1);
}
void blueLedsTickerSetup() {
  blueLedsSetup();
  // every 500 ms, call setPin(1) : neg logic
  tickerSetLow.attach_ms(500, setPin, 1);
  // every 550 ms, call setPin(0)
  tickerSetHigh.attach_ms(550, setPin, 0);  
}
void setPin(int state) {
  digitalWrite(led1, state);
}
void blueLedsBlinkSetup(int msecOnOffTime) {
  blueLedsSetup();
  // every msecOnOffTime ms, call blinkPin() : neg logic
  tickerBlink.attach_ms(msecOnOffTime, blinkLeds);
}
void stopBlueLedsBlinking() {
  tickerBlink.detach();
}
void blinkLeds() {
  digitalWrite(led1, !digitalRead(led1));
  digitalWrite(led2, !digitalRead(led2));
  digitalWrite(led3, !digitalRead(led3));
  digitalWrite(led4, !digitalRead(led4));
}	
void runBlueLedsWait(int wait1, int wait2) {
  digitalWrite(led1, !digitalRead(led1));
  delay(wait1);
  digitalWrite(led2, !digitalRead(led2));
  delay(wait1);
  digitalWrite(led3, !digitalRead(led3));
  delay(wait1);
  digitalWrite(led4, !digitalRead(led4));
  delay(wait2);
}

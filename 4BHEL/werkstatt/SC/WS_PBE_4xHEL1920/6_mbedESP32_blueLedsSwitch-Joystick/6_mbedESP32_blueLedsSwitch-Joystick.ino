#include "mbedappboard.h"

void serialSetup() {
  Serial.begin(115200);       
  Serial.println("\n== Blue Leds on ESP32S board with Joystick on mbed board ==="); 
}

void infoJoystickFunctions() {
  Serial.println("\n=== Switch Blue Leds with Joystick on mbed board ==="); 
  Serial.println("    Js UP --> Toggle LED 1;    Js DOWN --> Toggle LED 2");
  Serial.println("    Js LEFT --> Toggle LED 3;  Js RIGHT --> Toggle LED 4");
  Serial.println("    Js CENTER --> Toggle Blinking Led 1 - 4");
}

void setup(){
  //blueLedsTickerSetup();
  //blueLedsBlinkSetup(200);
  serialSetup();
  blueLedsSetup();
  joystickInterruptSetup();
  infoJoystickFunctions();
} // setup

void runJoystick() {
  static bool blinckLedsActiv = false;
  switch (interruptJoystickState) {
    case jsUP:
      digitalWrite(led1, !digitalRead(led1));
      break;
    case jsDOWN:
      digitalWrite(led2, !digitalRead(led2));
      break;
    case jsLEFT:
      digitalWrite(led3, !digitalRead(led3));
      break;
    case jsRIGHT:
      digitalWrite(led4, !digitalRead(led4));
      break;
    case jsCENTER:
        if (!blinckLedsActiv) {
          blueLedsBlinkSetup(200);
          blinckLedsActiv = true;
        }
        else {
          stopBlueLedsBlinking();
          blinckLedsActiv = false;
        }
      break;
    case jsRELEASED:
      break;
    default:
      break;
  }
  if (interruptJoystickState != jsRELEASED) {
    releaseJoystick();
    infoJoystickFunctions();
  }
}

void loop(){
  //runBlueLedsWait(100, 1000);
  runJoystick(); 
}

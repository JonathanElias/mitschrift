#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>
#include <SPIFFS.h>

#define pinD2 17
#define pinD3 16

uint8_t ADC6 = 34; 
uint8_t ADC7 = 35; 
uint16_t adcPoti2, adcPoti1;

const char* path_root   = "/index.html";

/// Access Point
const char* ssid = "xy-NodeMCU_v2";
const char* password = "08154711";  // set to "" for open access point w/o passwortd

IPAddress Ip(192, 168, 12, 1);
IPAddress NMask(255, 255, 255, 0);

#define BUFFER_SIZE 16384
uint8_t buf[BUFFER_SIZE];

AsyncWebServer server(80);

boolean readHTML() {
  File htmlFile = SPIFFS.open(path_root, "r");
  if (!htmlFile) {
    Serial.println("Failed to open index.html");
    return false;
  }
  size_t size = htmlFile.size();
  if (size >= BUFFER_SIZE) {
    Serial.print("File Size Error:");
    Serial.println((int)size);
  }
  else {
    Serial.print("File Size OK:");
    Serial.println((int)size);
  }
  htmlFile.read(buf, size);
  htmlFile.close();
  return true;
}

void LedOn() {
  Serial.println("Leds EIN");
  digitalWrite(pinD2, LOW);
  digitalWrite(pinD3, LOW);
}

void LedOff() {
  Serial.println("Leds Aus");
  digitalWrite(pinD2, HIGH);
  digitalWrite(pinD3, HIGH);
}

String potiVal() {
  adcPoti2 = analogRead(ADC6);     
  adcPoti1 = analogRead(ADC7);     
  float aInFloat1 = (adcPoti1/4096.0)*3.3;
  float aInFloat2 = (adcPoti2/4096.0)*3.3;
  String stringOne = "1Analogwert P1 = " + String(adcPoti1) + " ;  Spannung = " + String(aInFloat1, 3) + " V;  ";
  stringOne += "Analogwert P2 = " + String(adcPoti2) + " ;  Spannung = " + String(aInFloat2, 3) + " V";
  Serial.println(stringOne);
  return stringOne;
}

void info2Serial(int traceLevel) {
  if (traceLevel > 0) {
    Serial.println("Connecting to Access Point");
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.softAPIP());  
  }
}

void setup() {
  Serial.begin(115200);
  delay(500);
  SPIFFS.begin();
  if (!readHTML()) {
    Serial.println("Read HTML error!!");
  }
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(Ip, Ip, NMask);

  info2Serial(1);

  //old// server.on("/", handleRoot);
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    Serial.println("Access ");
    request->send(200, "text/html", (char *)buf);
  });
  
  server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request){
    LedOn();
    request->send(200, "text/html", "0Alle LEDs EIN");
  });
     
  server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request){
    LedOff();
    request->send(200, "text/html", "0Alle LEDs AUS");
  });
     
  server.on("/analogIn", HTTP_GET, [](AsyncWebServerRequest *request){
    String myVal = potiVal();
    request->send(200, "text/html", myVal);
  });
  
  server.begin();

  pinMode(pinD2, OUTPUT);          // sets the digital pin D2 on ESP12E as output
  pinMode(pinD3, OUTPUT);          // sets the digital pin D3 on ESP12E as output
}

void loop() {
}

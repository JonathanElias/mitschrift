//#include <ESP32Ticker.h>
#include <Ticker.h>
#define INPUT_PULLDOWN 0x09 // https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-gpio.h
#define onBoardLED 2
const byte exIntCenter = 25;  // esp32->GPIO25; mbed->p14 / JoyStick Center
volatile int interruptCounter = 0;
int numberOfInterrupts = 0;
volatile bool interruptFlag = true;
Ticker tInt;

 
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
 
void IRAM_ATTR handlePinInterrupt() {
  portENTER_CRITICAL_ISR(&mux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&mux);
  if (interruptFlag) {
    interruptFlag = false;
    digitalWrite(onBoardLED, !digitalRead(onBoardLED));
    tInt.once_ms(100, timeoutInt);  // waiting time for debouncing is 100 mse
  }
}

void timeoutInt() {
  interruptFlag = true;  
}
 
void setup() {
  pinMode(onBoardLED, OUTPUT);
  Serial.begin(115200);
  Serial.println("Monitoring interrupts: ");
  pinMode(exIntCenter, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntCenter), handlePinInterrupt, RISING);
}
 
void loop() {
  if(interruptFlag && (interruptCounter>0)){   
    Serial.print("Counted Interrups Total: ");
    Serial.println(interruptCounter); // this documents the bouncing
    portENTER_CRITICAL(&mux);
    interruptCounter = 0;
    portEXIT_CRITICAL(&mux);
    
    numberOfInterrupts++;
    Serial.print("An interrupt has occurred. Total: ");
    Serial.println(numberOfInterrupts);
  }
}

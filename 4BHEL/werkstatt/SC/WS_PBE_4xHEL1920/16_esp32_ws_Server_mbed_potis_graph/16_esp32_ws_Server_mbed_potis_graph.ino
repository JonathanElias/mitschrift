/* Access Point, Web-Sever (async), Websocket-Server (sync)
 * with Websocket Client in Java Script
 * Switch mbedAppBoard RGB-RED-Led and read get the 2 mbed poti valus with 5Hz
*/

#include <WiFi.h>
#include <SPIFFS.h>
#include <ESPAsyncWebServer.h>
#include <WebSocketsServer.h>
#include <Ticker.h>
 
// Constants
// Hotspot am Handy aktivieren mit nachfolgen Einstellungen 
//const char* ssid = "scAndroid"; // am Handy
//const char* password = "helsko123"; // set to "" for open access point w/o passwortd 
const char* ssid = "fritzbox-SC"; // fritzbox sc
const char* password = "0123456789abcdef"; // set to "" for open access point w/o passwortd 
const int http_port = 80;
const int ws_port = 1337;
const float updatePeriod = 0.5; //seconds, 2Hz
const uint8_t ADC6 = 34; 
const uint8_t ADC7 = 35; 
 
// Globals
AsyncWebServer server(http_port);
WebSocketsServer webSocket = WebSocketsServer(ws_port);
char msg_buf[] = "xy00";
int led_state = 0;
Ticker updater;
uint16_t adcPoti2, adcPoti1; 
char potiBuff[40];
 
/***********************************************************
 * Functions
 */
void sendPotiValues() {
  adcPoti2 = (analogRead(ADC6)/4095.0)*100;     
  adcPoti1 = (analogRead(ADC7)/4095.0)*100;     
  sprintf(potiBuff, "P1=%04d;P2=%04d", adcPoti1, adcPoti2);   
  webSocket.broadcastTXT(potiBuff);
}

 
 
// Callback: receiving any WebSocket message; syncronous call: see webSocket.loop(); in loop
void onWebSocketEvent(uint8_t client_num,
                      WStype_t type,
                      uint8_t * payload,
                      size_t length) {
 
  // Figure out the type of WebSocket event
  switch(type) {
 
    // Client has disconnected
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Disconnected!\n", client_num);
      break;
 
    // New client has connected
    case WStype_CONNECTED:
      {
        IPAddress ip = webSocket.remoteIP(client_num);
        Serial.printf("[%u] Connection from ", client_num);
        Serial.println(ip.toString());
        updater.detach();
        updater.attach(updatePeriod, sendPotiValues);
      }
      break;
 
    // Handle text messages from client
    case WStype_TEXT: {
 
      // Print out raw message
      Serial.printf("[%u] Received text: %s\n", client_num, payload);
 
      // start to send poti values to clients
      if ( strcmp((char *)payload, "ON_Potis") == 0) {
        msg_buf[1] = '1';
        Serial.printf("Poti-Values sending started from client [%u]: %s\n", client_num, msg_buf);
        webSocket.sendTXT(client_num, msg_buf);
        //webSocket.broadcastTXT(msg_buf);
        //updater.detach();
        //updater.attach(updatePeriod, sendPotiValues);
      }
      // stop poti values sending to clients
      else if ( strcmp((char *)payload, "OFF_Potis") == 0) {
        msg_buf[1] = '0';
        Serial.printf("Poti-Values sending stopped from client [%u]: %s\n", client_num, msg_buf);
        webSocket.sendTXT(client_num, msg_buf);
        //webSocket.broadcastTXT(msg_buf);
        //updater.detach();
      }
      // Message not recognized
      else {
        Serial.println("[%u] Message not recognized");
      }
      break; }
 
    // For everything else: do nothing
    case WStype_BIN:
    case WStype_ERROR:
    case WStype_FRAGMENT_TEXT_START:
    case WStype_FRAGMENT_BIN_START:
    case WStype_FRAGMENT:
    case WStype_FRAGMENT_FIN:
    default:
      break;
  }
}
 
// Callback: send homepage
void onIndexRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/index.html", "text/html");
} 
// Callback: send favicon.ico
void onFaviconRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/favicon.ico", "image/x-icon");
} 
// Callback: send 404 if requested file does not exist
void onPageNotFound(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(404, "text/plain", "Not found");
}
 
/************************************************************/
 
void setup() {
  // Start Serial port
  Serial.begin(115200);
  // Make sure we can read the file system
  if( !SPIFFS.begin()){
    Serial.println("Error mounting SPIFFS");
    while(1);
  }
  // Start access point
  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);
  Serial.println("Connecting to Access Point");
  while (WiFi.status() != WL_CONNECTED) { 
    delay(500); 
    Serial.print("."); 
  }
  // Print our IP address
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP()); 
 
  // On HTTP request for root, provide index.html file
  server.on("/", HTTP_GET, onIndexRequest);  
  // On favicon request, provide favicon.ico file
  server.on("/favicon.ico", HTTP_GET, onFaviconRequest);
  // Handle requests for pages that do not exist
  server.onNotFound(onPageNotFound);
  // Start web server
  server.begin();
  // Start WebSocket server and assign callback
  webSocket.begin();
  webSocket.onEvent(onWebSocketEvent);  
}
 
void loop() {
  
  // Look for and handle WebSocket data
  webSocket.loop();
}

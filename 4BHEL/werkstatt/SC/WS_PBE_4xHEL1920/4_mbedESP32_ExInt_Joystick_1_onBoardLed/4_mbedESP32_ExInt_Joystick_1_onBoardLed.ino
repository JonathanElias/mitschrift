#define INPUT_PULLDOWN 0x09 // https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-gpio.h

const byte exIntDown = 27;  // esp32->GPIO27; mbed->p12
volatile int interruptCounter = 0;
int numberOfInterrupts = 0;
#define onBoardLED 2
 
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
 
void IRAM_ATTR handleInterrupt() {
  portENTER_CRITICAL_ISR(&mux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&mux);
  digitalWrite(onBoardLED, !digitalRead(onBoardLED));
}
 
void setup() {
  pinMode(onBoardLED, OUTPUT);
  Serial.begin(115200);
  Serial.println("Monitoring interrupts: ");
  pinMode(exIntDown, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntDown), handleInterrupt, RISING);
}
 
void loop() {
 
  if(interruptCounter > 0){
 
      portENTER_CRITICAL(&mux);
      interruptCounter--;
      portEXIT_CRITICAL(&mux);
 
      numberOfInterrupts++;
      Serial.print("An interrupt has occurred. Total: ");
      Serial.println(numberOfInterrupts);
  }
}

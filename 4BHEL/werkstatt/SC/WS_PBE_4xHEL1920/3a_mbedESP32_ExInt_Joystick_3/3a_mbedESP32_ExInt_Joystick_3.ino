// https://techtutorialsx.com/2017/09/30/esp32-arduino-external-interrupts/
#define INPUT_PULLDOWN 0x09 // https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-gpio.h
#define led1 19 // GPIO19
#define led2 17 // GPIO17
#define led3 16 // GPIO16
#define led4 4 // GPIO04

const byte exIntDown = 27;  // esp32->GPIO27; mbed->p12
const byte exIntCenter = 25;  // esp32->GPIO25; mbed->p14 / JoyStick Center
const byte exIntUp = 33;  // esp32->GPIO33; mbed->p15 / JoyStick Up
volatile short interruptCounter = 0;
int counter = 0;
 
//  Declare a variable of type portMUX_TYPE, which we will need to take care of the synchronization between the main code and the interrupt.
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

// The interrupt handling routine should have the IRAM_ATTR attribute (IRAM=Internal RAM), in order for the compiler to place the code in IRAM. 
// Also, interrupt handling routines should only call functions also placed in IRAM, as can be seen here in the IDF documentation. 
void IRAM_ATTR handleIntCountUp() { 
// critical section: interruptCounter is used in interrupt and main programm 
	portENTER_CRITICAL_ISR(&mux);	// ENTER and EXIT CRITICAL_ISR: critical section: interruptCounter ist nun für einen Zugriff durch "andere" gesperrt
	interruptCounter = 1;
	portEXIT_CRITICAL_ISR(&mux); 	// EXIT CRITICAL_ISR: auf interruptCounter kann jetzt wieder "anderweitig zugegriffen werden (z.B. im main loop)
  digitalWrite(led1, !(digitalRead(led1)));
}
void IRAM_ATTR handleIntCountDown() { 
	portENTER_CRITICAL_ISR(&mux);
	interruptCounter = -1;
	portEXIT_CRITICAL_ISR(&mux);
  digitalWrite(led4, !(digitalRead(led4)));
}
void IRAM_ATTR handleIntCountReset() { 
	portENTER_CRITICAL_ISR(&mux);
	interruptCounter = SHRT_MAX;	// maximum value of short integer
	portEXIT_CRITICAL_ISR(&mux);
  digitalWrite(led2, !(digitalRead(led2)));
  digitalWrite(led3, !(digitalRead(led3)));
}
 
void setup() {
	Serial.begin(115200);
	Serial.println("Monitoring interrupts: ");
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(exIntUp, INPUT_PULLDOWN);
  pinMode(exIntDown, INPUT_PULLDOWN);
  pinMode(exIntCenter, INPUT_PULLDOWN);
	attachInterrupt(digitalPinToInterrupt(exIntUp), handleIntCountUp, RISING);
	attachInterrupt(digitalPinToInterrupt(exIntDown), handleIntCountDown, RISING);
	attachInterrupt(digitalPinToInterrupt(exIntCenter), handleIntCountReset, RISING);
}
 
void loop() {
  if(interruptCounter != 0){
    Serial.print("interruptCounter:  ");
    Serial.println(interruptCounter);
  	if (interruptCounter >0) {
  		if (interruptCounter == SHRT_MAX) {
  			counter = 0;	
  		}
  		else {
  			counter++;
  		}
  	}
  	else {
  		counter--;
  	}
  	portENTER_CRITICAL(&mux);	// ENTER CRITICAL: critical section: interruptCounter ist nun für einen Zugriff durch "andere" gesperrt
  	interruptCounter = 0;
  	portEXIT_CRITICAL(&mux);	// EXIT CRITICAL: auf interruptCounter kann jetzt wieder "anderweitig zugegriffen werden (z.B. im Interrupt)
   
    Serial.print("counter value = ");
  	Serial.println(counter);
    Serial.println();
	}
}

#define INPUT_PULLDOWN 0x09 // https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-gpio.h

const byte exIntDown = 27;  // esp32->GPIO27; mbed->p12 / JoyStick Down
const byte exIntLeft = 26;  // esp32->GPIO26; mbed->p13 / JoyStick Left
const byte exIntCenter = 25;  // esp32->GPIO25; mbed->p14 / JoyStick Center
const byte exIntUp = 33;  // esp32->GPIO33; mbed->p15 / JoyStick Up
const byte exIntRight = 32;  // esp32->GPIO32; mbed->p16 / JoyStick Right
volatile int interruptCounter_Down = 0;
volatile int interruptCounter_Up = 0;
volatile int interruptCounter_Left = 0;
volatile int interruptCounter_Right = 0;
volatile int interruptCounter_Center = 0;
//int numberOfInterrupts = 0;
 
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
 
void IRAM_ATTR handleExIntDown() {
  portENTER_CRITICAL_ISR(&mux);
  interruptCounter_Down++;
  portEXIT_CRITICAL_ISR(&mux);
}

 void IRAM_ATTR handleExIntUp() {
  portENTER_CRITICAL_ISR(&mux);
  interruptCounter_Up++;
  portEXIT_CRITICAL_ISR(&mux);
}

 void IRAM_ATTR handleExIntLeft() {
  portENTER_CRITICAL_ISR(&mux);
  interruptCounter_Left++;
  portEXIT_CRITICAL_ISR(&mux);
}

 void IRAM_ATTR handleExIntRight() {
  portENTER_CRITICAL_ISR(&mux);
  interruptCounter_Right++;
  portEXIT_CRITICAL_ISR(&mux);
}

 void IRAM_ATTR handleExIntCenter() {
  portENTER_CRITICAL_ISR(&mux);
  interruptCounter_Center++;
  portEXIT_CRITICAL_ISR(&mux);
}
 
void setup() {
  Serial.begin(115200);
  Serial.println("Monitoring interrupts from mbed applcation board :");

  pinMode(exIntDown, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntDown), handleExIntDown, RISING);
  pinMode(exIntUp, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntUp), handleExIntUp, RISING);
  pinMode(exIntLeft, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntLeft), handleExIntLeft, RISING);
  pinMode(exIntRight, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntRight), handleExIntRight, RISING);
  pinMode(exIntCenter, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntCenter), handleExIntCenter, RISING);
}

void doExIntterupt(volatile int &interruptCounter, String info, byte index) {
  static int numberOfInterrupts[5];
  if (interruptCounter > 0) {
      portENTER_CRITICAL(&mux);
      interruptCounter--;
      portEXIT_CRITICAL(&mux);
 
      numberOfInterrupts[index]++;
      Serial.print("An ");
      Serial.print(info);
      Serial.print(" interrupt has occurred. ");
      Serial.print(info);
      Serial.print("-Total: ");
      Serial.println(numberOfInterrupts[index]);
  }  
}

void loop() {
  doExIntterupt(interruptCounter_Down, "Joystick DOWN", 0);
  doExIntterupt(interruptCounter_Up, "Joystick UP", 1);
  doExIntterupt(interruptCounter_Left, "Joystick LEFT", 2);
  doExIntterupt(interruptCounter_Right, "Joystick RIGHT", 3);
  doExIntterupt(interruptCounter_Center, "Joystick CENTER", 4);
}

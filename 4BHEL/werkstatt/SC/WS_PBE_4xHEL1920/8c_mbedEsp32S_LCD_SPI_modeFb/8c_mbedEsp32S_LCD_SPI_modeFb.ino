#include <Arduino.h>
#include <U8g2lib.h>
#include <stdio.h>

//U8G2_ST7565_NHD_C12832_F_4W_HW_SPI u8g2(rotation, cs, dc [, reset])
U8G2_ST7565_NHD_C12832_F_4W_HW_SPI u8g2(U8G2_R2, 5, 14, 13); // rotation = 180° = U8G2_R2 // VSPImosi(GPIO23), VSPIclk(GPIO18)

unsigned long _lpCount = 0;
char _buff[20];
unsigned long _stoppTime =0;
unsigned long _startTime =0; 

void setup(void) {
  Serial.begin(115200);
  u8g2.begin();
  _startTime = millis();
}

void loop(void) {
  _stoppTime = millis();
  sprintf(_buff, "x = %d", _lpCount++%0xFFFF);
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_ncenB14_tr);
  u8g2.drawStr(0, 20, _buff);
  u8g2.sendBuffer();
  if (_lpCount%1000 == 0) {
    Serial.print("\n_doubleDiffTime = ");
    Serial.println((_stoppTime - _startTime)/1000.0, 3);  // 3 Nachkomma-Stellen    
    _startTime = _stoppTime;
  }
}

#include <Arduino.h>
#include <U8g2lib.h>
#include <stdio.h>

//U8G2_ST7565_NHD_C12832_F_4W_HW_SPI u8g2(rotation, cs, dc [, reset])
U8G2_ST7565_NHD_C12832_F_4W_HW_SPI u8g2(U8G2_R2, 5, 14, 13); // rotation = 180° = U8G2_R2 // VSPImosi(GPIO23), VSPIclk(GPIO18)

int lpCount = 0;
char buff[20];

void setup(void) {
  Serial.begin(115200);
  Serial.printf("\nFast mode setup 1: %d\n", lpCount++);
  u8g2.begin();
  Serial.printf("Fast mode setup 2: %d\n", lpCount++);
}

void loop(void) {
  sprintf(buff, "x = %d", lpCount++%0xFFFF);
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_ncenB14_tr);
  u8g2.drawStr(0,20,buff);
  u8g2.sendBuffer();
  delay(50);
}

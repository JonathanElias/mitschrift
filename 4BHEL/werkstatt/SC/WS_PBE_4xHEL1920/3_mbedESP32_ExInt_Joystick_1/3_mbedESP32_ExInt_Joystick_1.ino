 #define INPUT_PULLDOWN 0x09 // https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-gpio.h

const byte exIntDown = 27;  // esp32->GPIO27; mbed->p12
volatile int interruptCounter = 0;
int numberOfInterrupts = 0;
 
// https://techtutorialsx.com/2017/09/30/esp32-arduino-external-interrupts/
//  Declare a variable of type portMUX_TYPE, which we will need to take care of the synchronization between the main code and the interrupt.
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

// The interrupt handling routine should have the IRAM_ATTR attribute (IRAM=Internal RAM), in order for the compiler to place the code in IRAM. 
// Also, interrupt handling routines should only call functions also placed in IRAM, as can be seen here in the IDF documentation. 
void IRAM_ATTR handleInterrupt() { 
// critical section: interruptCounter is used in interrupt and main programm 
  portENTER_CRITICAL_ISR(&mux);	// ENTER and EXIT CRITICAL_ISR: critical section: interruptCounter ist nun für einen Zugriff durch "andere" gesperrt
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&mux); 	// EXIT CRITICAL_ISR: auf interruptCounter kann jetzt wieder "anderweitig zugegriffen werden (z.B. im main loop)
}
 
void setup() {
 
  Serial.begin(115200);
  Serial.println("Monitoring interrupts: ");
  pinMode(exIntDown, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(exIntDown), handleInterrupt, RISING);
 
}
 
void loop() {
 
  if(interruptCounter > 0){
 
      portENTER_CRITICAL(&mux);	// ENTER CRITICAL: critical section: interruptCounter ist nun für einen Zugriff durch "andere" gesperrt
      interruptCounter--;
      portEXIT_CRITICAL(&mux);	// EXIT CRITICAL: auf interruptCounter kann jetzt wieder "anderweitig zugegriffen werden (z.B. im Interrupt)
 
      numberOfInterrupts++;
      Serial.print("An interrupt has occurred. Total: ");
      Serial.println(numberOfInterrupts);
  }
}

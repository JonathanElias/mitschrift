#include <WiFi.h>
#include <FS.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

#define mbedLED_R 0   // GPIO00
#define mbedLED_G 2   // GPIO02
#define mbedLED_B 15  // GPIO15


const char *ssid = "scESP32AP";
const char *password = "scpassword";
IPAddress Ip(192, 168, 12, 1);
IPAddress NMask(255, 255, 255, 0);

AsyncWebServer server(80);
 
void setup(){
  pinMode(mbedLED_R, OUTPUT);
  pinMode(mbedLED_G, OUTPUT);
  pinMode(mbedLED_B, OUTPUT);
  
  Serial.begin(115200);
  WiFi.softAP(ssid, password);  // default IP = 192.168.4.1

  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(Ip, Ip, NMask);
  
  Serial.println();
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP());
 
  server.on("/hello", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", "Hello World: blabla");
  });
  server.on("/tr", HTTP_GET, [](AsyncWebServerRequest *request){  // tr = toggle red
    digitalWrite(mbedLED_R, !digitalRead(mbedLED_R));
    static uint16_t count=0;
    String str2Send = String(count++, DEC);
    if (digitalRead(mbedLED_R)== LOW)
      str2Send += ": mbed Red LED is ON";
    else
      str2Send += ": mbed Red LED is OFF";
    request->send(200, "text/plain", str2Send);
  });
  server.on("/tg", HTTP_GET, [](AsyncWebServerRequest *request){  // tr = toggle green
    digitalWrite(mbedLED_G, !digitalRead(mbedLED_G));
    static uint16_t count=0;
    String str2Send = String(count++, DEC);
    if (digitalRead(mbedLED_G)== LOW) // negative logic
      str2Send += ": mbed Green LED is ON";
    else
      str2Send += ": mbed Green LED is OFF";
    request->send(200, "text/plain", str2Send);
  });
  server.on("/tb", HTTP_GET, [](AsyncWebServerRequest *request){  // tr = toggle blue
    digitalWrite(mbedLED_B, !digitalRead(mbedLED_B));
    static uint16_t count=0;
    String str2Send = String(count++, DEC);
    if (digitalRead(mbedLED_B)== LOW)
      str2Send += ": mbed Blue LED is ON";
    else
      str2Send += ": mbed Blue LED is OFF";
    request->send(200, "text/plain", str2Send);
  });
  server.begin();
} // setup
 
void loop(){
  Serial.println();
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP());
  delay(10000);
}

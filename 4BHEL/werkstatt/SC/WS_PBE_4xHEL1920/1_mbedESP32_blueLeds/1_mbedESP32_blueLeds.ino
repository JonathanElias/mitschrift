#define led1 19       // GPIO19
#define led2 17       // GPIO17
#define led3 16       // GPIO16
#define led4 4        // GPIO04

void setup(){
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
} // setup
 
void loop(){
  digitalWrite(led1, !digitalRead(led1));
  delay(100);
  digitalWrite(led2, !digitalRead(led2));
  delay(100);
  digitalWrite(led3, !digitalRead(led3));
  delay(100);
  digitalWrite(led4, !digitalRead(led4));
  delay(1000);
}

#include <Ticker.h>

#define led1 19       // GPIO19
#define led2 17       // GPIO17
#define led3 16       // GPIO16
#define led4 4        // GPIO04

Ticker blinker;
Ticker changer;
float blinkerPace = 0.1;  //seconds

void stop() {
  blinker.detach();
  digitalWrite(led1, 0);  // leds leuchten; wegen negativer Logik
  digitalWrite(led2, 0);
  digitalWrite(led3, 0);
  digitalWrite(led4, 0);
}
  
void change() {
  blinker.detach();
  blinkerPace = 0.5;
  blinker.attach(blinkerPace, blink);  
  changer.once(10, stop); // nach 10 sec wird blinken gestoppt
  }

void blink() {
  digitalWrite(led1, !digitalRead(led1));
  digitalWrite(led2, !digitalRead(led2));
  digitalWrite(led3, !digitalRead(led3));
  digitalWrite(led4, !digitalRead(led4));
}


void setup(){
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  digitalWrite(led1, 1);
  digitalWrite(led2, 0);
  digitalWrite(led3, 0);
  digitalWrite(led4, 1);
  delay(2000);
  blinker.attach(blinkerPace, blink);
  changer.once(5, change); // 5 sec, dann wird langsamer geblinkt  
}
 
void loop(){
}


#define led1 19       // GPIO19
#define led2 17       // GPIO17
#define led3 16       // GPIO16
#define led4 4        // GPIO04
 
void setup() { 
  Serial.begin(112500);
  delay(100);
  pinMode(led1, OUTPUT);
  digitalWrite(led1, 0);
  pinMode(led2, OUTPUT);
  digitalWrite(led2, 1);
  
  xTaskCreate(
    taskOne,          /* Task function. */
    "TaskOne",        /* String with name of task. */
    10000,            /* Stack size in bytes. */
    NULL,             /* Parameter passed as input of the task */
    1,                /* Priority of the task. */
    NULL);            /* Task handle. */
 
  xTaskCreate(
    taskTwo,          /* Task function. */
    "TaskTwo",        /* String with name of task. */
    10000,            /* Stack size in bytes. */
    NULL,             /* Parameter passed as input of the task */
    1,                /* Priority of the task. */
    NULL);            /* Task handle. */
}
 
void loop() {
  delay(1000);
}
 
void taskOne( void * parameter ) {
  for( ; ; ){
    Serial.println("Hello from task 1");
    digitalWrite(led1, !digitalRead(led1));
    delay(1000);
  }
  Serial.println("Ending task 1");
  vTaskDelete( NULL );
}
 
void taskTwo( void * parameter) {
  for( int i = 0;i<10000;i++ ) {
    Serial.println("Hello from task 2");
    digitalWrite(led2, !digitalRead(led2));
    delay(1000);    
  }
  Serial.println("Ending task 2");
  vTaskDelete( NULL );
}

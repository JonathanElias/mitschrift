# Ballerkennung

Um den Ball zu erkennen ist die Idee, einen CNY70 Fototransistor zu verwenden. Laut Datenblatt ist der Collectorstrom bei einer kurzen Distanz zum Objekt vor dem Fototransistor im Vergleich zu einer längeren Distanz groß.

![2020_01_27_cny70_datasheet01](2020_01_27_cny70_datasheet01.png)

Bei einem Collectorstrom von ca. 1mA  ist die Collector Emitter Spannung sehr hoch (=der Phototransistor sperrt), und bei einem Collectorstrom von ca. 0,1mA ist die Collector Emitter Spannung klein (unter 0,5V). Dieser Spannungsunterschied am Collector ist als Output nutzbar.

![2020_01_27_cny70_datasheet02](2020_01_27_cny70_datasheet02.png)
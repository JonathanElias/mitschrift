# Internetradio

- Internet
- Bluetooth
- FM Radio
- Lokale Medien
- Rotary Encoder
- Display
- ==RGB==

## Features

- Internet
  - Podcast
    - RSS/iTunes
  - YouTube
    - YouTubeDL?
  - Webradio
- Bluetooth
- FM Radio
  - RDS (Sendertitel)
- Lokale Medien
  - DLNA

## Steuerung

### Am Gerät

- Rotary Encoder, Display

### Extern

- Website/App (App die die Website aufruft)


# Werkstättenprotokoll Bogensperger Jonathan

## 30.9.2019

In der Green Village gibt es keinen Zugriff auf das Schulnetzwerk, und dieses Problem soll mit einem Mirkotik Router als Wireless Client behoben werden. Dafür muss man in der Mikrotik Router Konfiguration eine neue Bridge erstellen. Eine Anleitung für die Konfiguration findet man auf [https://wiki.mikrotik.com/wiki/Connect_to_an_Available_Wireless_Network](https://wiki.mikrotik.com/wiki/Connect_to_an_Available_Wireless_Network).

## 7.10.2019

### Netzwerk aufbauen

Die Computer auf einer Seite der Werkstatt sollen mit einem Mikrotik Router (mit eingebautem Switch) miteinander verbunden werden, und über zwei weitere Switches, wie in der Abbildung gezeigt, über eine Glasfaserleitung mit den PCs auf der anderen Seiter der Werkstatt verbunden werden.

![](../images/2019_10_27_netzwerk_aufbau.png)

## Port Forwarding

Auf je einem PC pro Werkstattseite soll ein Webserver gestartet werden, auf den die PCs auf der anderen Seite Zugriff haben sollen. Um auf den Webserver zuzugreifen, muss auf den Mikrotik Routen Port Forwarding konfiguriert werden. Diese Konfiguration heist auf den Mikrotik Routen "dstnat" (destination NAT). Eine Anleitung für die Konfiguration findet man auf [http://www.icafemenu.com/how-to-port-forward-in-mikrotik-router.htm](http://www.icafemenu.com/how-to-port-forward-in-mikrotik-router.htm). Um die Konfiguration zu testen, muss eine Person auf einer Seite der Werkstatt auf den Router der anderen Seite zugreifen. Als Adresse muss die öffentliche Adresse des Routers verwendet werden (die Adresse, die der Router im Netzwerk zwischen den beiden Routern hat).

## 14.10.2019

Am 14.10.2019 wurde wieder wie am 7.10. ein Portforwarding für einen Webserver eingerichtet. Diese Konfiguration wurde danach für Minetest umkonfiguriert. Minetest verwendet UDP statt TCP und es verwendet den Prot 30000 statt 8080 (oder 80).

## 21.10.2019

Am 21.10. wurde ein DNS Server mit bind9 konfiguriert.

### Installation

```bash
sudo apt install bind9
```

Wenn sudo nicht installiert ist, kann man sich zuerst mit `su` als root user anmelden, und dann den Befehl ohne sudo in den Terminal reinschreiben.

Konfiguriert wird der DNS Server in `/etc/bind`










# Deutsch lernen

### Textanalyse

#### Einleitung

- Titel
- Autor
- Wann erschienen
- Textsorte

#### Hauptteil

- Inhalt kurz
- inhaltliche Analyse
  - zentrale Aussage des Themas
- formale Analyse
  - Struktur und Gliederungen
  - Argumentationslinien
    - wie Argumentiert wird
  - Thesen
  - Verweis auf Expertinnen/Experten?
  - Begleitmaterial miteinbeziehen
- sprachliche Analyse
  - Sprache/stil
  - Wortwahl, Wortschatz, Fachvokabular
  - Satzbau
  - Stilfiguren
  - Leitwörter, Schlüsselbegriffe
  - Bezug um Titel/Überschrift
- Kommunikationsanalyse
  - Textfunktion
  - Textleistung
  - Nutzen für die Leser*innen
  - Wirkung auf Leser*innen

### Textinterpretation

#### Einleitung

- Eckdaten
  - Titel
  - Textsorte
  - Wo
  - Wann
  - Autor*in
- Thema des zu interpretierenden Textes
- Erster Ansatz in welche Richtung die Interpretation geht

### Hauptteil

- Inhalt kurz zusammenfassen
- strukturelle und sprachliche Analyse
- Interpretation

### Schluss

- Text gründlich bewerten
- beschreiben wie der Text auf die Leser*innen wirkt
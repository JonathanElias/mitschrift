# Textanalyse Schreiben Durchstarten S.136

- Hauptsächlich im Präsens
- Sachlich und Klar formulieren
- kein "ICH"
- auf den Text konzentrieren
- Fachvokabular verwenden, dass für eine Analyse üblich ist
- 405-495 oder 540-660 Wörter

## In eigenen Worten

Eine Textanalyse ist ein sachlicher Text, den man hauptsächlich im Präsens schreibt, der eine Struktur hat und nicht abschweift. Man analysiert wie der Autor den Text geschrieben hat.

## Kommentar vs. Textanalyse

- Kommentar ist um das Thema, Textanalyse um den Text selbst
- Textanalyse ist Subjektiv



- Textanalyse ist nicht wertend
- Leser werden nicht direkt angesprochen



- Alles, was du bei der sprachlichen Analyse beschreibst, unterlege mit passenden Beispielen aus dem Text. 

- sachlich, nüchtern, emotional, distanziert schwärmerisch, ironisch, anspruchsvoll, stilistisch gehoben, umgangssprachlich, vulgär
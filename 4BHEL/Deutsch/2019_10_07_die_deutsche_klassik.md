# Die Deutsche Klassik (1786-1832)

#### Goethe und Schiller

1786: erste Italienreise von Goethe

1832: Goethe Tod

klassisch kommt aus lat. `classicus`: römischer Bürger der höchsten Steuerklasse

`scriptor classicus`: Schriftsteller ersten Ranges

Ausdruck für zeitlos gültige, große künstlerische Leitung

#### Klassische Antike

- griechische Götter

#### Österreichisches Parlament in Wien

- Griechischen Parlament nachempfunden

- vor dem Parlament ist Pallas Athene

#### Englische Klassik

- früher
- 16. Jahrhundert
- Shakespeare (Christopher Marlowe (Kollege und Rivale))
- Elizabeth I.

#### Französische Klassik

- Ludwig XIV.
- Barok
- Moliere

#### Wiener Klassik

- Musikalische Richtunk
  - Josef Haiden
  - Wolfgang Mozart
  - Ludwig von Bethoven

#### zurück zur deutschen Klassik

- Anknüpfen an die Kunstauffassung der Renaissance
- Historischer Hintergrund: franz. Revolution 
  - Napoleon kommt an die Macht
- Ort: Weimar
- Grundgedanke bei Goethes Italienreise
  - In Italien wird Goethe mit der Klassischen Antike konfrontiert
- Herzog Karl August: aufgeklärter absolutistischer Monarch
  - gab 1816 als erster deutscher Landesherr seinem Land eine Verfassung
  - ca. 100.000 Einwohner
  - besonderes Interesse: Kunst und Wissenschaft
  - lud 1775 den 26-Jährigen Goethe nach Weimar ein

#### Grundideen der Klassik

- greift Idealmensch der Renaissance wieder auf
- der Mensch soll einem Ideal zustreben, das den Begriffen "Harmonie" und "Totalität" umschrieben wurde
- die Wirklichkeit wird als unzureichend betrachtet (streben Ideal (bestmöglicher unerreichbarer Zustand) zu)

## Goethe

#### Goethe übernimmt viele Reisen nach Italien

- Goethe verfasste auch naturwissenschaftliche arbeiten zu:
  - Botanik
  - Geologie
  - Chemie
    - vorläufiges Perodensystem
  - Optik
  - Zoologie
    - Entdeckt Zwischenkieferknochen beim Menschenembryo

## Friedrich von Schiller

- Musste in die Militärakademie eintreten
- Später ein Rechtsstudium
- führt ein eher ungeordnetes Leben
- wurde 1788 auf Betreiben Goethes als Geschichtsprofessor nach Jena berufen
- 1794 beginnt die Zusammenarbeit mit Goethe
- 1799 zieht Schiller nach Weimar

## Wirkung der Klassik auf das Schulsystem

- Wilhelm und Alexander von Humboldt
- Wilhelm:
  - arbeitet im preußischen Staatsdienst
  - leitet 1809 eine Reform des Schulwesens ein
  - besonderes Gewicht auf das Gymnasium
  - gründet die Universität in Berlin (Humboldt-Universität)

#### Schule soll nicht für einen bestimmten Beruf ausbilden

- Humboldt tritt für eine Gesamtschule bis 14 ein
- Methode des Unterrichts dürfte nicht von Drill und Auswendiglernen geprägt sein
- Erfolg dieser Bildung solle durch das Abitur überprüft werden


# Interpretation von Kurzgeschichten

## Merkmale von Kurzgeschichten

- Selten eine Einleitung
- Meist offener Schluss
- Wir erfahren wenig über die handelden Charaktere 
- Die Protagonisten sind meist Alltagspersonen und keine Helden
- Es kommen häufig Dialoge vor
- Der Höhepunkt am Ende mit oft überraschender Wende

## Kurzgeschichteninterpretation

1. Inhaltangabe
   - Personen (wie verhalten sich die Personen; die Beziehungen zwischen den Personen)
   - Ort
   - Zeit
   - Handlungsverlauf
2. Aufbau
   - Beginn am Ende der Geschichte
   - Typischer Aufbau?
   - Zeitsprünge
   - Überraschende Wendungen 
     - Kurz vor ende tritt oft eine überraschende Wende auf
3. Erzählweise
   - Auffälligkeiten in der Sprache
   - Satzbau (einfache oder komplex?)
   - Fremdwörter, wörtliche Rede
   - Rhetorische Mittel 
   - Zeitform (Gegenwart oder Vergangenheit)
   - Perspektive (auktoriale, peronale, neutrale oder Ich- Perspektive)
4. Deutung/Bewertung
   - Aussageabsicht
   - evtl. Hinterfründe des Autors/ der Epoche miteinbeziehen
   - Aussagen begründen

## Rhetorische Mittle

- Metapher
  - nicht anschauliche Begriffe werden mit einer Metapher umschrieben damit sie anschaulicher werden
  - `zB: jeamdnen das Herz brechen`
- Vergleich
  - ähnlich zu Metapher
  - Unterschied:
    - Bei Vergleich werden Vergleichspartikel verwendet (wie, als)
    - `Verleich: Er ist stark wie ein Löwe`
    - `Metapher: Er ist löwenstark`
- Wiederholung
  - mehrmaliges Benennen eines gleichen Wortes oder Folge von Wörtern
  - Wirkung: Verstärkung, bleiben mehr im Gedächtnis
  - `Bsp: Mein Gott mein Gott warum hast du mich verlassen`
- Parallelismus
  - Entsteht durch eine parallel Syntax
  - Wirkung: verstärkung
  - `Bsp: Das Schiffchen fliegt, der Webstuhl kracht.`
  - `Bsp: Ich bin reich du bist arm.`
- Antithese
  - Gegenüberstellung
  - Gegensätzliche Gedanken und Begriffe werden kombiniert
  - Wirkung: Kontrastierung
  - `Bsp: Der Geist ist willig, aber das Fleisch ist schwach.`
- Parataxe
  - Aneinanderreihung von selbstständigen Sätzen (Hautpsätze)
  - werden durch Satzzeichen oder Konjunktion voneiander getrennt
  - `Bsp: Moser ist krank. Er liegt im Bett.`
- Ellipse
  - Verkürzte, grammatikalisch nicht vollständige Sätze
  - unwichtige Teile werden ausgelassen
  - Wirkung:  Verstärkung
  - `Bsp: Erst die Arbeit, dann das Vergnügen`
  - `Bsp: Sonst noch was`
- Rhetorische Frage
  - Frage, die keine Antwort vom Gegenüber verlangt
  - Kommt in Funktion eher einer Aussage nahe
  - Wirkung: Verstärkung
  - `Bsp: Habe ich es dir nicht gesagt?`
  - `Bsp: Bist du noch bei Sinnen?`

## Erzählperspektiven

### Autoriale Erzähler

- "allwissender" Erzähler
- Außenperspektive
- Kann in Figuren hineinschauen, was sie denken, was sie fühlen
- ist auf räumlicher und zeitlicher Ebene allwissen (Rückblicke, Vorhausdeutungen)
- Kann den Leser ansprechen und das Geschehen von außen kommentieren

### Personaler Erzähler

- ist nicht allwissend
- Innenperspektive
- beschreibt aus der Perspektive einer einzelnen Figur
- Keine Vorausdeutung
- verwendet 3. person
- kommentiert Geschichte nicht

### Neutraler Erzähler

- bewertet die Handlung der Erzählung nicht und kommentiert sie nicht
- er beschreibt nur von außen
- keine Gefühle

### Ich Erzähler

- Sonderstellung
- Berichtet das Geschehen aus der Ich-Form
- Unterscheidung das erlebende und das erzählende Ich
- Kann ähnlich sein wie Personaler Erzähler
- Kann auch auktoriale Züge annehmen wenn der Ich-Erzähler eine Geschichte aus einem großem Zeitabstand erzählt

## Aufbau der Interpretation

### Einleitung

- Titel, Autor, Textsorte
- Kurze Zusammenfassung
- Erste Deutungshypothese

### Hautteil

- Alle vier Aspekte (Inhaltsangabe, Aufbau, Erzählweise, Deutung)
- Fließender Übergang
- Belege

### Schluss

- Zusammenhang
- Hypothese aufgreifen

### Die Deutungshypothese
- geht der Kernaussage des Textes auf den Grund
- Muss noch nicht belegt werden
- Ende der Einleitung
- Im Hauptteil wird versucht die Hypothese zu bestätigen
- Am Schluss sagen ob belegt oder widerlegt wenn widerlegt eine Alternative Interpretation
# Meinungsrede

## Roland Düringer WIR SIND WÜTEND !!!

### Thematik

- Redet von Mittelschicht
- Arbeit die nicht Berufung ist und nicht happy macht
- Wütend über das nichts tun der Politik
- Regt sich über fehlende Pressefreiheit auf
- Ausbildungssystem statt Bildungssystem

### Sprachlich

- sagt immer wir
- Hamsterrad als Metapher
- wird immer lauter

### Fragen

- was ist die Intention der Rede?
  - Zuhörer zum nachdenken über das nicht entkommen der Mittelschicht anregen
  - Ausbrechen aus den festgefahrenen Mustern
  - Es geht darum sich als Wutbürger der Mittelschicht grundsätzlich ins Gespräch zu bringen
- Aus welcher Perspektive?
  - aus der Perspektive der Mittelschicht
- Wie baut er die Rede auf?
  - Bringt Leute dazu sich in seine Lage zu versetzen mit dem "wir" und dadurch, dass er die Zuhörer*innen und sich als Mittelschicht bezeichnet
  - Wird im laufe der Rede lauter
  - Sein Vokabular wird vulgärer
- Welche Stilmittel
  - Metapher (hamsterrad)
  - Metapher (Das Wasser steht bis zum Hals)
  - Ironie, Vergleich (Karamellbonbon)
  - Metapher (Eier)
  - Vulgär und Umgangssprachlich (Eier)


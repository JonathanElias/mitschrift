# OPV Wiederholung

## Nicht invertierender Verstärker

![Nicht invertierender Verstärker](./images/2019_09_18_Nicht_invert_OPV.jpg)

$\frac{Ue}{R1} = \frac{Ua}{R1+R}$

$V=\frac{Ua}{Ue}=\frac{R1+R2}{R1}=\frac{R2}{R1}+1$

#### Bsp:

$R1=10k\Omega$

$R2=10k\Omega$

$V=2$

#### Bsp

V soll 1 sein

$R2=0\Omega$

$R1=\infty\Omega$

## Invertierender Verstärker

![Invertierender Verstärker](./images/2019_09_18_invert_OPV.jpg)



$\frac{Ue}{R1}=-\frac{Ua}{R2}$

$V=\frac{Ua}{Ue}=-\frac{R2}{R1}$

#### Bsp

$R2=10k\Omega$ 

$R1=10k\Omega$

$V=-1$

#### Bsp

$V=-120$

$R2=10k\Omega$

$R1=83,33\Omega$

# Zu beachten bei Verstärkerschaltungen

- Wie groß ist der Eingangswiderstand

![Eingangs Widerstand](./images/2019_09_18_EingangsR.jpg)

$Iin=\frac{Ue}{83\Omega}=\frac{10mV}{83\Omega}=120µA$

Ri soll möglichst groß sein, um die Ue nicht zu belasten

$Re=R1$

#### Ausgangs-R

$Iopv,max>Iout$

#### zb: 

laut Datenblatt 10mA

$Ia, gefordert=7mA$

$U2max=UB=9V$ => $R2min=\frac{U2max}{I2max}=\frac{9V}{3mA}=3k\Omega$

Ra soll möglichst klein sein (ist im OPV)




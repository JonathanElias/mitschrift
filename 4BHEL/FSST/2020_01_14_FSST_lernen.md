# FSST lernen

Datenbank erstellen:

```mysql
CREATE DATABASE db_namen;
```

Tabelle erstellen:

```mysql
CREATE TABLE kunden (
    kunden_ID INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(50)
);
```

Daten in Tabelle einfügen:

```mysql
INSERT INTO kunden ( Name, whatever)
	VALUES
	("hans", "whatever"),
	("peter", "whatever2");
```

Tabellen in einer Datenbank anzeigen:

```mysql
SHOW TABLES FROM db_namen;
```

Datensatz löschen:

```mysql
DELETE FROM kunden WHERE kunden_ID = 1;
```

Spalten von Tabelle anzeigen:

```mysql
SELECT kunden_ID, Name FROM kunden;
```

Zeile ausgeben:

```mysql
SELECT * FROM kunden WHERE kunden_ID = 1;
```

Spalte hinzufügen:

```mysql
ALTER TABLE bestellungen ADD COLUMN (Kunden_ID varchar(50));
```

Foreign Key hinzufügen: (braucht schon eine foreign keys spalte)

```mysql
ALTER TABLE Bestellungen ADD FOREIGN KEY (Kunden_ID) REFERENCES kunden(kunden_ID);
```

Select aus 2 tabellen die verknüpft sind:

```mysql
SELECT * FROM kunden, bestellungen WHERE kunden.kunden_ID = bestellungen.Kunden_ID;
```

Tabelleneintag ändern

```mysql
UPDATE kunden SET Name = "Gergö" WHERE kunden_ID = 5
```

```mysql
SELECT * FROM kunden, bestellungen WHERE kunden.name LIKE "%os%";
```


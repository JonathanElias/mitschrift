# Teststoff

- Signaldarstellung
  - Zeitbereich
  - Frequenzbereich
    - Liniendiagramm
      - einseitig
      - zweiseitig
        - alternativ: algebraische Form
    - Zeigerdiagram
      - einseitig
      - zweiseitig
  - mathem. Herleitung der Darstellung
- Fourierreihen
  - Satz von Fourier
  - Summenzeichen
  - Harmonische (=vielfache der Grundfrequenz)
- Amplitudenamplitation
  - Träger
    - TC - Transmitted carrier
    - RC - Reduced carrier
    - SC - Supressed carrier
  - Seitenband
    - DSB - Double Sideband
    - VSB - Vestigial Sideband
      - VLSB - vestigial lower sideband
      - VUSB - vestigial upper sideband
    - SSB - Single Sideband
      - LSB - lower sideband
      - USB - upper sideband

![2020_01_15_teststoff](images/2020_01_15_teststoff.jpg)


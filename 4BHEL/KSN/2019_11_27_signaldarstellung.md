# Signaldarstellung

Ein Signal ist ein Träger von Information.
Information ist ein Wissensinhalt, der Ungewissheit verringert.

Signal = Wissensdarstellung
Information = Wissensinhalt

## Einteilung von Signalen

#### Analog & Digital

- analog
  - unendlicher Wertevorrat
- digital
  - endlichen Wertevorrat

#### Zeitbereich & Frequenzbereich

- Zeitbereich
  ![](images/2019_11_27_zeitbereich.jpg)

- Frequenzbereich
  ![](images/2019_11_27_frequenzbereich.jpg)

Der Zusammenhang zwischen Zeit- und Frequenzbereich liefert die Fourier reihe. 

$y(t)=\sum\limits_{n=0}^{\infin}{A_{u} * cos(n*\omega t+\varphi_{n})}=\overline{A} _{0}+\sum\limits_{n=1}^{\infin}{A_{n}*cos(n\omega t+\varphi_{n})}$

$\overline{A}_{0}=A_{0}*cos(0+\varphi)=A_{1}*cos(\varphi_{1})$ = Gleichanteil

| Signal   | Fourier reihe                                                |
| -------- | ------------------------------------------------------------ |
| Sinus    | $A*cos(\omega t + \varphi)$                                  |
| Rechteck | $\frac{4A}{\pi}(cos(\omega t-\frac{\pi}{2})+\frac{1}{3} cos(\omega t-\frac{\pi}{2})+\frac{1}{5} cos(\omega t-\frac{\pi}{2})...)$ |
| Sägezahn | $\frac{2A}{\pi}(cos(\omega t-\frac{\pi}{2})+\frac{1}{2}cos(2\omega t-\frac{\pi}{2})+\frac{1}{3}cos(3\omega t-\frac{\pi}{2})+\frac{1}{4}cos(4\omega t-\frac{\pi}{2})...)$ |
| V1       | $\frac{2A}{\pi}(cos(\omega t-\frac{\pi}{2})+\frac{1}{2}cos(2\omega t+\frac{\pi}{2})+\frac{1}{3}cos(3\omega t-\frac{\pi}{2})+\frac{1}{4}cos(4\omega t+\frac{\pi}{2})...)$ |
| Dreieck  | $\frac{8A}{pi²}(cos(\omega t-\frac{\pi}{2})+(\frac{1}{3})^2*cos(3\omega t-\frac{\pi}{2})+(\frac{1}{5})^2*cos(5\omega t-\frac{\pi}{2})+(\frac{1}{7})^2*cos(7\omega t\frac{\pi}{2})$ |


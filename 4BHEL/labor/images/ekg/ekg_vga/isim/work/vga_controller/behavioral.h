////////////////////////////////////////////////////////////////////////////////
//   ____  ____   
//  /   /\/   /  
// /___/  \  /   
// \   \   \/  
//  \   \        Copyright (c) 2003-2004 Xilinx, Inc.
//  /   /        All Right Reserved. 
// /---/   /\     
// \   \  /  \  
//  \___\/\___\
////////////////////////////////////////////////////////////////////////////////

#ifndef H_Work_vga_controller_behavioral_H
#define H_Work_vga_controller_behavioral_H
#ifdef __MINGW32__
#include "xsimMinGW.h"
#else
#include "xsim.h"
#endif


class Work_vga_controller_behavioral: public HSim__s6 {
public:

    HSim__s1 SE[7];

HSim__s4 CJ;
HSim__s4 CR;
HSim__s4 CY;
HSim__s4 C15;
HSim__s4 C1c;
HSim__s4 C1j;
HSim__s4 C1s;
HSim__s4 C1z;
HSim__s4 C1G;
HSim__s4 C2d;
  HSimArrayType Rom_640base;
  HSimArrayType Rom_640;
  char *t0;
HSim__s4 Cdn;
    HSim__s1 SA[14];
    Work_vga_controller_behavioral(const char * name);
    ~Work_vga_controller_behavioral();
    void constructObject();
    void constructPorts();
    void reset();
    void architectureInstantiate(HSimConfigDecl* cfg);
    virtual void vhdlArchImplement();
};



HSim__s6 *createWork_vga_controller_behavioral(const char *name);

#endif

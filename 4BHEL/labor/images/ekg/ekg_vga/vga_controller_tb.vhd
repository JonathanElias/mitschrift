
--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:22:11 03/04/2020
-- Design Name:   vga_controller
-- Module Name:   E:/schule/BHEL4/LAB/ekg/ekg_vga/vga_controller_tb.vhd
-- Project Name:  ekg_vga
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: vga_controller
--
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends 
-- that these types always be used for the top-level I/O of a design in order 
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

ENTITY vga_controller_tb_vhd IS
END vga_controller_tb_vhd;

ARCHITECTURE behavior OF vga_controller_tb_vhd IS 

	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT vga_controller
	PORT(
		clk_i : IN std_ulogic;
		reset_i : IN std_ulogic;          
		h_sync_o : OUT std_ulogic;
		v_sync_o : OUT std_ulogic;
		red_o : OUT std_ulogic_vector(2 downto 0);
		green_o : OUT std_ulogic_vector(2 downto 0);
		blue_o : OUT std_ulogic_vector(1 downto 0)
		);
	END COMPONENT;

	--Inputs
	SIGNAL clk_i :  std_ulogic := '1';
	SIGNAL reset_i :  std_ulogic := '1';

	--Outputs
	SIGNAL h_sync_o :  std_ulogic;
	SIGNAL v_sync_o :  std_ulogic;
	SIGNAL red_o :  std_ulogic_vector(2 downto 0);
	SIGNAL green_o :  std_ulogic_vector(2 downto 0);
	SIGNAL blue_o :  std_ulogic_vector(1 downto 0);

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: vga_controller PORT MAP(
		clk_i => clk_i,
		reset_i => reset_i,
		h_sync_o => h_sync_o,
		v_sync_o => v_sync_o,
		red_o => red_o,
		green_o => green_o,
		blue_o => blue_o
	);

reset_i <= '0' after 1 ns;
clk_i <= not clk_i after 5 ns;

END;


--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:18:53 02/26/2020
-- Design Name:   clock_divider
-- Module Name:   E:/schule/BHEL4/LAB/ekg/clock_divider/clock_divider_tb.vhd
-- Project Name:  clock_divider
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clock_divider
--
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends 
-- that these types always be used for the top-level I/O of a design in order 
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

ENTITY clock_divider_tb_vhd IS
END clock_divider_tb_vhd;

ARCHITECTURE behavior OF clock_divider_tb_vhd IS 

	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT clock_divider
	PORT(
		clock_i : IN std_ulogic;
		reset_i : IN std_ulogic;          
		clock64_o : OUT std_ulogic
		);
	END COMPONENT;

	--Inputs
	SIGNAL clock_i :  std_ulogic := '1';
	SIGNAL reset_i :  std_ulogic := '1';

	--Outputs
	SIGNAL clock64_o :  std_ulogic;

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: clock_divider PORT MAP(
		clock_i => clock_i,
		reset_i => reset_i,
		clock64_o => clock64_o
	);
	reset_i <= '0' after 5 ns;
	clock_i <= not clock_i after 10 ns;
	
END;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:07:19 02/26/2020 
-- Design Name: 
-- Module Name:    clock_divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_divider is
    Port ( clock_i : in  STD_uLOGIC;
           reset_i : in  STD_uLOGIC;
           clock64_o : out  STD_uLOGIC);
end clock_divider;

architecture rtl of clock_divider is

	signal clock64 : std_ulogic;
	signal count : integer range 0 to 524287;
	
begin

	count_p: process(reset_i, clock_i)
	begin
		if(reset_i = '1') then
			count <= 0;
			clock64 <= '0';
		elsif(clock_i' event and clock_i = '1') then
			if(count = 390624) then
				count <= 0;
				clock64 <= not clock64;
			else
				count <= count + 1;
			end if;
		end if;
	end process count_p;
	
	clock64_o <= clock64;
end rtl;


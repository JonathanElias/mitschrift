----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:36:04 02/26/2020 
-- Design Name: 
-- Module Name:    address_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity address_counter is
    Port ( clock_i : in  STD_uLOGIC;
           reset_i : in  STD_uLOGIC;
           address_o : out  STD_uLOGIC_VECTOR (5 downto 0));
end address_counter;

architecture Behavioral of address_counter is
	signal count : std_logic_vector(5 downto 0);
begin
	
	counter_p: process (clock_i, reset_i)
	begin
		if(reset_i = '1') then
			count <= "000000";
		elsif(clock_i' event and clock_i = '1') then
			if(count = "111111") then
				count <= "000000";
			else
				count <= count + "000001";
			end if;
		end if;

	end process counter_p;
	
	address_o <= To_StdULogicVector(count);
end Behavioral;


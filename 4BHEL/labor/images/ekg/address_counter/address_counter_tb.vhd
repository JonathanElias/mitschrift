
--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:53:33 02/26/2020
-- Design Name:   address_counter
-- Module Name:   E:/schule/BHEL4/LAB/ekg/address_counter/address_counter_tb.vhd
-- Project Name:  address_counter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: address_counter
--
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends 
-- that these types always be used for the top-level I/O of a design in order 
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

ENTITY address_counter_tb_vhd IS
END address_counter_tb_vhd;

ARCHITECTURE behavior OF address_counter_tb_vhd IS 

	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT address_counter
	PORT(
		clock_i : IN std_ulogic;
		reset_i : IN std_ulogic;          
		address_o : OUT std_ulogic_vector(5 downto 0)
		);
	END COMPONENT;

	--Inputs
	SIGNAL clock_i :  std_ulogic := '1';
	SIGNAL reset_i :  std_ulogic := '1';

	--Outputs
	SIGNAL address_o :  std_ulogic_vector(5 downto 0);

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: address_counter PORT MAP(
		clock_i => clock_i,
		reset_i => reset_i,
		address_o => address_o
	);

	clock_i <= not clock_i after 10 ns;
	reset_i <= '0' after 5 ns;

END;


--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:46:21 02/26/2020
-- Design Name:   rom
-- Module Name:   E:/schule/BHEL4/LAB/ekg/rom/rom_tb.vhd
-- Project Name:  rom
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: rom
--
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends 
-- that these types always be used for the top-level I/O of a design in order 
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

ENTITY rom_tb_vhd IS
END rom_tb_vhd;

ARCHITECTURE behavior OF rom_tb_vhd IS 

	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT rom
	PORT(
		clock_i : IN std_ulogic;
		address_i : IN std_ulogic_vector(5 downto 0);          
		data_o : OUT std_ulogic_vector(5 downto 0)
		);
	END COMPONENT;

	--Inputs
	SIGNAL clock_i :  std_ulogic := '1';
	SIGNAL address_i :  std_ulogic_vector(5 downto 0) := (others=>'0');

	--Outputs
	SIGNAL data_o :  std_ulogic_vector(5 downto 0);

signal k : integer range 0 to 63;

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: rom PORT MAP(
		clock_i => clock_i,
		address_i => address_i,
		data_o => data_o
	);

	tb : PROCESS
	BEGIN
		wait for 5 ns;
		
		for k in 0 to 63 loop
			address_i <= to_stdulogicvector(conv_std_logic_vector(k,6));
			
			wait for 20 ns;
		end loop;

		wait; -- will wait forever
	END PROCESS;
	
	clock_i <= not clock_i after 10 ns;

END;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:38:44 02/26/2020 
-- Design Name: 
-- Module Name:    rom - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rom is
    Port ( clock_i : in  STD_ULOGIC;
           address_i : in  STD_uLOGIC_VECTOR (5 downto 0);
           data_o : out STD_uLOGIC_VECTOR (5 downto 0));
end rom;

architecture rtl of rom is
type rom_64 is array(0 to 63) of std_ulogic_vector(5 downto 0);
	
constant ekg : rom_64 := 

("001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001101",
 "001110",
 "001111",
 "001111",
 "010000",
 "010000",
 "010001",
 "010001",
 "010010",
 "010010",
 "010010",
 "010010",
 "010010",
 "010010",
 "010010",
 "010001",
 "010000",
 "001111",
 "001110",
 "001101",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001011",
 "000111",
 "000110",
 "001010",
 "010111",
 "101000",
 "111111",
 "101011",
 "100001",
 "010100",
 "000000",
 "000010",
 "000111",
 "001010",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001101",
 "001110",
 "001111",
 "001111",
 "001111",
 "001110",
 "001110",
 "001101",
 "001100",
 "001100",
 "001100");
begin
	rom_p: process(clock_i, address_i)
	begin
		
		if(clock_i'event and clock_i='1') then
			data_o <= ekg(Conv_Integer(To_Stdlogicvector(address_i)));
		end if;
	end process rom_p;
end rtl;


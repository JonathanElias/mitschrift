////////////////////////////////////////////////////////////////////////////////
//   ____  ____   
//  /   /\/   /  
// /___/  \  /   
// \   \   \/  
//  \   \        Copyright (c) 2003-2004 Xilinx, Inc.
//  /   /        All Right Reserved. 
// /---/   /\     
// \   \  /  \  
//  \___\/\___\
////////////////////////////////////////////////////////////////////////////////

#ifndef H_Work_rom_rtl_H
#define H_Work_rom_rtl_H
#ifdef __MINGW32__
#include "xsimMinGW.h"
#else
#include "xsim.h"
#endif


class Work_rom_rtl: public HSim__s6 {
public:

    HSim__s1 SE[3];

  HSimArrayType Rom_64base;
  HSimArrayType Rom_64;
  char *t0;
HSim__s4 C1t;
    Work_rom_rtl(const char * name);
    ~Work_rom_rtl();
    void constructObject();
    void constructPorts();
    void reset();
    void architectureInstantiate(HSimConfigDecl* cfg);
    virtual void vhdlArchImplement();
};



HSim__s6 *createWork_rom_rtl(const char *name);

#endif


--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   09:45:18 03/04/2020
-- Design Name:   ekg_audio
-- Module Name:   E:/schule/BHEL4/LAB/ekg/ekg_audio/ekg_audio_tb.vhd
-- Project Name:  ekg_audio
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ekg_audio
--
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends 
-- that these types always be used for the top-level I/O of a design in order 
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

ENTITY ekg_audio_tb_vhd IS
END ekg_audio_tb_vhd;

ARCHITECTURE behavior OF ekg_audio_tb_vhd IS 

	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT ekg_audio
	PORT(
		clock_i : IN std_ulogic;
		reset_i : IN std_ulogic;          
		data_o : OUT std_ulogic_vector(5 downto 0)
		);
	END COMPONENT;

	--Inputs
	SIGNAL clock_i :  std_ulogic := '1';
	SIGNAL reset_i :  std_ulogic := '1';

	--Outputs
	SIGNAL data_o :  std_ulogic_vector(5 downto 0);

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: ekg_audio PORT MAP(
		clock_i => clock_i,
		reset_i => reset_i,
		data_o => data_o
	);

	clock_i <= not clock_i after 10 ns;
	reset_i <= '0' after 5 ns;

END;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:55:23 03/04/2020 
-- Design Name: 
-- Module Name:    ekg_audio - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ekg_audio is
    Port ( clock_i : in  STD_ULOGIC;
           reset_i : in  STD_ULOGIC;
           data_o : out  STD_ULOGIC_VECTOR (5 downto 0));
end ekg_audio;

architecture rtl of ekg_audio is
	-- Signals
	signal clock64 : std_ulogic;
	signal address : std_ulogic_vector(5 downto 0);

----------------------------------------------------------------------
	-- Components
	-- clock divider
	component clock_divider
		port(clock_i, reset_i : in std_ulogic;
			  clock64_o : out std_ulogic);
	end component;
	
	-- address counter
	component address_counter
		port(clock_i, reset_i : in std_ulogic;
			  address_o : out std_ulogic_vector(5 downto 0));
	end component;
	
	-- rom
	component rom
		port(clock_i : in std_ulogic;
		     address_i : in std_ulogic_vector(5 downto 0);
			  data_o : out std_ulogic_vector(5 downto 0));
	end component;
----------------------------------------------------------------------
begin
	-- clock divider
	CD: clock_divider port map(clock_i => clock_i,
										reset_i => reset_i,
										clock64_o => clock64);
	
	-- address counter
	AD: address_counter port map(clock_i => clock64,
										  reset_i => reset_i,
										  address_o => address);
										  
	-- rom
	RO: rom port map(clock_i => clock64,
						  address_i => address,
						  data_o => data_o);


end rtl;


# Neuronale Netze

#### Neuronalen Netze im menschlichen Gehirn

- Synapsen geben Signale chemisch an Dendriten weiter
- eine Zelle hat viele Dendriten und Synapsen
- Dendriten laden den Zellkern
- Der Zellkern und das Umhüllende ist das Neuron
- Ein Axon ist am Neuron "angedockt"
- Wenn die Spannung im Neuron einen Schwellenwert, überschreitet entladet sich die Zelle
- an Synapsen kommt es zu einer Bio-Chemische Reaktion die ein Signal an den Dendrit abgibt  

![Hirnzelle](images/2019_10_08_hirnzelle.png)

## Menschliches Hirn im Vergleich zu Computern

#### Hirn

Anzahl der Neuronen: 10¹¹

Anzahl der Synapsen: 10¹⁴

#### Computer

Moores Law besagt, dass sich die Anzahl der Transistoren in einer Fläche alle 18 Monate verdoppelt (Jetzt ist das nicht mehr so)

## Neuronale Netze bei Firmen rechen lassen

Man kann neuronale Netze bei Firmen (zB. Google) rechnen lassen. Google hat spezielle Hardware für diese Art von Berechnungen.

## Neuron

- Eingang wird mit einer Gewichtung multipliziert

- Eingänge werden summiert

- Activation Function errechnet einen Ausgangswert

![Neuron](images/2019_10_08_neuron.png)

### Aktivierungsfunktion

![Aktivierungsfunktion](images/2019_10_08_aktivirungsfunktion.png)

#### Betrachtung von Aktivierungsfunktionen

1. Sigmoid

   $Sigmoid(x)=\frac{1}{1+e^{-x}}$

   für x = 100

   $e^{-100}=\frac{1}{e^{100}}=3,7*10^{-44}\approx0$

   $sigmoid(100)=\frac{1}{1+3,7*10^-44}\approx\frac{1}{1}=1$

   :arrow_right: wie groß ist e¹⁰⁰

   $sigmoid(-100)=\frac{1}{1+2,69*10^{43}}\approx0$

2. Relu (Rectified Linear Unit)

   ![Relu Funktion](images/2019_10_08_relu.png)

   für x<0: 0

   für x≥0: x
   
3. Schwellwertfunktion

   ![Schwellwertfunktion](images/2019_10_08_schwellwertfkt.jpg)

   für x >= t: 1

   für x < t:0

   Kann man auch anders realisiern

   ![bias](./images/2019_10_08_bias.jpeg)
   
4. Logisches Gatter mittels Neuronen

   ![OR](images/2019_10_08_or.jpg)

   $x=a_1*w_1+a_2*w_2+b$

   $step(x)=0$ für $x<0$

   $step(x) = 1$ für $x>=0$

   | A1   | A2   | OR   |
   | ---- | ---- | ---- |
   | 0    | 0    | 0    |
   | 0    | 1    | 1    |
   | 1    | 0    | 1    |
   | 1    | 1    | 1    |

   ges: w1, w2, b werte für OR

   w1=1; w2=1; b=-1;

   ges: w1; w2; b werte für AND

   w1=0,5; w2=0,5;b=-1;

5. Multilayer Network (Mehrlagiges Neuronales Netz)

   Sequentielles Neuronales Netz 4x3x2

   ![Sequentielles Neuronales Netz](images/2019_10_08_seqnet.jpg)

## Wie lernt ein neuronales Netzwerk?

zb: Bilderkennung: 

![Ziffernerkennung](images/2019_10_08_ziffernerkennung.jpg)

jeder Pixel ist ein Neuronen Eingang
Es werden Eingangs und Ausgangswerte gegeben
Der Optimierer verändert die Gewichtung zwischen den Neuronen


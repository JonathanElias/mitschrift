# Gerade und Ungerade Ziffernerkennung

## Aufgabenstellung

Die AI soll so angepasst werden, dass sie die NMIST Ziffern in gerade und ungerade einteilt. Dafür werden die Trainingsdaten und Testdaten des NMIST Beispiels angepasst.

## Code

```Python
model = Sequential()
model.add(Dense(128, activation="relu", input_shape=(784,)))
model.add(Dense(32, activation="sigmoid"))
model.add(Dense(2, activation="softmax"))	# Ausgangsneuronen von 10 auf 2 reduziert

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])
```

Das Beispiel der Ziffernerkennung hat 10 Ausgangsneuronen, für jede Ziffer eine. Für das Erkennen von geraden und ungeraden Zahlen braucht man aber nur zwei, eine für `gerade` und eine für `ungerade`. 

```Python
for i in range(len(y_train)):
  y_train[i]=y_train[i]%2

for i in range(len(y_test)):
  y_test[i]=y_test[i]%2
```

Mit diesen for Schleifen werden die Trainings- und Testdaten angepasst. Wenn die Ziffer gerade ist bleibt bei einer Division durch 2 ein Rest von 0. Deshalb sind die neuen Trainings- und Testdaten bei Ziffern die gerade sind 0. Bei ungeraden bleibt als Rest 1, deshalb sind in den Trainings- und Testdaten von ungeraden Ziffern die Zahl eins. 

Das Training des Netzes ist gleich wie im Ziffernerkennung NMIST Beispiel.

Mit `model.predict(x_test.reshape(-1,784))` wird das Netz getestet. Als Ausgang kommt eine zweidimensionale Liste. Sie hat zwei Spalten und so viele Zeilen wie es Tests gegeben hat. 

```Python
clpred = []
for i in range(0, 20):
    if(predictions[i][0] < 0.5):
      clpred.append(1)
    else:
      clpred.append(0)
    print("Below you see image number: " + str(i) + " with expected result = " + str(y_test[i]) + " and it is: " + str(clpred[i]))
```

Um den Erfolg der AI zu sehen, werden in dieser for Schleife die Sollwerte und Istwerte ausgegeben. Die Istwerte in `predictions` müssen noch in eine leserliche Form gebracht werden. Für diese Aufgabe wird das `if` verwendet.
Der Output bestätgt, dass das Netz die NMIST Ziffern in gerade und ungerade Ziffern einteilen kann:
![](images/2019_12_17_geradeUngeradeOutput.png)
(wie oben schon beschrieben, steht als expected result bei geraden Zahlen eine 0, und bei ungeraden eine 1)


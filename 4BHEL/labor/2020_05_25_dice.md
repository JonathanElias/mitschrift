# DICE Labor Projekt

## Aufgabenstellung

Es soll mit einem LFSR auf einer FPGA ein elektronischer Würfel entwickelt werden. Es soll ein Fibonacci und ein Galois LFSR in VHDL implementiert werden, zwischen denen man mit einem Schalter, der an das Eingangssignal <b>lfsr_select_i</b> verbunden ist, mit einem Multiplexer umschalten kann. Die LFSRs generieren Zahlen solange es einen Takt gibt, der mit einem Taktteiler erzeugt wird, den man mit <b>enable_i</b> aktivieren bzw. deaktivieren kann. Die Zahl aus dem Multiplexer soll dann auf einem 7 Segment Display ausgegeben werden. Dafür wird das Signal <b>seven_segment_o(6:0)</b> verwendet. Das Signal wird auch auf einer zusätzlichen Platine mit 7 LEDs ausgegeben, die in der Form von Würfelaugen angeordnet sind. Dafür wird das Signal <b>led(6:0)</b> verwendet.

### Blockschaltbild der Aufgabenstellung

![2020_05_25_blockschaltbild01](images/2020_05_25_blockschaltbild01.png)

## Abänderung der Aufgabenstellung

Um die Ergebnisse "zufälliger" zu machen haben die LFSRs in der hier dokumentierten Version eine Taktfrequenz von 50MHz was es zwar theoretisch nicht zufällig macht, aber, weil der Würfel von Menschen verwendet wird ist er doch zufällig, weil wir einen Taster nicht auf 20ns genau drücken können. Mit einer Frequenz von 50MHz würde man die LEDs am Würfel aber durchgehen eingeschaltet sehen, das gleiche gilt für die LEDs in der 7 Segment anzeige, deshalb wird mit einer Frequenz von 4Hz der Wert der LFSRs für 250ms gespeichert, um den Anwendern unterschiedliche Zahlen am Würfel anzuzeigen.

### Blockschaltbild der veränderten Aufgabenstellung

![2020_05_26_blockschaltbild2](images/2020_05_26_blockschaltbild2.png)

## VHDL

Für jeden Block im Blockschaltbild gibt es ein .vhd File, die als Components in einem File eingebunden werden.

```vhdl
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity dice is
    Port ( clock_i : in  STD_ULOGIC;
           reset_i : in  STD_ULOGIC;
           enable_i : in  STD_ULOGIC;
		   select_i : in  STD_ULOGIC;
		   select_digit_i : in STD_ULOGIC_VECTOR(3 downto 0);
           seven_segment_o : out  STD_ULOGIC_VECTOR (6 downto 0);
           anode_o : out  STD_ULOGIC_VECTOR(3 downto 0);
           dice_o : out  STD_ULOGIC_VECTOR (6 downto 0));
end dice;

architecture rtl of dice is
	-- signals
	signal clock4 : std_ulogic;
	signal lfsr_fib, lfsr_gal, lfsr_fib_sample, lfsr_gal_sample, lfsr_muxed : std_ulogic_vector(2 downto 0);

	-- components
	component clock_divider
		port(clock_i, reset_i : in std_ulogic;
			  clock4_o : out std_ulogic);
	end component;
	
	component decoder
		port(data_i : in std_ulogic_vector(2 downto 0);
			  select_digit_i : in std_ulogic_vector(3 downto 0);
			  anode_o : out std_ulogic_vector(3 downto 0);
			  seven_segment_o, dice_o : out std_ulogic_vector(6 downto 0));
	end component;
			  
	component lfsr
		port(clock_i, reset_i, enable_i : in std_ulogic;
		     serial_fib_o, serial_gal_o : out std_ulogic;
			  data_fib_o, data_gal_o : out std_ulogic_vector(2 downto 0));
	end component;
	
	component lfsr_sample
		port(clock_i, reset_i : in std_ulogic;
		     lfsr_fib_i, lfsr_gal_i : in std_ulogic_vector(2 downto 0);
			  lfsr_fib_o, lfsr_gal_o : out std_ulogic_vector(2 downto 0));
	end component;
			
	component mux
		port(enable_i, select_i : in std_ulogic;
		     lfsr_fib_i, lfsr_gal_i, lfsr_fib_sample_i, lfsr_gal_sample_i : in std_ulogic_vector(2 downto 0);
			  data_o : out std_ulogic_vector(2 downto 0));
	end component;
	
begin
	CD: clock_divider port map(clock_i => clock_i,
										reset_i => reset_i,
										clock4_o => clock4);
	
	LF: lfsr port map(clock_i => clock_i,
							reset_i => reset_i,
							enable_i => enable_i,
							serial_fib_o => open,
							serial_gal_o => open,
							data_fib_o => lfsr_fib,
							data_gal_o => lfsr_gal);
	
	SH: lfsr_sample port map(clock_i => clock4,
									 reset_i => reset_i,
									 lfsr_fib_i => lfsr_fib,
									 lfsr_gal_i => lfsr_gal,
									 lfsr_fib_o => lfsr_fib_sample,
									 lfsr_gal_o => lfsr_gal_sample);
	
	MX: mux port map(enable_i => enable_i,
						  select_i => select_i,
						  lfsr_fib_i => lfsr_fib,
						  lfsr_gal_i => lfsr_gal,
						  lfsr_fib_sample_i => lfsr_fib_sample,
						  lfsr_gal_sample_i => lfsr_gal_sample,
						  data_o => lfsr_muxed);
	
	DE: decoder port map(data_i => lfsr_muxed,
								select_digit_i => select_digit_i,
								anode_o => anode_o,
								seven_segment_o => seven_segment_o,
								dice_o => dice_o);
end rtl;
```

### Clockteiler

$clk=\frac{clock\_i}{n}$

$n=\frac{clock\_i}{clk}=\frac{50MHz}{4Hz}=12500000$

Im Zähler des Clockdividers zählt man bis $\frac{n}{2}-1$:

$\frac{12500000}{2}-1=6249999$

```vhdl
entity clock_divider is
    Port ( clock_i : in  STD_ULOGIC;
           reset_i : in  STD_ULOGIC;
           clock4_o : out  STD_ULOGIC);
end clock_divider;

architecture rtl of clock_divider is
	signal divider : std_ulogic;
	signal count : integer range 0 to 6249999;
begin
	count_p: process(reset_i, clock_i)
	begin
		if(reset_i = '1') then
			count <= 0;
			divider <= '0';
		elsif(clock_i' event and clock_i = '1') then
			if(count = 6249999) then	-- zählt bis 6249999 ändert den Pegel des Takts und setzt den Zähler auf 0
				count <= 0;
				divider <= not divider;
			else
				count <= count + 1;
			end if;
		end if;
	end process count_p;
	
	clock4_o <= divider;
end rtl;
```

### LFSRs

Im Würfel gibt es zwei LFSRs, ein Fibonacci und ein Galois LFSR. Gegeben ist das Polynom $P_{fib}(x)=x^3+x^2+1$ für ein 3 bit Fibonacci LFSR und das Zwillingspolynom $P_{gal}(x)$ ist für ein 3 bit Galois LFSR zu berechnen.

$P_{gal}(x)=x^{3-3}+x^{3-2}+x^{3-0}=x^3+x^1+1$

```vhdl
entity lfsr is
    Port ( clock_i : in  STD_ULOGIC;
           enable_i : in  STD_ULOGIC;
           reset_i : in  STD_ULOGIC;
           serial_fib_o : out  STD_ULOGIC;
           serial_gal_o : out  STD_ULOGIC;
           data_fib_o : out  STD_ULOGIC_VECTOR (2 downto 0);
           data_gal_o : out  STD_ULOGIC_VECTOR (2 downto 0));
end lfsr;

architecture Behavioral of lfsr is
	signal lfsr_fib_reg : std_ulogic_vector(2 downto 0);
	signal lfsr_gal_reg : std_ulogic_vector(2 downto 0);
begin
	lfsr_fib_p: process(clock_i, reset_i)
	begin
		if (reset_i = '1')then
			lfsr_fib_reg <= "111";
		elsif(clock_i' event and clock_i = '1')then
			if(enable_i = '1')then
				lfsr_fib_reg(2 downto 1) <= lfsr_fib_reg(1 downto 0);
				lfsr_fib_reg(0) <= lfsr_fib_reg(2) xor lfsr_fib_reg(1);
			end if;
		end if;
	end process lfsr_fib_p;
	
	lfsr_gal_p: process(clock_i, reset_i)
	begin
		if(reset_i = '1')then
			lfsr_gal_reg <= "111";
		elsif(clock_i' event and clock_i = '1')then
			if (enable_i = '1')then
				 lfsr_gal_reg(0) <= lfsr_gal_reg(2);
				 lfsr_gal_reg(1) <= lfsr_gal_reg(2) xor lfsr_gal_reg(0);
				 lfsr_gal_reg(2) <= lfsr_gal_reg(1);
			end if;
		end if;
	end process lfsr_gal_p;
	
	serial_fib_o <= lfsr_fib_reg(2);
	serial_gal_o <= lfsr_gal_reg(2);
	
	data_fib_o(2 downto 0) <= lfsr_fib_reg(2 downto 0);
	data_gal_o(2 downto 0) <= lfsr_gal_reg(2 downto 0);
end Behavioral;
```

### Sample and Hold

Wie bei einem Analogen Sample and Hold wird bei diesem Component der Wert bei der Taktflanke bis zur nächsten Taktflanke gespeichert. Das Sample and Hold wird wie ein D-FlipFlop implementiert.

```vhdl
entity lfsr_sample is
    Port ( clock_i : in  STD_ULOGIC;
           reset_i : in  STD_ULOGIC;
           lfsr_fib_i : in  STD_ULOGIC_VECTOR (2 downto 0);
           lfsr_gal_i : in  STD_ULOGIC_VECTOR (2 downto 0);
           lfsr_fib_o : out  STD_ULOGIC_VECTOR (2 downto 0);
           lfsr_gal_o : out  STD_ULOGIC_VECTOR (2 downto 0));
end lfsr_sample;

architecture rtl of lfsr_sample is

begin
	sample_p: process(clock_i, reset_i)
	begin
		if(reset_i = '1')then
			lfsr_fib_o <= "111";
			lfsr_gal_o <= "111";
		elsif(clock_i' event and clock_i = '1')then
			lfsr_fib_o <= lfsr_fib_i;
			lfsr_gal_o <= lfsr_gal_i;
		end if;
	end process sample_p;
end rtl;
```

### Mux

Im Mux wird das Signal <b>select_i</b>, mit dem man das richtige LFSR auswählt, und <b>enable_i</b> zum Signal <b>sel</b> mit Concatenation kombiniert.

```vhdl
entity mux is
    Port ( enable_i : in  STD_ULOGIC;
           lfsr_fib_i : in  STD_ULOGIC_VECTOR (2 downto 0);
           lfsr_gal_i : in  STD_ULOGIC_VECTOR (2 downto 0);
           lfsr_fib_sample_i : in  STD_ULOGIC_VECTOR (2 downto 0);
           lfsr_gal_sample_i : in  STD_ULOGIC_VECTOR (2 downto 0);
           data_o : out  STD_ULOGIC_VECTOR (2 downto 0);
           select_i : in  STD_ULOGIC);
end mux;

architecture rtl of mux is
	signal sel: std_ulogic_vector(1 downto 0);
begin
	sel <= enable_i & select_i;
	
	mux_p: process(sel, lfsr_fib_i, lfsr_gal_i, lfsr_fib_sample_i, lfsr_gal_sample_i)
	begin
		case sel is
			when "00" => data_o <= lfsr_fib_i;
			when "01" => data_o <= lfsr_gal_i;
			when "10" => data_o <= lfsr_fib_sample_i;
			when "11" => data_o <= lfsr_gal_sample_i;
			when others => data_o <= "111";
		end case;
	end process mux_p;
end rtl;
```

### Decoder

Im Decoder wird wird das Signal <b>data_i</b> in die Signale <b>seven_segment_o</b> und <b>dice_o</b> dekodiert.

```vhdl
entity decoder is
	Port ( data_i : in  STD_ULOGIC_VECTOR (2 downto 0);
		   select_digit_i : in STD_ULOGIC_VECTOR(3 downto 0);
           anode_o : out  STD_ULOGIC_VECTOR (3 downto 0);
           seven_segment_o : out  STD_ULOGIC_VECTOR (6 downto 0);
           dice_o : out  STD_ULOGIC_VECTOR (6 downto 0));
end decoder;

architecture rtl of decoder is

begin
	anode_o <= not select_digit_i;
	
	seven_p: process(data_i)
	begin
		case data_i is
			when "001" => seven_segment_o <= "1001111";
			when "010" => seven_segment_o <= "0010010";
			when "011" => seven_segment_o <= "0000110";
			when "100" => seven_segment_o <= "1001100";
			when "101" => seven_segment_o <= "0100100";
			when "110" => seven_segment_o <= "0100000";
			when "111" => seven_segment_o <= "0110000";
			when OTHERS => seven_segment_o <= "0110000";
		end case;
	end process seven_p;
	
	dice_p: process(data_i)
	begin
		case data_i is
			when "001" => dice_o <= "0001000";
			when "010" => dice_o <= "1000001";
			when "011" => dice_o <= "1001001";
			when "100" => dice_o <= "1010101";
			when "101" => dice_o <= "1011101";
			when "110" => dice_o <= "1110111";
			when others => dice_o <= "1111111";
		end case;
	end process dice_p;
end rtl;
```

## Simulation

Der Würfel wurde für 600ms simuliert, um zwei mal zu sehen, dass das Sample and Hold einen Wert der LFSR speichert, der dann auf dem 7 Segment Display und den LEDs ausgegeben wird.

![2020_05_25_dice_edit](images/2020_05_25_dice_edit.png)

\[1]: Das Signal <b>clock4</b> hat seine erste Taktflanke. der aktuelle wert des Fibonacci LFSR wird gespeichert, weil <b>select_i</b> auf 0 ist. 

\[2]: <b>enable_i</b> wird auf 0 gesetzt, und somit gibt der Mux nicht den gespeicherten Wert aus, sondern den aktuellen wert des Fibonacci LFSR.

\[3]: <b>enable_i</b> wird wieder auf 1 gesetzt, und die LFSRs generieren wieder Zahlen.

\[4]: (mit einem Pfeil gekennzeichnet) <b>select_i</b> wird auf 1 gesetzt. Ab diesem Zeitpunkt wird der Galois LFSR verwendet. 

\[5]: <b>enable_i</b> wird wieder auf 0 gesetzt und der aktuelle LFSR Wert wird ausgegeben.

## UCF File

```
net "clock_i" loc = "B8";
net "reset_i" loc = "M4";	# 2. taster von links auf basys2

net "enable_i" loc = "A7"; # 1. taster von links
net "select_i" loc = "P11"; # schalter rechts

net "seven_segment_o<0>" loc = "L14";
net "seven_segment_o<1>" loc = "H12";
net "seven_segment_o<2>" loc = "N14";
net "seven_segment_o<3>" loc = "N11";
net "seven_segment_o<4>" loc = "P12";
net "seven_segment_o<5>" loc = "L13";
net "seven_segment_o<6>" loc = "M12";
net "anode_o<0>" loc = "F12";
net "anode_o<1>" loc = "J12";
net "anode_o<2>" loc = "M13";
net "anode_o<3>" loc = "K14";
net "select_digit_i<0>" loc = "G3";
net "select_digit_i<1>" loc = "F3";
net "select_digit_i<2>" loc = "E2";
net "select_digit_i<3>" loc = "N3";

net "dice_o<0>" LOC = "C5"; # LED: LinksOben
net "dice_o<1>" LOC = "B6"; # LED: LinksMitte
net "dice_o<2>" LOC = "C6"; # LED: LinksUnten

NET "dice_o<3>" LOC = "B7";  # LED: Mitte Mitte

NET "dice_o<4>" LOC = "A9";  # LED: Rechts Oben 
NET "dice_o<5>" LOC = "B9";  # LED: Rechts Mitte 
NET "dice_o<6>" LOC = "C9";  # LED: Rechts Unten

```




# EKG Simulator

## Aufgabenstellung

Ein EKG Signal soll im Internet gefunden werden und manuell mit einer Auflösung von 6 Bit digitalisiert werden. Dieses EKG Signal soll in einen 64 Werte großen ROM in der FPGA eingetragen werden, und über einen DAC, der auf einem Erweiterungsboard ist, als Audiosignal ausgegeben werden. Die Herzfrequenz beim Ausgeben über den DAC soll 60 BPM haben (1 Hz). Dieses EKG Signal soll auch über den VGA Anschluss am Basys 2 Board auf einem VGA Monitor angezeigt werden. 

## Signal Digitalisieren

Ein Bild von einem EKG Signal, das ein Raster im Hintergrund hat, wurde auf einer Stockfotoseite gefunden, und die Werte wurden in Excel eingetragen. Das Signal das dabei entstanden ist, ist im Bild unten zu sehen.
 ![2020_03_04_EKG](images/2020_03_04_EKG.png)

## Ausgabe als Audiosignal

Um Das EKG Signal, das im ROM gespeichert ist auszugeben, muss man mit einem Zähler den Inhalt aller Adressen an den Pins die mit dem DAC verbunden sind anlegen. Dazu braucht man, wie im Blockschaltbild unten beschrieben, einen Clockdivider, der aus den 50 MHz vom Basys 2 Board 64 Hz macht, die man braucht um 64 Werte innerhalb einer Sekunde auszugeben (60 BPM (1Hz); 64 Werte/Herzschlag). Der Addresscounter zählt mit der Frequenz vom Clockdivider innerhalb von einer Sekunde von 0 bis 63 und gibt die Zahl an den Rom, der den Wert, den das Signal zu dem Zeitpunkt hat, zurück gibt.

![2020_03_04_blockschaltbild01](images/2020_03_04_blockschaltbild01.png)

### Clockdivider

Wenn man einen Clockdivider baut, muss man zuerst wissen, wie hoch man zählen muss, bevor man das Ausgangssignal toggelt.

$value=\frac{eingangsfrequenz}{2*ausgangsfrequenz}-1=\frac{50MHz}{2*64Hz}-1=390624$

#### VHDL Code

Der Clockdivider hat die 50 MHz und Reset als Eingänge, und der Ausgang ist das Taktsignal mit 64 Hz.

```VHDL
entity clock_divider is
    Port ( clock_i : in  STD_uLOGIC;
           reset_i : in  STD_uLOGIC;
           clock64_o : out  STD_uLOGIC);
end clock_divider;
    
architecture rtl of clock_divider is

	signal clock64 : std_ulogic; -- Extra Signal, weil man von clock64_o nicht lesen kann
	signal count : integer range 0 to 524287; 
	
begin

	count_p: process(reset_i, clock_i)
	begin
		if(reset_i = '1') then
			count <= 0;
			clock64 <= '0';
		elsif(clock_i' event and clock_i = '1') then
			if(count = 390624) then -- setzt count auf 0 wenn 390624 erreicht wird
				count <= 0;
				clock64 <= not clock64;
			else
				count <= count + 1;
			end if;
		end if;
	end process count_p;
	
	clock64_o <= clock64;
end rtl;
```

### ROM

Wenn man in VHDL einen ROM bauen möchte, braucht man einen eigenen Datentypen. Das wird mit `type rom_64 is array(0 to 63) of std_ulogic_vector(5 downto 0);` gemacht. Man definiert einen Datentypen, der ein Array aus 64 std_ulogic_vector(5 downto 0) ist. 

#### VHDL Code

```vhdl
entity rom is
    Port ( clock_i : in  STD_ULOGIC;
           address_i : in  STD_uLOGIC_VECTOR (5 downto 0);
           data_o : out STD_uLOGIC_VECTOR (5 downto 0));
end rom;

architecture rtl of rom is
type rom_64 is array(0 to 63) of std_ulogic_vector(5 downto 0);	-- eigener Datentyp
	
constant ekg : rom_64 := 

("001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001101",
 "001110",
 "001111",
 "001111",
 "010000",
 "010000",
 "010001",
 "010001",
 "010010",
 "010010",
 "010010",
 "010010",
 "010010",
 "010010",
 "010010",
 "010001",
 "010000",
 "001111",
 "001110",
 "001101",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001011",
 "000111",
 "000110",
 "001010",
 "010111",
 "101000",
 "111111",
 "101011",
 "100001",
 "010100",
 "000000",
 "000010",
 "000111",
 "001010",
 "001100",
 "001100",
 "001100",
 "001100",
 "001100",
 "001101",
 "001110",
 "001111",
 "001111",
 "001111",
 "001110",
 "001110",
 "001101",
 "001100",
 "001100",
 "001100");
begin
	rom_p: process(clock_i, address_i)
	begin
		
		if(clock_i'event and clock_i='1') then
			data_o <= ekg(Conv_Integer(To_Stdlogicvector(address_i))); -- [1]
		end if;
	end process rom_p;
end rtl;
```

##### Kommentare

\[1]: Die Adresse, die der ROM bekommt, ist std_ulogic_vector, aber der ROM braucht integer 

### Adresszähler

Der Adresszähler zählt pro Sekunde ein mal von 0 bis 63. In einer Sekunde werden somit alle 64 Samples aus dem ROM ausgelesen und man erhält eine Herzfrequenz von 60 BPM. Um bis 63 zu zählen braucht man einen 6 Bit Zähler. Für einen Zähler in VgHDL braucht man `use IEEE.STD_LOGIC_ARITH.ALL;` um die Addition durchzuführen.

### VHDL Code zum zusammenfügen der Components

Um ein das EKG Signal über das DAC Auszugeben muss man die Components wie im Blockschaltbild zusammenfügen. Man braucht auch 2 Signals (`clock64` (das 64 Hz Clocksignal) und address (6 bit Signal des Output vom Adresszähler)) um die Components zu verbinden.

### UCF File

Um das R2R-Netzwerk DAC, das auf einem Zusatzboard für das Basys2 Board ist zu verwenden, muss man das Audiosignal auf folgende Datenpins ausgeben:

```
net "data_o<0>" loc = "B5";
net "data_o<1>" loc = "J3";
net "data_o<2>" loc = "A3";
net "data_o<3>" loc = "B2";
net "data_o<4>" loc = "B6";
net "data_o<5>" loc = "C6";
```

Das restliche UCF File gibt an, an Welchen Pins die Clock angeschlossen ist, und an welchem Pin der Reset Taster hängt:

```
net "clock_i" loc = "B8";
net "reset_i" loc = "A7";
```
## EKG Signal auf einem VGA monitor ausgeben

Da der VHDL Code der VGA Ausgabe sehr  lange ist (22000 Zeilen) war ein Großteil schon gegeben, und der ROM  musste durch die eigenen ROM werte ersetzt werden (und verzehnfacht werden (64 Werte horizontal, 640 Pixel horizontal)). Eine VGA Ausgaben besteht im wesentlichen aus 5 Signalen und Ground, Rot, Grün, Blau, Horizontal Sync und Vertical Sync. Die Übertragung eines Bildes über die VGA Schnittstelle erfolgt wie eine Bildröhre es anzeigen würde. Rot, Grün und Blau sind analoge Werte, die links oben anfangen, und dann Linie für Linie nach Rechts unten angezeigt werden. Nach jeder Linie geht das Horizontal Sync Signal auf 0 und nach einer Gewissen Zeit Wieder auf 1. Das Passiert in einem nicht angezeigten Bereich der Linie (der Elektronenstrahl in einem Röhrenmonitor würde Zeit zum zurückkehren brauchen). Nach jedem Field (Bild) geht auch das Vertical Sync Signal auf 0.

#### VHDL Code

```vhdl
case h_counter is -- [1]
                   
 when "0000000000" => if(ecg(1) > ecg(0))then -- [2]
                          if((conv_integer(v_counter) >= (conv_integer(v_distance_c) - conv_integer(ecg(1))))
                             and (conv_integer(v_counter) <= (conv_integer(v_distance_c) - conv_integer(ecg(0)))))then -- [3]
                                                   red   <= "111"; 
                                                   green <= "100";
                                                   blue  <=  "00";
                          else
                                                   red   <= "000"; 
                                                   green <= "000";
                                                   blue  <=  "00";
                          end if;
                        elsif(ecg(1) < ecg(0))then -- [2]
                          if((conv_integer(v_counter) >= (conv_integer(v_distance_c) - conv_integer(ecg(0))))
                             and (conv_integer(v_counter) <= (conv_integer(v_distance_c) - conv_integer(ecg(1)))))then
                                                   red   <= "111";
                                                   green <= "100";
                                                   blue  <=  "00";
                          else
                                                   red   <= "000";
                                                   green <= "000";
                                                   blue  <=  "00";
                          end if;
                        else -- [2]
                          if(conv_integer(v_counter) = (conv_integer(v_distance_c) - conv_integer(ecg(0))))then
                                                   red   <= "111";
                                                   green <= "100";
                                                   blue  <=  "00";
                          else
                                                   red   <= "000";
                                                   green <= "000";
                                                   blue  <=  "00";
                          end if;
                        end if;
```

#### Kommentare

\[1]: case mit dem Horizontalen Zähler (der Zähler der verwendet wird um das Horizontal Sync zu erstellen)

\[2]: Überprüft ob der nächste Wert im ROM größer, kleiner oder gleich ist

\[3]: Überprüft, ob der Vertikale Zähler im Bereich zwischen dem aktuellen Wert vom ROM und dem nächsten Wert ist


# Entscheidungsträger

der Auftraggeber ist der der die Entscheidung über Aufnahme und abbrechen des Projekts trifft. Trägt auch die Kosten

#### Lenkungsausschuss

ist bei größeren Projekten sinnvoll. Der Ausschuss dient als übergeordnetes Entscheidungsgremium für alle Fachlichen und Projekt planerischen Fragen
Weitere Aufgaben:

- Ernennung des Projektleiters
- GenehmiguPrng und Planung des Budgets des Projektes
- externe Berater in Verbindung mit dem Auftraggeber und Projektleiter hinzuzuziehen
- fungiert als schlichtungs- und Entscheidungsgremium für die fälle, bei denen die Kompetenz des Projektleiters überschritten wird

#### Fachausschuss

der Fachausschuss hat keine Entscheidungskompetenz
Aufgabe eines Fachausschusses ist es beratend und unterstützend zu wirken. Der Fachausschuss sorgt für einen permanenten Informationsfluss zwischen den Projekt und den Fachabteilungen und initiiert Interaktionen die für beide Bereiche, Fachabteilung und Projekt profitabel sind.

#### Projektleiter

Der Projektleiter hat eine Einzelverantwortung.
Pflichten:

- Erreichung aller im Lastenheft angeführter Ziele
- Sicherstellung, dass der Projektauftrag ordnungsgemäß abgewickelt wird
- Führung des Projektteams
- Personalentwicklung im Team
  - Fortbildung
  - Wo brauch ich wann wie viele Personen

Aufgaben:

- Projektplanung und Projektcontrolling (Leistungen Termine Losten)
- Projektorganisation insbesondere Aufgabenverteilung
- Erarbeitung von Strategien und Maßnahmen zur Erreichung der ziel vorgaben
- Führung des Projektteams und Leitung der Teamleitungen
- Gestaltung der Kundenbeziehungen
- Gestaltung der Beziehung zu wichtigen Umfeldgruppen
- Risikomanagement
- Projektadministration und Dokumentation
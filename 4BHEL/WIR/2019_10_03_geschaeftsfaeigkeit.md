# Geschäftsfähigkeit

#### Fähigkeit gültige Verträge abzuschließen

Abhängig vom Alter und vom Geisteszustand

- 0-7 Jährige: Kinder
  - vollkommen geschäftsunfähig
  - außer "Taschengeldgeschäfte" altersübliche geringfügige Geschäfte
  - Verträge rückabwickelbar
- 7-14 Jährige : unmündige Minderjährige
  - beschränkt geschäftsfähig
  - "Taschengeldgeschäfte" (altersübliche geringfügige Geschäfte)
  - z.B.: kein Nintendo DS
  - Schenkungen nur ohne Verpflichtungen erlaubt
  - Keine Piercings und Tattoos
  - KEINE Internetgeschäfte
  - Gesetzliche Vertreter können ungültige Geschäfte auch nachträglich genehmigen und bis zur Genehmigung spricht man von einem hinkenden (schwebend unwirksam) Rechtsgeschäft
- 14-18 Jährige: mündige Minderjährige
  - erweitert geschäftsfähig
  - dürfen über Sachen die ihnen frei überlassen wurden und übers Einkommen frei verfügen
  - Grenze des freien Verfügens liegt bei Gefährdung des Lebensunterhalt 
  - Muss selbst für den Unterhalt aufkommen können (was dann überbleibt kann frei verfügt werden)
  - Ratenzahlungen dürfen eingegangen werden (ca. 10-15% des Gehalts)
  - Dienstleistungsverträge, Ferialverträge: ja
  - Lehr- und Ausbildungsverträge: nein (Eltern müssen mit unterschreiben)
  - ungültige Geschäfte können durch Zustimmung der Eltern (ein Elternteil reicht) nachträglich gültig gemacht werden
- 18+: Volljährige


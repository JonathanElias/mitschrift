#  Sachenrecht

## Unterteilungen

- Körperliche Sachen
  - Dinge die mit den Sinnen wahrnehmbar ist 
- Unkörperliche Sachen
  - Sind nicht mit den Sinnen wahrnehmbar
    - Patentrecht



- Bewegliche
  - Ohne Substanzverlust von A nach B bringbar
    - zB. Buch, Computer, Auto
- Unbewegliche Sachen
  - Mit Grund und Boden verbunden
  - Bewegliche Sachen können zu unbeweglichen werden (wenn sie miteinander verbunden werden (zB. Rohr in Wand einbauen))



- Vertretbare Sachen
  - können durch gleichartige ersetzt werden
- Unvertretbare Sachen
  - können nicht durch gleichartige ersetzt werden
    - zB: Gemälde



- Freistehende Sachen
  - haben keinen Eigentümer
  - gehören niemanden 
    - zB: Kastanien, Pilze
- Öffentliche und Private Sachen
  - Öffentliche Sachen gehören dem Staat
  - Private Sachen gehören einer Privatperson

## Begriffserklärung

- Innehabung
  - Wer, der eine Sache im Machtbereich, möchte sie aber nicht behalten zB: Gardarobe
- Besitz
  - Besitzer: jemand der die Sache im Machtbereich hat und behalten möchte 
    - zB: Dieb ist ein Besitzer (nicht rechtsmäßig) 
    - Sachbesitz, Rechtsbesitz
- Eigentum
  - Eigentümer hat das absolute Herrschaftsrecht über eine Sache

## Eigentumserwerb

Man braucht einen Titel + Modus

- Titel (Rechtsgund)
  1. derivativer Eigentumserwerb
     - Abgeleiteter Eigentumserwerb
     - Abgeleitet vom Vormann
       - Vormann war Eigentümer und überträgt mir die Rechte weiter
  2. originärer Eigentumserwerb
     - Ursprünglicher Eigentumserwerb
     - Von niemanden erworben
     - Aneignung
       - man kann sich freistehende Sachen (herrenlose Sachen) aneignen
       - Fund
         - man muss sich nicht um die Sache kümmern
         - unter 10€: örtlich bekannt geben
         - über 10€: zur behörde
         - Anspruch auf finderlohn
           - 10% bis 2000€
           - 5% vom Mehrbetrag
           - zB: 3000€ Fund: 250% Finderlohn
         - wenn sich der Eigentümer 1 Jahr nicht meldet kann man Eigentümer werden
  3. Gutgläubiger Eigentumserwerb
     - regelt Eigentumserwerb von einem nicht rechtsmäßigen Eigentümer
- Modus (Art des Erwerbs)
  - Bewegliche Sache
    - Körperliche Übergabe (Fernseher wird übergeben)
  - Unbewegliche Sache
    - Die Eintragung im Grundbuch


# Teststoff

## 2. Test

- Vertragsvoraussetzungen
- Wie man Vertrag abschließen kann
- Vertragsfreiheit
  - Inhalt kann alles was erlaubt ist und nicht gegen die guten Sitten verstoßt
  - Privatautonomie außer Kontrahierungszwang
- Dauerschuldverhältnis
  - Handyvertrag
- Zielschuldverhältnis
  - Kaufvertrag
- Bestandsvertrag
  - Miete und Pacht
# Fähigkeiten

## Rechtsfähigkeit

- Ist die Fähigkeit Rechte und Pflichten zu haben
- Man ist Rechtsfähig von Geburt bis zum Tod
  - Todeserklärung
    - Allgemeine Verschollenheit: 10 Jahre
    - Flugverschollenheit, Kriegsverschollenheit, Schiffsvershollenheit
    - Keine Nachrichten
    - Umstände müssen vorliegen die den Tod als wahrscheinlich erscheinen lassen
    - wenn doch nicht tot, muss alles wieder zurück abgewickelt werden
  - Für Organspenden ausdrücklich ablehnen
- unabhängig vom Alter und Geisteszustand

## Deliktsfähigkeit

- Die Fähigkeit für Delikte bestraft zu werden
- ab 14 (wenn der Geisteszustand in Ordnung ist)
- unter 14 ist die Aufsichtsperson verantwortlich
  - Aufsichtsperson ist nur verantwortlich wenn die Aufsichtsperson verletzt ist

#### Prüfung Deliktsfähigkeit 

1. Alter
   - Unmündiger Minderjähriger <14:arrow_right: Prüfung Aufsichtspflicht
   - Mündiger Minderjähriger >= 14 :arrow_right: Schadenersatz
2. Verletzung der Aufsichtspflicht?
   - Umfang der Aufsichtspflicht?
     - Alter, Eigenschaften, Entwicklungsstand
   - ja?:arrow_right: Schadenersatzzahlung der Aufsichtsperson
   - nein?:arrow_right: möglicherweise Haftung der Minderjährigen
3. Schadenersatzzahlung
4. Haftung des Minderjährigen
   - Einsichtfähigkeit trotz Alter gegeben?
   - Schadenstagungsfähigkeit? (muss ein Vermögen haben (Erbe/Haftpflichtversicherung))


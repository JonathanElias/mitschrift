![2020_02_24_uebungsblatt](images/2020_02_24_uebungsblatt.jpg)

1)

Mit 13 Jahren ist Irene beschränkt Geschäftsfähig, und darf deshalb nur altersübliche Geschäfte tätigen. Mit 13 Jahren ist ein Computer nicht altersüblich und es ist zu teuer, aber die Mutter hat zugestimt also ist es erlaubt. Die Mutter könnte dem Medizinstudenten aber auch sagen, dass sie mit dem Kauf nicht zustimmt, und, dass der Kauf schwebend unwirksam ist und rückabgewickelt werden muss.

Es handelt sich um einen C2C Verkauf. Das bedeutet, dass die Gewährleistung ausgeschlossen werden kann. Bei dem Beispiel wurde die Gewährleistung nicht ausgeschlossen. Der Fehler ist ein defekter Mangel. Der Fehler ist in den ersten 6 Monaten aufgefallen, deshalb liegt die Beweislast beim Verkäufer, dass das Produkt beim Kaufzeitpunkt nicht defekt war. Der Verkäufer hat gewusst, dass der PC kaputt war und hat ihn falsch beworben. Da das Motherboard nicht reparierbar oder austauschbar ist, kann Irene das Geld zurückverlangen. Der Verkäufer kann nicht mit "Gekauft ist gekauft"  argumentieren.

2) a)

Das Produkt entspricht nicht dem Vertrag, und ist deshalb ein Fehler. Sie kann zum Verkäufer gehen und der kann das Produkt ersetzen oder reparieren. Sie muss dafür nichts bezahlen.

b)

1. Nein
2. Durch Gewährleistung
3. 2 Jahre, nach 6 Monaten kehrt sich aber die Beweislast um

c) 

Wenn sie beweisen kann, dass der Fehler seit dem Kauf besteht, dann ja, aber das ist schwer

d)

1. Die Gewährleistung kann geändert werden, wenn die Erstzulassung des Autos vor mehr als einem Jahr war, die Änderung muss aber extra vereinbart werden
2. Ja weil sich die Beweislast noch nicht umgekehrt hat (6 Monate)
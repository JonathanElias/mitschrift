# Pfandrecht

zB: man verpfändet sein Auto, und bekommt Geld. Wenn man das Geld zurück gibt bekommt man das Auto zurück

Das Recht eines Gläubigers sich an einer Sache schadlos zu halten, wenn die zustehende Forderung nicht erfüllt wird.

## Wie kann ein Pfandrecht entstehen

- Vertrag
- Gesetz
  - Wenn man eine Mietwohnung mietet, und man nicht Miete bezahlen kann der Wohnungseigentümer das Eigentum pfänden
- Richterspruch
  - Gehaltspfändung

## Arten

- Kann an beweglichen und Unbeweglichen Sachen geben
- Unbeweglichen Sachen
  - Hypothek
- Bewegliche Sachen
  - Faustpfand

## Folgen

- Die Forderung wird erfüllt
  - Man bekommt die Sache zurück
- Die Forderung wird nicht erfüllt
  - Es erfolgt eine Versteigerung


# Begriffserklärung

## Rechtsordnung

- es braucht Regeln in der Gesellschaft
  - Regeln nennt man Normen
  - Normen regeln was erlaubt ist und welche strafen
- Sitte und Moral auch in der Rechtsordnung wichtig
  - Sitte und Moral werden von Gesellschaft bestimmt
- Unterschied zwischen Normen Sitten und Moral:
  - Normen können zwangsweise durchgesetzt werden
  - Sitten und Moral können nicht zwangsweise durchgesetzt werden

## Rechtssubjekt

- Sind Personen
  - Natürliche Personen 
    - Sind wir (Menschen)
  - Juristische Personen
    - Sind künstliche gebilde
    - zB: Aktien Gesellschaft
    - Juristische Personen des öffentlichen Rechts
      - Gebietskörperschaften: Bund, Land, Gemeinden, Kammern, Sozialversicherungsanstalt
        - Kammern: Interessensvertretung
        - Sozialversicherungsanstalt: Arbeitslosenversicherung, Gesundheitsversicherung, Pensionsversicherung, Unfallversicherung
    - Juristische Personen des privat Rechts
      - Kapitalgesellschaften: AG, GmbH, Vereine, Genossenschaften
        - Genossenschaften: große Mitgliederanzahl: bessere Preise für Mitglieder

## Rechtsobjekte

- Sachen 
- Tiere (teilweise)
  - Eigentumserwerb bei beweglichen Objekten:
    - Es braucht eine Körperliche übergabe
  - Eigentumserwerb bei unbewegelichen Objekten:
    - Es braucht eine Eintragung in Grundbuch

## Arten des Rechts

- Völkerrecht
  - Regelt Rechtsbeziehungen zwischen Staaten und übernationalen Organisationen
  - Völkerrechtsstatus haben auch die Menschenrechte
- Innerstaatliches Recht
  - Regelt Rechtsbeziehungen innerhalb eines Staates
  - Öffentliches Recht
    - Rechtsverhältnis zwischen Staat und privaten
      - Strafrecht, Steuerrecht, DSGVO
  - Privatrecht
    - Rechtsverhältnis zwischen zwei Privaten
      - Familienrecht
      - Heiratsrecht: ab 18 für eine Person & ab 16 mit Unterschrift der Eltern

## Stufenbau der Rechtsordnung

- Besteht aus Normen und die stehen zueinander in Stufen
- Stufen müssen ineinander Deckung finden
- Darunter liegende Stufe erklärt die darüber liegende Stufe genauer

### 7 Stufen der Rechtsordnung

1. Leitenden Grundsätze der Bundesverfassung
2. EU Gemeinschaftsrecht
3. Verfassungsgesetze
4. Einfachen Gesetze
   - ABGB Allgemeines Bürgerliches Gesetzbuch (alt) Hauptrechtsquelle
   - KSCHEG Konsumentenschutzgesetzt
   - STGB Strafgesetzbuch
   - ECG E-commerce Gesetz (Behandelt verkauf durch das Internet)
5. Verordnungen
   - Verordungen erklären die darüberliegenden Gesetze genauer
     - zB: Leistungsfeststellung in HTLs, Lehrpläne
6. Urteile, Beschlüsse und Bescheide
   - Urteile und Beschlüsse kommen von Richtern
   - Bescheid kommen von Verwaltungsbehörden
7. Exekution
   - Vollstreckung 

Verfassungsgesetze, Einfache Gesetze und Verordnungen sind allgemeine Rechtsnormen

Urteile, Beschlüsse, Bescheide und Exekution sind individuelle Rechtsnormen              
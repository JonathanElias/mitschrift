# Buchhaltung

## Doppelte Buchhaltung

Für mich selber Buchhalten und wissen wie hoch mein Gewinn ist.
Gewinn versteuern: Einkommenssteuer

### Vermögenswerte

![2020_06_08_vermoegenswerte_erklärung](images/2020_06_08_1.jpeg)

Linke Seite: Aktive Konten
Rechte Seite: Passive Konten

![2020_06_08_2](images/2020_06_08_2.jpg)

![2020_06_08_3](images/2020_06_08_3.jpg)

#### Beispiel 1

![2020_06_08_4](images/2020_06_08_4.jpg)

#### Beispiel 2

![2020_06_08_5](images/2020_06_08_5.jpg)

#### Beispiel 3

![2020_06_08_6](images/2020_06_08_6.jpg) 

![2020_06_08_7](images/2020_06_08_8.jpg)


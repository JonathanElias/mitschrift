# Schuldverhältnisse

## Entstehung von Schuldverhältnissen

- durch Rechtsgeschäft
  - Kaufvertrag
  - Schenkung
  - Arbeitsvertrag
- durch Rechtswidrige Handlung (gegen das Gesetz)
  - Sachbeschädigung
- Aufgrund des Gesetzes
  - Unterhalt
    - durch nicht-zusammenleben mit dem Kind muss das Elternteil an das andere Elternteil Unterhalt bezahlen

## Voraussetzung eines gültigen Rechtsgeschäfts

- Geschäftsfähigkeit
- übereinstimmende Willenserklärungen müssen vorliegen
  - Konsens
    - Schriftlich
    - Mündlich
    - Gesten (Handschlag)
    - Schlüssig
      - man kann aus meinem Verhalten den Schluss ziehen, dass ich den Vertrag abschließt
  - Schweigend
    - durch Schweigen kommt in der Regel kein Vertrag zustande
    - Ausnahme: Wenn man in ständiger Geschäftsbeziehung steht
  - Inhalt muss möglich und erlaubt sein
    - Es gibt Verträge die unmöglich sind
      - zB: Grundstücksverkauf am Mond
      - Stück von der Sonne
    - Erlaubt:
      - Vertrag darf nicht gegen das Gesetz verstoßen und nicht gegen die guten Sitten
  - Willenserklärung muss ernst gemeint sein und frei von Zwang
    - Scherzerklärungen sind nicht ernst gemein
    - List
  - Einhaltung von Formvorschriften
    - Bestimmte Verträge brauchen eine Bestimmte Form
      - Ehevertrag/Eheversprechen muss mündlich gemacht werden (vor Standesbeamten)
      - Bürgschaftserklärung muss Schriftlich gemacht werden
        - Bürgschaft: jemand tritt für die Schuld von anderen ein
      - Notariatsakt:
        - sämtliche Verträge zwischen **Ehegatten**

## Unbestellte Zusendungen

Es gibt keine Verpflichtungen für den Kunden es retour zu schicken, wenn falsch gesendet zb an Nachbarn dann zurück schicken oder den Nachbarn geben. Kann man wegwerfen.
# Arten der Rechtsgeschäfte

- Einseitige Rechtsgeschäfte
  - Kommt durch Willenserklärung von einer Person zustande
    - zB: Testament
      - Handschriftliche Testament braucht keine Zeugen
      - Fremdgeschriebene (Computer) braucht Zeugen
        - Gute Zeugen: 
          - Volljährig
          - Nicht eingeschränkt (Sinnlose)
          - Blind, Taub, Stumm geht auch nicht
          - Blind, Taub, Stumm geht auch nicht
    - zB: Kündigung
      - Kann von Chef, oder Arbeitsnehmer
      - braucht keine Gründe
      - fristlose Kündigung braucht Begründung
      - Arbeitsverweigerung kann zu Entlassung führen
      - Entlassung vs Kündigung
        - Kündigung hat Frist
- Zweiseitige Rechtsgeschäfte
  
  - Ein Vertrag
## Form
- Form freie Rechteschäfte
  
  - Siehe 2019_11_14_schuldverhaeltnisse
- Form pflichtige Rechtsgeschäfte
  
  - Siehe 2019_11_14_schuldverhaeltnisse
  
## Zielschuldverhältnis vs Dauerschuldverhältnis

  - Zielschuldverhältniss
    Einmalige Leitungserbringung
    - zB: Kaufvertrag
    - endet mit Erfüllung des Kaufvertrag
  - Dauerschuldverhältniss
    Mehrmalige Leistungserbringung
    - Arbeitsvertrag
    - Kreditvertrag
    - Zeitungsabo
    - endet wenn der Vertrag auslauft, oder wenn er gekündigt wird
    
# Arbeitsblatt Rechtsformen

1. a) Jahresumsatz: € 700.000
   b) ja
   c) nein, weil Herma Speiser nicht persönlich für Bucheggers Laden haftet
2. a) ja
   b) nein, Umsatz < € 700.000
   c) ja
   d) nein, Umsatz < € 700.000
3. a) ja, und b) ja weil Umsatz > € 700.000
4. a) ja, weil Umsatz > € 700.000
   b) ja
   c) nein, weil Landwirte von der € 700.000 Regel ausgenommen sind
5. a) ja
   b) ja, die GesmbH entsteht bei Eintragung ins Firmenbuch
6. a) nein, Umsatz < € 700.000
   b) ja, weil sie eine OG sind
7. a) Personengesellschaft
   b) Kapitalgesellschaft
   c) Kapitalgesellschaft
   d) Personengesellschaft
8. a) 
   Gesellschaftsform: Offene Gesellschaft
   VT: volle Kontrolle durch alle Gesellschafter
   NT: unbeschränkte  Haftung aller Gesellschafter
   Eintragung in Firmenbuch erforderlich: Ja
   Doppelte Buchhaltung wenn Jahresumsatz € 700.000 beträgt
   b)
   Gesellschaftsform: Kommanditgesellschaft
   VT: Kommanditisten haften nicht mit Privatvermögen, Kommanditist muss nicht in Entscheidungen eingebunden werden
   NT: Kommanditisten haben nur beschränkt Kontrolle
   Eintragung in FB erforderlich: ja
   Doppelte Buchhaltung wenn Jahresumsatz € 700.000 beträgt
9. Karin haften auch für die Lieferanten schulden aber nicht für die Bankschulden weil die Haftung 5 Jahre dauert
10. a) ja
    b) ja
    c) wenn  der Jahresumsatz > € 700.000 ist dann nicht
11. 
    ![2020_04_13_rechtsformen](images/2020_04_13_rechtsformen.png)
12. nein, zuerst wird aus dem Kapital in der Gesellschaft bezahlt
13. (b) Das Unternehmen muss zwar nicht eingetragen werden, Ingrid Müller kann sich jedoch freiwillig eintragen lassen.
    (c) Das Unternehmen kann freiwillig eine doppelte Buchhaltung führen.
14. a) ja [(muss in eine OG/KG umgewandelt werden](https://www.usp.gv.at/Portal.Node/usp/public/content/gruendung/gruendungsfahrplan_gesellschaften/gesellschaftsformen/gesbr/Seite.470212.html))
    b) ja
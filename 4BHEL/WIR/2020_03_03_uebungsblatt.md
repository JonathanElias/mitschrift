# Uebungsblatt

![2020_03_03_uebungsblatt_01](images/2020_03_03_uebungsblatt_01.jpg)

![2020_03_03_uebungsblatt_02](images/2020_03_03_uebungsblatt_02.jpg)

1. Fuchs kann verlangen, dass der Fehler behoben wird, weil der Fehler schon bei der Übergabe vorhanden war. Die Beweislast, dass der Fehler bei der Übergabe noch nicht vorhanden war, liegt beim Verkäufer, und es ist schwer das zu beweisen. gewährleistung
2. Der Händler hat gesagt, dass es rund 3 Wochen dauern wird, deshalb soll Kaufmann ihm noch Zeit geben. Nachfrist setzen. Wenn es dann noch immer nicht da ist, kann sie verlangen, dass es ersetzt wird/geld zurück weil man keine endlosen Reparaturen akzeptieren muss
3. Pammer kann eine Nachfrist setzen. Das bedeutet, dass er sagt, dass die Arbeit innerhalb einer Bestimmten Zeit gemacht werden muss, und wenn das nicht passiert, beauftragt er ein anderes Unternehmen. Pammer kann dann verlangen, dass die Kosten vom anderen Unternehmen vom ersten  Installateur bezahlt werden.
4. Fragen:
   1. 2 Jahre
   2. 3 Jahre. Die Fliesen sind unbewegliche Sachen, weil sie mit dem Haus verbunden werden
   3. 3 Jahre. Unbewegliche
5. Personenschaden können in der Produkthaftung nicht ausgeschlossen werden. Bei der Produkthaftung haften Hersteller und Importeur. Wenn der Verkäufer innerhalb von 14 Tagen den Hersteller nicht nennt haftet der Verkäufer
6. Ist die Sache in den ersten 6 Monaten beschädigt wird vermutet dass die sache zu beginn kaputt war
7. - primäre Gewährleistungsbehilfen
     - Reparatur
     - Austausch
   - Sekundäre Gewärhleistungsbehilfen
     - Wandlung (geld zurück)
     - Preisminderung
8. Gewährleistung, weil der Schaden schon ab Verkauf ist also Reparatur
9. Reparatur und austausch
10. Wenn der Schaden schon bei Verkauf war ist es Gewährleistung, vielleicht Garantie, Produkthaftung wenn ein Schaden durch den kaputten Blinker entsteht
11. Beschränkung
    1. nein
    2. ja
    3. ja, wenn ausdrücklich vereinbart
    4. ja (beschränkt und ausgeschlossen)
12. Die Gewährleistung auf das Ausgetauschte oder reparierte teil beginnt am 20.3.2012 wieder von vorne also ist er am 20.1.2014 noch in der Gewährleistung für die Bildröhre
13. Wenn er das am 20.1.2014 beansprucht ist er schon außerhalb der Gewährleistung für die Lautsprecher; Nur für die Bildröhre ist die Gewährleistung verlängert worden
14. Asans MP3 Player
    1. Garantie: Vertrag zwischen Käufer und Hersteller freiwillig
       Gewährleistung: Gesetzliche Haftung des Verkäufers gegenüber dem Käufer
    2. Dass der Schaden wieder gut gemacht wird. Wie das gemacht wird darf der Händler selbst entscheide
    3. 2 Jahre weil der MP3 Player beweglich ist. In den ersten 6 Monaten liegt die Beweislast beim Verkäufer also kann Asan den MP3 Player leicht repariert bekomen
    4. Weil  Asan dann beweisen muss, dass der Schaden schon beim Kauf war
15.  Nein, einem Vertrag kann man nicht schweigend zustimmen
16. Ja, weil beschränkt Geschäftsfähig und es ist nicht altersüblich. Es ist schwebend unwirksam und kann mit ihrer Zustimmung gültig werden. Wenn sie es nicht macht muss der Vertrag rückabwickelt werden
17. Ja, weil erweitert Geschäftsfähig und ein teurer laptop ist nicht altersüblich. Es ist schwebend unwirksam und kann mit ihrer Zustimmung gültig werden. Wenn sie es nicht macht muss der Vertrag rückabwickelt werden. Der Kauf würde sein Lebensunterhalt gefährden
18. Wenn der Kostenvoranschlag verbindlich ist, dann muss nicht mehr bezahlt werden. Ein Kostenvoranschlag ist immer verbindlich solange nichts anderes gesagt wird
19. Ja, weil die Heizung unbeweglich ist und da die Gewährleistung 3 Jahre ist
20. Wenn das Fahrrad noch in der Gewährleistung ist dann vom Verkäufer Ersatz fürs Rad; Mit der Produkthaftung für die Vase
21. Die Produkthaftung ist nicht an den Besitzer/Eigentümer des Produkts gebunden
22. Fahrlässig und vorsätzlich. Es gibt schwach und stark fahrlässig
23. Hersteller und Importeur, wenn nicht bekannt dann Verkäufer
24. Folgeschäden die durch das Produkt verursacht wurden
25. Kontrahierungszwang solange an die AGBs gehalten wird. Ein Unternehmen muss in einen Vertrag mit Personen gehen, die sich an die AGBs halten. Beförderungsunternehmen, Versorgungsunternehmen
26. bezahlen, stornieren und die entstandenen Kosten begleichen
27. sobald er es weis informieren, sie bezahlen


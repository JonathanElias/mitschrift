# Vertragsfreiheit

Nennt man auch Privatautonomie

Man ist frei Verträge abzuschließen bezüglich des Partners, Dauer...

## Einschränkungen

- Einschränkungen bezogen auf den Inhalt
  - Nicht gegen das Gesetz
  - Nicht gegen die guten Sitten
- Einschränkungen bezogen auf die Form eines Vertrages (bestimmt Verträge brauchen eine bestimmte Form)
- Einschränkungen bezogen auf die freie Wahl des Vertragspartners
  - Manche Unternehmen müssen mit Jedem der sich an die AGBs halten einen Vertrag abschließen
    - zB: Beförderungsunternehmen, Versorgungsunternehmen (Strom, Gas, Wasser)
  - nennt man Kontrahierungszwang (Kontrahieren ist mit jemanden Geschäfte machen)


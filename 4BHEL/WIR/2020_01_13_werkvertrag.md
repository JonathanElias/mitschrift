# Werkvertrag

## Kaufvertrag vs Werkvertrag

- Kaufvertrag kaufe ich etwas Fertiges
- Werkvertrag wird etwas angefertigt

Werkverträge werden oft aufgrund eines Kostenvoranschlags erstellt. 

### Kostenvoranschlag

Bekommt Kostenvoranschlag für zB 2000€ will nachher aber 3000€.
Wie viel bezahlen?

#### Verbindlicher vs Unverbindlicher Kostenvoranschlag

##### Verbindlicher Kostenvoranschlag

Es dürfen keine Mehrkosten verrechnet werden, außer wenn ich weitere Änderungen haben will.

##### Unverbindlicher Kostenvoranschlag

Toleranzgrenze: 10-15%
Wenn mehr als 10-15% mehr: muss mich informieren. Kann dann akzeptieren oder zurücktreten. Bei Rücktritt aber Aufwandsersatz.

#### Was wenn nicht gesagt ob verbindlich oder unverbindlich?

Default: Verbindlich (laut Konsumentenschutzgesetz) 
Kostenvoranschlag kostet auch nichts wenn nichts anders ausgemacht ist.


# Leistungsstörungen

Etwas wird beim Vertrag nicht richtig gemacht

## Gewährleistung

- Ist eine Haftung des Verkäufers für:
  - Fehler die eine Sache im Zeitpunkt der Übergabe hat
- Gewährleistung gibt es nur bei Entgeltlichen Geschäften

### Was ist ein Fehler

- Fehler ist wenn die Sache nicht dem Vertrag entspricht
- Die Sache hat nicht gewöhnlich vorausgesetzte Eigenschaften
- Die Sache entspricht nicht den öffentlichen Äußerungen
  - Ernst gemeinte, produktspezifische Aussagen
- Haftung für Montagemängel
  - Wenn ich was selber zusammenbaue und die Anleitung schlecht ist, ist es eine Gewährleistungssache
- Verdecke und offene Mängel
  - Offen:
    - Mängel die man sofort sieht
  - Verdeckt:
    - Mängel die sich nach zeit zeigen

### Beweislastumkehr

- ersten 6 Monate: Verkäufer muss beweisen, dass der Mangel beim Verkauf nicht da war
  - Wenn die Sache im ersten 1/2 Jahr kaputt geht, geht man davon aus, dass es vom Anfang an kaputt war
  - Ausnahme: Produkte mit geringer Haltbarkeit
- restlichen 18 Monate: muss Käufer beweisen, dass der Mangel von Anfang an da war

### Was kann man fordern im Rahmen einer Gewährleistung?

- je nach Wert der Sache wird repariert oder ausgetauscht
  - Verkäufer muss die Chance haben den Fehler wieder gut zu machen
  - Nach Reparatur beginnt bezüglich des reparierten Teils die Gewährleistung neu
  - Man braucht keine endlosen Reparaturen akzeptieren
- Reparatur oder Austausch nicht möglich: :arrow_forward: sekundäre Möglichkeiten
  - Rückabwicklung des Vertrages (Wandlung)
  - Preisminderung

==Wenn ich streiten gehen will:==
==§§ 932 ABGB==

#### Wenn nicht repariert wird:

- Nachfrist setzen (Wenn Sie nicht x innerhalb von y zeit machen dann wird ein Ersatzunternehmen beauftragt)

- Eine Andere Firma beauftragen (Beweissicherung!!!)

### Wie erfolgt die Fehlerbehebung?

Die Fehlerbehebung erfolgt GRATIS! Sämtliche durch die Fehlerbehebung entstandenen Kosten trägt der Verkäufer!

### Dauer der Gewähleistung

- Bewegliche Sachen: 2 Jahre
- Unbewegliche Sachen: 3 Jahre
  - Arbeit an einer unbeweglichen Sache auch 3 Jahre

Gewährleistung beginnt zum laufen bei Übergabe der Sache

Bei verdeckten Fehler: Wann der Fehler objektiv erstmals feststellbar ist

### Abänderung der Fristen

#### B2B Business to Business

Gewährleistung abändebar

#### B2C Business to Customer

- Neue Sachen: keine Gewährleistungsbeschränkung möglich

- Gebrauchte Sachen: Verkürzung auf 1 Jahr möglich wenn ausdrücklich vereinbart (nur in AGBs reicht nicht) 
  - Ausnahme: Gebrauchte Autos: auf 1 Jahr verkürzt werden

#### C2C Customer to Customer

- Grundsätzlich ja
- Außnahme: kann ausgeschlossen werden muss aber ausdrücklich gesagt werden

## Garantie

Die Garantie ist eine freiwillige Haftung und ist nicht gesetzlich geregelt. Eine Garantie wird meistens vom Hersteller gegeben. Der Inhalt der Garantie ist nach individueller Vereinbarung. Bei der Garantie ist es normalerweise egal wann der Fehler auftritt.

## Schadenersatz

### Voraussetzungen

1. Schaden muss gegeben sein

	- Sachschaden
	- Personenschaden (Schmerzensgeld)
2. Verursachung (Causalität)
   - Äquivalenztheorie (ob der Schaden wegfallen würde wenn man sich das Verhalten wegdenkt, oder ein unterlassen dazudenkt)
   - Adäquanztheorie (man haftet nur für adäquate Schäden (man haftet nur für Schaden die durch das Verhalten normalerweise entsteht)) (man haftet nicht für unlogische Verkettungen)
3. Rechtswidrigkeit
   - Zum Schadenersatz werde ich nur verpflichtet wenn mein Verhalten Rechtswidrig war (zB: gegen StVO, gegen Vertrag)

==AUSNAHME für Rechtswidrigkeit: Notwehr== Notwehr ist das gelindeste mittel um eine Gefahrensituation zu vermeiden 

4. Verschulden
   - Fahrlässigkeit
     - ==Sorgfallsverstoß==
       - leichte Fahlässigkeit
         - kann leicht passieren
         - Vermögensschaden wird ersetzt
       - grobe Fahrlässigkeit
         - man muss schon sehr unvorsichtig sein
         - Vermögensschaden und entgangener Gewinn wird ersetzt
   - Vorsatz
     - wissen und wollen einer Tat (ich weis was ich mach und will des auch)
     - höherer Schadensersatz
     - Vermögensschaden, entgangener Gewinn und Liebhaberwert (emotionaler Wert)

#### Wie lange kann man sich auf Schadenersatz stützen?

3 Jahre nach Kenntnis des Schadens; maximale Frist: 30 Jahre

## Produkthaftung

- Sonderform des Schadenersatzes

- Wird für Schäden, die durch ein Fehlerhaftes Produkt entstehen

- Produkthaftung deckt sowohl Personen als auch Sachschaden ab
- Wer haftet?
  - Hersteller
  - Importeur
  - eventuell auch Verkäufer
    - wenn er weder den Hersteller oder den Importeur in einer Angemessenen Frist benennen kann
- Verschuldens unabhängige Haftung
- Zahlungen
  - Bei Sachschäden gibt es einen Selbstbehalt von 500€
  - Für Personenschäden kann die Produkthaftung NICHT ausgeschlossen werden
  - geschützt sind alle, die durch das Produkt verletzt worden sind
- Dauer
  - 3 Jahre ab Kenntnis des Schadens; maximale Frist: 10 Jahre ab Inverkehrbringen des Produkts
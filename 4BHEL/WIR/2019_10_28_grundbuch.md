# Grundbuch

Das Grundbuch ist ein öffentliches Verzeichnis, in das alle unbeweglichen Sachen eingetragen werden

- wird an Bezirksgerichten geführt

## Einteilung

- Grundstücksdatenbank
  - 
- Urkundensammlung
  - Gliedert sich in 3 Blätter:
  - A-Blatt:
    - Gutsbestandsblatt
    - Steht alles drinnen was mit dem Grundstück zu tun hat
    - Größe, Adresse, Widmung
      - Widmung: für was es gedacht ist ist
    - KG: Katastralgemeinde 
  - B-Blatt:
    - Eigentumsblatt
    - Finden sich alle Informationen bezüglich des Eigentümers
    - Name, Eigentumsanteil
    - Eventuelle Persönliche Beschränkungen
      - zB: Geistige Einschränkung, Alter
  - C-Blatt:
    - Lastenblatt
    - Stehen alle Belastungen drinnen
    - Kredite
    - Hypotheken
    - Vorkaufsrecht:
      - Eine Person muss vorher gefragt werden ob er es kaufen will
    - Servituten
      - Nutzungsrechte an Fremden Sachen

## Prinzipien im Grundbuchrecht

- Publizitätsprinzip
  - Für die Öffentlichkeit frei Zugänglich
  - Gegen Datenschutz aber es ist wichtiger, dass es zugänglich ist
- Prioritätsprinzip
  - "Wer zuerst kommt mahlt zuerst"
  - Wenn 2 Leute im Grundbuch stehen, gilt der frühere Eintrag
- Vertrauensprinzip
  - Man darf auf den Stand des Grundbuchs vertrauen

## Arten von Grundbuchseintragungen

- Vormerkung
  - Wenn eine Urkunde noch nicht alle Formerfordernisse
  - Diese Urkunden müssen beim Notar gemacht werden
- Anmerkung
  - wichtige Umstände werden dort eingragen
    - Konkurseröffnung
    - Bestellung eines Sachwalters
- Einverleibung
  - Mit der Einverleibung ist man definitiv Eigentümer des Grundstücks
  - Grundbucheintragung kann 1/2 Jahre dauern

## Treuhänder

- Üblicherweise ein Jurist
- Ist eine Person der der Käufer das Geld gibt und nach dem Verkauf gibt der Treuhänder dem Verkäufer das Geld


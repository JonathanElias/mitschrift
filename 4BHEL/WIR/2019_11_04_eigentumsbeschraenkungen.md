# Eigentumsbeschränkungen

## Eigentumsbeschränkungen im Öffentlichen Interesse

- Baurecht
  - Flächenwidmungsplan
  - Höhe
- Naturschutzgesetze
- Denkmalschutz
- Forstgesetz
  - zB: Zugang zum Wald muss für Erholungszwecke gesichert werden
- Enteignung
  - "Wegnehmung" des Eigentums im öffentlichen Interessee
  - nur vom Staat vornehmbar
  - gegen angemessene Entschädigung

## Eigentumsbeschränkungen im Privaten Interesse (Immissionen)

- Einwirkungen durch Rauch, Staub, Lärm, Wärme

Immissionen können verboten werden wenn sie das ortsübliche Maß überschreiten


# Verschiedene Verträge

## Kaufvertrag

- Regelung finden sich im ABGB
- Was muss drinnen stehen
  - Ware
  - Preis
- Erfüllter Kaufvertrag alleine überträgt noch kein Eigentum

## Bestandsvertrag

- Immobilien sind Bestandsobjekte
- Beispiele
  - Miete 
    - Miete ist bloßer Gebrauch
  - Pacht
    - Pacht hat eine Wirtschaftliche Nutzung

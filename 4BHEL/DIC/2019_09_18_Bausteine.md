# Bausteine

## Portexpander

Besitzt der µC zu wenige I/O-Leitungen (Ports), kann man durch Verwendung eines Portexpanders weitere Leitungen hinzufügen.

`Port: Eine Anzahl (meist 8) zusammengehöriger Leitungen, die als Ein- oder Ausgänge verwendet werden. Die Nummerierung erfolgt von 0 (LSB(2⁰)) bis 7 (MSB(2⁷)).`

Der einfachste Portexpander ist ein Schieberegister, das einen Takt- und einen Dateneingang sowie 8 Ausgänge besitzt.

## 74HCT164 Schieberegister:

![74HCT164](./images/2019_09_18_74HCT164.jpg)

8 Stück D-Flipflops

"serial in/parallel out shift register"

Zwei Portleitungen des µC  steuern 8 Ausgänge des Schieberegisters.

Arbeitsweise: Der µC schiebt ein Byte beginnend mit dem MSB in das Register. Pro Bit ist ein Taktimpuls notwendig.

#### Vorteile:

- 2 Datenleiten :arrow_right: 8 Ausgänge
- mehrere Bausteine können kaskadiert (=hintereinander) geschaltet werden

#### Nachteile:

- Langsamer als die direkte Ansteuerung über die Ports eines µC

### Kaskadierung:

![Kaskadierung 74HCT163](./images/2019_09_18_Kaskadierung.jpg)

#### Frage:

Wie hoch darf die Taktfrequenz sein?

Bei $UB = 4,5V$ beträgt die maximale Taktfrequnz 31MHz.

#### Frage:

Wie lange dauert die Übertragung eines Byts in das Schieberegister?

$f=31MHz$

$T=\frac{1}{31*10⁶}=32,3ns$

$tbyte = 8T=8*32,3ns=258ns$

Datenrate: $r=\frac{nbytes}{sec}$

$r=\frac{1}{258ns}=3,8MB/s$

## Das funktioniert so nicht!

#### Warum?

Weil sich die Ausgänge des Schieberegisters während des Schiebeprozesse ständig ändern und dies unerwünschte bzw. unerlaubte Schaltvorgänge zur Folge hätte

![Schieberegister Problem](./images/2019_09_18_Shift_Register_Problem.jpg)

#### Wir benötigen ein Schieberegister mit einem Zwischenspeicher

## 74HCT595

![74HCT595](./images/2019_09_18_74HCT595.jpg)

#### Funtktion der Pins:

DS: Data Serial :arrow_right: serieller Dateneingang (D eines D-FF)

SHCP: Shift register Clock Pulse :arrow_right: Takteingang des Schieberegisters, die Daten werden während der steigenden Flanke in das Schieberegister übernommen.

M̅R̅: Master Reset :arrow_right: Alle Bits des Schieberegisters werden auf Null gesetzt.

Q7S: Serieller Ausgang des Schieberegister (MSB)

STCP: Storage Register Clock Pulse :arrow_right: Takteingang des Zwischenspeichers, die Inhalte des Schieberegisters werden bei er steigenden Flanke in den Zwischenspeicher übernommen.

O̅E̅: Output Enable :arrow_right: O̅E̅ = 0 :arrow_right: an den Ausgängen erscheinen die Bitwerte des Zwischenspeichers O̅E̅ = 1 :arrow_right: Tristate

Q0...Q7: Ausgänge

Wird die Tristate-Funktion der Auusgänge nicht benötigt, kann OE mit Masse (GND) verbunden werden.

## Ansteuerung durch die Software

- Schleife von 1...8:
  - erstes bzw. nächstes Datenbit an DS anlegen
  - Taktleitung auf 1 legen
  - Taktleitung auf 0 legen
- STCP auf 1 legen
- STCP auf 0 legen


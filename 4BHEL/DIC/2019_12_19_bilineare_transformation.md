# Bilineare-Transformation

```Matlab
%% Bilinear-Transformation 19.12.2019

clc;
close all;
clear all;
```

Die Ausgabe von Matlab wird mit `clc;` , `close all`; und `clear all;` geleert.

```matlab
R = 10e3; % R=10kOhm
C = 0.2e-6;    % C=0,2µF
fa = 20e3;
```

Die Werte des Filters der Bilinearen Transformation sind $R=10k\Omega$ und $C=0,2µF$. Die Abtastfrequenz ist $f_a=20kHz$.

```matlab
z = [1];		% Zähler
n = [R*C 1];	% Nenner
G = tf(z,n)   	% Übertragungsfunktion G(s)
```

###### Ausgabe:

```
G =
       1
  -----------
  0.002 s + 1
```

In diesem Code wird der Zähler und der Nenner angegeben, aus dem die Übertragungsfunktion errechnet wird.

```matlab
[zz, nz] = bilinear(z,n,fa); % zz nz = Tähler und Nennner in z Bilineartransforamtion von Zähler z und Nenner n in s
```

Mit `bilinear` wird in Matlab die Bilineare Transformation durchgeführt. 

```matlab
H = tf(zz, nz, (1/fa))    % Übertragungsfunktion H(z)
```

###### Ausgabe

```
H =
  0.01235 z + 0.01235
  -------------------
      z - 0.9753
```

Die von Matlab errechnete Übertragungsfunktion ist gleich wie die händisch letzte Woche errechnete.

```matlab
figure(1);
zplane(zz, nz)  % PN Diagram im z-Bereich
```

###### Ausgabe

![ausgabe von zplane](images/2019_12_19_zplane.png)

Die ausgabe von `zplane` ist das PN-Diagramm im z-Bereich.

```matlab
figure(2);
freqz(zz,nz)    % Amplituden & Phasengang für dig. Übertragungsfunktionen
```

###### Ausgabe

![Amplituden und Phasengang](images/2019_12_19_ampl_phasengang.png)

```matlab
fvtool(zz, nz)  % Filter-Vizualizing Tool
```

###### Ausgabe

![Ausgabe von fvtool](images/2019_12_19_fvtool.png)

Mit `fvtool` wird ein Fenster erstellt in dem Man verschiedene Arten von Diagrammen ansehen kann.

#### Impulsantwort

![impulsantwort](images/2019_12_19_impulsantwort.png)

#### Sprungantwort

![sprungantwort](images/2019_12_19_sprungantwort.png)
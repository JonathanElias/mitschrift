# I²C Bus Fortsetzung 

# Datenübertragung am Beispiel des EEPROMs 24LC246

![2020_03_12_einleiten](images/2020_03_12_einleiten.jpg)

Diese Sequenz leitet sowohl Lese- als auch Schreibzugriffe ein!

<b>Fortsetzung beim Schreiben</b>

![2020_03_12_schreibarten](images/2020_03_12_schreibarten.jpg)

<b>Fortsetzung beim Lesen</b>

![2020_03_12_lesearten](images/2020_03_12_lesearten.jpg)

Bei Lesen muss zusätzlich noch ein mal die Startbedingung gesendet werden, gefolgt vom Control Byte, diesmal mit $R/\overline{W}=1$ .
Der Master muss den Empfang der Bytes mit ACK quittieren.
Die Struktur der Übertragung hängt vom Baustein ab.

==Besonderheit des 24LC256: Das "ack-polling"==
Warum?
Der Schreibvorgang im Baustein dauert länger als die Übertragung I²C Bus. Ist diese beendet, hat der Baustein noch nicht genügend Zeit gehabt, die Daten dauerhaft zu speichern.

- Der Master beendet den Schreibvorgang ohne ein Ack vom EEPROM erhalten zu haben.
- Das EEPROM startet den Schreibvorgang intern nach der Stoppbedingung 
- der Master muss durch senden einer Startbedingung und des Control bytes (R/WN=0) feststellen, ob der interne Schreibvorgang schon beendet ist
- Hat das EEPROM den Schreibzyklus abgeschlossen, antowortet es dem Master mit ACK, andernfalls gibt es kein ACK aus.
# Schnittstellen

![2020_01_16_schnittstellen](images/2020_01_16_schnittstellen.jpg)

==unidirektional==: nur in eine Richtung
==bidirektional==: in beide Richtungen

==vollduplex==: gleichzeitiges Senden 
==halbduplex==: abwechselndes Senden

==zeichenorientiert==: zeichenwiese Übertragung
==blockorientiert==: Übertragung in Blöcken

==seriell==: eine Leitung für die aufeinander folgende Übertragung von Bits
==parallel==: jedes Bit besitzt eine private Leitung

==synchron==: die Gültigkeit eines Bits wird mit einem Taktsignal festgelegt (steigende Flanke, fallende Flanke, Pegel)
==asynchron==: ohne Taktsignal, der  Takt wird in einem Zeitraster "versteckt"

==asymetrisch==: Es gibt eine gemeinsame Masseleitung für alle Signale
==symetrisch==: jedes Signal besitzt seine eigene Rückleitung

![2020_01_16_symetrisch](images/2020_01_16_symetrisch.jpg)


# µP-Peripheriebausteine

## Überblick

#### 80er Jahre:

µP = CPU

#### Heute:

µC = CPU + Peripherie

Peripherie (I/O = Input/Output)

- RAM: Schreib-/Lesespeicher
  - Random Access Memory
  - Random = "direkter Zugriff" auf Daten, ohne vorher andere lesen zu müssen
- ROM: Festwertspeicher
  - Read Only Memory
  - Flash-ROM, EEPROM
  - EEPROM = elektrisch beschreibbarer Festwertspeicher
- Portexpander
  - Port: 8-Bit breite Ein-/Ausgabe Schnittstelle
  - Kann als Ein- oder Ausgang konfiguriert werden
  - Anschließen von LEDs (Ausgang)
  - Anschließen von Tastern (Eingang)
- Echtzeituhren -> Pufferbatterie!!!
- EEPROMs
- A/D- und D/A-Wandler (DACs und ADCs)
- Netwerk-Controller

## Bauarten eines Portanschlusses

![Ports](./images/2019_09_11_Ports.jpg)

Zu (2):

Verwendung als Ausgang

- 1 bzw. HIGH ausgeben durch Sperren des Transistors
- 0 bzw. LOW ausgeben durch Leitentmachen des Transistors

Verwendung als Eingang

- Transistor sperren
- Probleme:
  - Wird HIGH ausgegeben, darf der Portpin nicht zu stark belastet werden, da sonst der Logikpegel in den ungültigen Bereich absinkt.

#### gültige und ungültige Pegel:

![Logikpegel](./images/2019_09_11_logikpegel.jpg)

#### Anschluss von LEDs und Schaltern

![LED](./images/2019_09_11_LED.jpg)

![Schalter](./images/2019_09_11_Schalter.jpg)

![Open collector/Drain](./images/2019_09_11_OpenCollector.jpg)

- Benötigt immer einen Pullup-Widerstand
- Kann die Leitung nur gegen Masse ziehen
- Vorteile:
  - LEDs können direkt geschalten werden
  - Es können mehrere Ausgänge dieser Art zusammengeschaltet werden

![Erklärung Open Drain](./images/2019_09_11_OpenDrainErklaerung.jpg)

#### Anwendung

- Bus-System
  - I²C-Bus

![Bus vs Stern](./images/2019_09_11_BusvsStern.jpg)

- Interruptleitung
  - Interrupt <-> Polling

![Push-Pull Ausgang](./images/2019_09_11_Push-Pull.jpg)


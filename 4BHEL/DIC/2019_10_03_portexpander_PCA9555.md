# Portexpander PCA 9555/Ports

## Register

![Reigister](./images/2019_10_03_Register.jpg)

- Configuration Register: Jede Portleitung kann unabhängig als Ein- (=1) oder Ausgang (=0) konfiguriert werden. Nach dem Reset/Einschalten sind beide Ports als Eingänge definiert. 
  Conf. Reg.: 0b 1111 1111 (0b = Prefix für binär)

- Input Register: Spiegeln die Pegel an den Porpins wider.

- Output Register: der Wert wird auf den Portpins ausgegeben

- Polarity Inversion Register: 
  Beispiel: Taster als Eingabe
  ![Taster](./images/2019_10_03_Taster.jpg)

  Diese Schaltung gibt die Spannung "richtig" wider.
  ![P I Reg](./images/2019_10_03_PIReg.jpg)
  Steht an einer Bitposition im Polarity Inversion Register eine Eins, wird der Pegel des zugehörigen Portpins invertiert an das Input Register weitergegeben.

  #### Interrupt-Ausgang

  Am Interruptausgang wird Low-Pegel ausgegeben, wenn sich die Zustände an den Eingangspins ändern.

  
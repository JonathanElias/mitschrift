# Motorregler L293DD

##### Ansteuerung von Gleichstrommotoren mit Kommentatoren (Stromwender nicht bürstenlose Motoren)

Bei Gleichstrommotoren ist der Zusammenhang zwischen Spannung und Drehzahl linear.

$U=k*n*\phi$

Phi: Magnetischer Fluss (konstant)

n: Drehzahl

k: "Maschinenkonstante" (Aufbau)

U: Spannung (duh)

$n=\frac{U}{k*\phi}$ porportional zu $U$ $(=k'*x)$

$n$ proportional zu $\frac{1}{\phi}=\frac{1}{x}$

## Drehzahlstellung

![](./images/2019_10_03_vorwidsteuerung.jpg)

#### Verlustlose Steuerung? PWM!

Pulsweitenmodulation

![PWM](./images/2019_10_03_PWM.jpg)

Der  Motor wird mit einer konst. Rechtecksfrequenz mit variablen Tastgrad angesteuert.

Tastgrad = 0...1 = 0%...100%

#### Die Drehzahl des Motors wird über den arithmetischen Mittelwert der Spannung gesteuert.

![Durchsnittsspannung](./images/2019_10_03_udurchschnitt.jpg)

#### Beispiel:

Batteriespannung $U_B=12V$

Trägerfrequenz $f=100Hz=\frac{1}{T}$

Gesucht ist der Tastgrad, damit die Spannung am Motor $3V$ ist.

$\overline{u}=U_B+\frac{\tau}{T}$ :arrow_forward: $\tau=\frac{\overline{u}T}{U_B}=\frac{3V*0,01s}{12V}=0,0025s=2,5ms$

$\tau=\frac{T}{4}=\frac{0.01s}{4}=2,5ms$

#### Wie muss ich einen Motor für ein Elektrofahrzeug ansteuern? 

#### Betriebsfälle?

- beschleunigen, antreiben :arrow_right: Motor: :arrows_clockwise:
- bremsen :arrow_right: Motor: :arrows_clockwise:
- beschleunigen :arrow_left: Motor: :arrows_counterclockwise:
- bremsen :arrow_left: Motor: :arrows_counterclockwise:

$M=k'*I*\phi$

$U=k*n*\phi$

#### "Vierquadrantenkennlinienfeld"

![4 Quadrantenbetrieb](./images/2019_10_03_4Qbetrieb.jpg)

#### Einquadrantenbetrieb (1. Quadrant)

![](./images/2019_10_03_einquadr.jpg)

#### Wo funktioniert PWM?

- Motoren
- Heizkörper
- Beleuchtung

:arrow_up: "Trägheit"

Kriterien für die Trägerfrequenz:

- Motoren: hoch genug, damit der Motor rund läuft
- eventuelle akustische Vorgaben


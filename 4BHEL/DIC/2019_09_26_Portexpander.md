# Portexpander mit Input

## 74xx165 

![74xx165](./images/2019_09_26_74xx165.jpg)

D⁰...D⁷: Dateneingänge

$\overline{PL}$=Parallel Load, asynchron, die Daten werden übernommen wenn $\overline{PL}$=0 ist

DS = Data Serial: Serieller Dateneingang zum

$\overline{CE}$=Clock Enable:Taktfreigabe für das Schieberegister

![AND](./images/2019_9_26_ClockAND.jpg)

CP = Clock Pulse: Takt für das Schieberegister

Q⁷, $\overline{Q}$⁷ = serielle Ausgänge

![Q7 zu DS](./images/2019_09_26_Q7zuDS.jpg)

## Portexpander PCA 9555

#### Eigenschaften:

- 16 Ein-/Ausgangsleitungen
- Interruptleitung: Gibt einen Interrupt aus wenn sich der Pegel an den Eingängen ändert.
- Versorgungsspannung von 2,3V-5V
- jedes Port besitzt ein Configuration Register, ein Input Register, ein out Output Register und ein Polarity Inversion Register
- 5V-tolerante Eingänge
- 400kHz I²C-Bus
- 3 Adressanschlüsse ermölichen die Verwendung von bis zu 8 Bausteinen
- LEDs können direkt angesteuert werden

#### Vor- und Nachteile des OpenDrain-Interrupt System

![Interrupt](./images/2019_09_26_Interrupt.jpg)

Vorteil: Ändern sich die Pegel an den Eingängen des PCA9555, wird dies über den $\overline{INT}$-Ausgang der CPU mitgeteilt. Die CPU kann dann den betreffenden Baustein sofort abfragen.

Nachteil: Sind mehrere Bausteine mit dem Interrupt verbunden, muss die CPU alle in Frage kommenden ICs abfragen.

#### Warum sind nach einem Reset bzw. nach dem Einschalten alle Portleitungen Eingänge?

Für den Chiphersteller ist es nicht klar, ob der Systementwickler einen bestimmten Anschluss als Ein- oder Ausgang verwenden will. 

Beispiel: An den Pin P03 ist ein Ventil angeschlossen. Dieses wird geöffnet, indem man log. 1 an den Steuereingang legt. 

Wichtig: Beim Einschalten bzw. nach einem Reset muss das Ventil geschlossen sein!

Schaltung:

![Schaltung](./images/2019_09_26_VentilBSP1.jpg)

Nach dem Einschalten (Reset) ist P03 ein Eingang: hochohmig, kein definierter Pegel

#### Lösung Pullup

![Schaltung](./images/2019_09_26_VentilBSP2.jpg)

Durch einen Pullup-Widerstand wird der Pin an einen Definierten Pegel gelegt. Leider stimmt dieser nicht mit dem gewünschten Ruhepegel des Ventils überein, daher ist ein zusätzlicher Inverter notwendig. 

#### Pins des PCA 9555

SCL, SDA: I²C-Bus

$\overline{INT}$: Interrupt-Ausgang

P00...P07: Port 0 8 Bit Breit

P10...P17: Port 1 8 Bit Breit

A0, A1, A2: Adressierungseingänge

#### Wozu gibt es A0, A1 und A2?

Schnittstelle: Bus :arrow_right: mehrere Bausteine an den 2 Leitungen

Ansprechen eines bestimmten Bausteins mit seiner Adresse. Bei einer Übertragung am I²C -Bus wird als erstes ein Command Byte übertragen, das die Adresse enthält.

#### Command Byte

![AdressPins](./images/2019_09_26_AdressPins.jpg)

Die Bits 1 bis 3 spiegeln die Pegel an den Pins A0 bis A3 wider. Der Schaltungsentwickler kann diese 3 Pins auf +UB oder Masse legen.

#### Welcher Adressbereich ist möglich?

![Adress-Bereich](./images/2019_09_26_I2C_AdressBereich.jpg)

#### Welche Inforamtion steckt im Bit 0 (LSB)?

R/$\overline{W}$= Read/$\overline{Wirte}$

R/$\overline{W}$= 1...Lesen(aus der CPU)

R/$\overline{W}$=0...Schreiben

Nach dem Command Byte weiß der Baustein, dass er angesprochen ist und ob er Daten aufnehmen oder abgeben muss.


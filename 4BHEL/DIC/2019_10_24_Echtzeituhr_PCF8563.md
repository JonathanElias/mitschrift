# Echtzeituhr PCF8563

#### Wozu benötige ich eine Echtzeituhr (real time clock)?

- die aktuelle Uhrzeit ist immer verfügbar, die CPU wird mit keinen Rechenvorgängen belastet.
- RTCs benötigen eine Pufferbatterie, um bei ausgeschaltetem Computer weiterlaufen zu können (heute meist eine Knopfzelle)
- RTCs können Interrupts auslösen
- RTCs können für Alarme programmiert werden
- (manche) RTCs können Rechteckfrequenzen erzeugen und ausgeben
- (manche) RTCs können periodische Interrupts ausgeben
- nur eine Uhr pro System erforderlich
- (manche) RTCs können Datum und Wochentag berechnen, auch für Schaltjahre
- fast alle Uhren arbeiten mit einem 32,768 kHz-Quarz
  32768 = 2¹⁵
- 
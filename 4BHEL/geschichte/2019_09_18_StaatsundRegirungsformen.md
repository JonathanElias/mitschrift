# Staats- und Regirungsformen eigene Definitionen
| Staats- und Regirungsform            | Definition                                                   |
| ------------------------------------ | ------------------------------------------------------------ |
| Republik                             | Ein Staat der den Bürgern dient und nicht einem Monarchen    |
| Diktatur                             | Eine Person hat unbegrenzte Macht und Freiheiten sind eingeschränkt. |
| Bundesstaat                          | Ein Staat der aus mehreren kleinern in manchen Teilen unabhängigen Gliederstaaten besteht |
| Rechtsstaat                          | Ein Staat in dem sich alle (auch der Staat) an die Gesetze halten muss |
| Polizeistaat                         | Ein Staat in dem Die Polizei sehr viel Kontrolle über die BürgerInnen ausübt |
| Monarchie                            | Ein System in dem es einen Monarchen gibt. Wird nicht von den BürgerInnen gewählt. |
| Demokratie                           | Ein System in dem die BürgerInnen die Entscheidungen direkt oder indirekt treffen |
| Direkte Demokratie                   | Entscheidungen werden von den BürgerInnen durch Volksabstimmungen direkt getroffen |
| Indirekte/ Repräsentative Demokratie | BürgerInnen wählen Leute die sie in einem Parlament räpresentieren. |
| Bundesverfassung                     | Die wichtigsten Gesetze der Republik                         |
| Gewaltenteilung                      | Ein Staat in dem die Macht auf unabhängige Gewalten aufgeteilt ist |


# Kirchengeschichte im 13. Jahrhundert

- `Nationalstaat: Staat, dessen Bürger [überwiegend] einer Nation angehören` 
- `Pontifikat: Amtszeit von 1 Papst oder Bischof`
- `Laterankonzil: Konzil in Lateran (Teil von Rom)`
- `Konzil: Treffen von hohen Kirchenleuten zur Entscheidungsfindung`
- `Serenissima: Republik Venedig`
- `paläologen: europäische herrscherfamilie`
- `Albigenserkreuzzug: Kreuzzug von Paps Innozenz III. gegen die "Ketzer" Katharer (Glaubensgemeinschaft in Südfrankreich)`
- `Häresie: Lehre die im Widerspruch zu den kirchlichen Glaubensgrundsätzen steht`
- `Inquisition: juristisches Verfahren gegen Kirchengegner`
- `Stauferherrschaft: Herrschaft der Staufer Familie`
- `Diözesanklerus: Klerus einer Diözese`
- `Brabant: NL`
- `Beginen: Leute die ein religiöses Leben außerhalb eines Ordens führen (religiöser als die meisten Menschen)`
- `Vita apostolica: christliches Lebensideal`
- `Scholastik: Versuch Philosophie der Antike und Biblische Botschaft in Übereinstimmung zu bringen`
- `transsubsatiation: Wein»Blut; Brot»Leib`


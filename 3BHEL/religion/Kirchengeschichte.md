# Kirchengeschichte im 13. Jahrhundert

- Beginn: höhe der weltlichen Macht
- Macht nicht durch eigene Ressource sondern durch Einfluss auf Einzelstaaten
- Einfluss wird kleiner durch die Gründung der Nationalstaaten Frankreich und England
- Das Selbe auch im kleineren im Deutschen Reich, aber mit Fürstentümern (zu Lasten der Zentralgewalt und der Kirche)

## Laterankonzil

- Innozenz der 3. beruft am ende seines Pontifikates das 4. Laterankonzil ein
- Es zeigt sich, dass Europa in einem Lehensverhältnis zum Vatikan steht
  - Trotzdem ist die päpstliche Macht begrenzt (zB: Trotz päpstlichem Einspruch hat der 4 Kreuzzug stattgefunden (geführt von der Republik Venedig))
- Laien wird als Minimum die Osterbeichte und Osterkommunion vergeschrieben
- Weltliche Obrigkeit wird zur Zusammenarbeit bei der Häretikerverfolgung verpfichtet
- Dogma der Transsubstantiation  (Wein»Blut; Brot»Leib) wird definiert

## Kreuzzüge

- Nach zB. Kinderkreuzzug: (Kinder und Jugendliche machen sich auf den Weg nach Israel aber werden auf den Weg in die Sklaverei verkauft) geht der Kreuzzuggedanke welcher die Christenheit vereint zuende

## 

- Friedrich I. widersetzt sich einer Schwächung der kaiserlichen Zentralgewalt
- Friedrich II. schafft es nicht mehr sich zu widersetzten und die Zentralgewalt wird geschwächt
- Fürsten machten sich dann mehr und mehr unabhängig vom Vatikan

## Orden

- Städte wachsen
- Kirche ist seelsorgerischen Aufgaben nicht gewachsen
- bisherigen Orden können die Lücke nicht füllen da die Mönche außerhalb der Stätte leben
- 1215: Innozenz 3. bestätigt die Franziskaner
- 1216: Honorius 3. bestätigt die Dominikaner
- Später entstehen weibliche Zweige
- Bewegung der Beginen breitet sich aus: Leute die ein Leben ähnlich wie in einem Orden führen wollen, aber die Klausur ablehnen (Klausur: das abgeschlossene leben der Mönche)



- Nicht nur durch Predigt und einer vita apostolica versucht Kircher der Häresie Herr zu werden » Gregor IX. ordnet Inquiositionsverfahren neu an
- Scholastik erreicht ihre Blüte
- Kardinäle versuche die Kirche mit Cölestin V. zu enpolitisieren
- Cölestin V. dankt ab
- Bonifaz VIII. beschleunigt Macht und Ansehensverlust durch Strapazierung der Autorität seines Amtes


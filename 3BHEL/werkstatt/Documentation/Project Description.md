# Project Description

The aim of the Keyboard++ project was to build a compact keyboard with a layout similar to a Poker 2. But unlike most keyboards using the Poker 2 layout, the Keyboard++ has some additional features like the built in three port USB 2.0 hub and volume dial, hence the ++.

(picture of poker 2 layout)

But not only does the Keyboard++ have some convenient features, it is all housed in a case made out of aluminum, beech wood and clear acrylic. 


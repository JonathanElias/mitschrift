# Voulume Dial 

The Keyboard++ has a rotary encoder built in to control the PCs volume, but integrating the dial directly into the keyboard matrix would be very difficult due to the rotary encoder used as the volume dial relying on time difference between to impulses to differentiate between counterclockwise and clockwise rotation. To get around this problem, the Keyboard++ has an integrated rotary encoder decoder. 

## How it works

(bild von den phasenverschobenen rotary encoder)

The rotary encoder's first pulse pulls the other pin of the rotary encoder to 0V. If then both pins are on 5V an AND Gate's output pin goes to 5V and unlocks the first pin's signal to go to a resistor to "push" a button in the keyboard matrix.

# Keyboard Controller

The keyboard matrix is read out by an ATMega32u4 micro controller, which was chosen because it is supported by the QMK firmware, it`s native USB support and it being used in the Arduino Micro, giving us an  easy way of prototyping the firmware. 

# USB Hub Chip

The GL850G USB 2.0 4 port hub was chosen because of it`s low price at about  € 0,25 per chip.


# Instruction Manual

## USB hub

The Keyboard++ has a built in 3 port USB hub. The USB Hub operates in bus powered mode, which means that each port can only supply up to 100 mA. You can use most devices with the built in USB hub, this includes most USB mice (gaming mice with lights generally don't work with the USB hub), USB flash drives, and many more. While many devices work with the built in hub, external HDDs without a separate power supply usually don't work. Charging your phone the the USB ports is not recommended.

# Volume control

The Keyboard++ also has a built in volume control. Turning the knob clockwise increases the volume on the PC and turning it counterclockwise decreases the volume. The knob can also be pushed down, which toggles play/pause.

## USB-C ports

The Keyboard++ has two USB-C ports on the back. For normal operation, only the one on the right has to be connected to a USB port on the PC. If only the right one is connected, the Built in RGB LEDs, which illuminate the desk, will not work. This is due to the relatively high power consumption of the RGB LEDs. If the left USB-C port is connected to a smart phone power supply with an output current of at least 1A (most phone power supplies can deliver 1A), the RGB LEDs will light up. 

## RGB LEDs

The RGB LEDs can be controlled by the Keyboard++'s keys. By pushing the FN and Q key, the RGB LEDs can be turned on and off and by pushing the FN and W key, it changes the animation mode.


# Basic Concept

It was important to us, that the Keyboard++ could at least work as a USB keyboard with Cherry MX Switches. The USB Hub and volume dial were a lower priority, due to them not being necessary in the keyboard's operation. 

The Keyboard++'s case had to at least be a 3D printed case, but the goal was a case built out of wood to give the keyboard a more premium feel. Furthermore we wanted the keyboard to connect to the PC via at least a micro USB kable, but the goal was to use a USB C connector to be future proof.


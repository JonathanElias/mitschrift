# test presentation

## Roman

- introduction of different parts of the keyboard++ 
  - Button Matrix
  - keyboard micro controller
  - usb hub
  - volume control
- button matrix
  - place  for 63 cherry mx switches

## leitinger

- introduction to qmk firmware
  - modifications done to the qmk firmware
    - layout
    - LEDs
    - power consumtion
- keyboard micro controller
  - Atmega 32u4
  - qmk firmware flashed onto the atmega 32u4
  - connected to the usb hub

# bogensperger

- usb hub
  - uses gl850g usb 2.0 hub
  - 4 port
  - one port for µcontroller
- rotary encoder board
  - rotary encoder is difficult to read in button matrix
  - simple circuit to one signal per direction instead of 2 time delayed signals
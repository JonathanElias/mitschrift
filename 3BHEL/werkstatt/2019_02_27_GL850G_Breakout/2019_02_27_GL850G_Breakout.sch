EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_PIC18:PIC18F23K20_ISS U1
U 1 1 5C768931
P 5800 3200
F 0 "U1" H 5800 4378 50  0000 C CNN
F 1 "PIC18F23K20_ISS" H 5800 4287 50  0000 C CNN
F 2 "Package_SO:SSOP-28_5.3x10.2mm_P0.65mm" H 5300 2700 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001303H.pdf" H 5800 3150 50  0001 C CNN
	1    5800 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J2
U 1 1 5C7689F9
P 7550 3150
F 0 "J2" H 7630 3142 50  0000 L CNN
F 1 "Conn_01x14" H 7630 3051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x14_P2.54mm_Vertical" H 7550 3150 50  0001 C CNN
F 3 "~" H 7550 3150 50  0001 C CNN
	1    7550 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J1
U 1 1 5C768A3B
P 4100 3250
F 0 "J1" H 4020 2325 50  0000 C CNN
F 1 "Conn_01x14" H 4020 2416 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x14_P2.54mm_Vertical" H 4100 3250 50  0001 C CNN
F 3 "~" H 4100 3250 50  0001 C CNN
	1    4100 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 3100 4350 3100
Wire Wire Line
	4350 3100 4350 2550
Wire Wire Line
	4350 2550 4300 2550
Wire Wire Line
	5000 2400 4400 2400
Wire Wire Line
	4400 2400 4400 2650
Wire Wire Line
	4400 2650 4300 2650
Wire Wire Line
	5000 2500 4450 2500
Wire Wire Line
	4450 2500 4450 2750
Wire Wire Line
	4450 2750 4300 2750
Wire Wire Line
	5000 2600 4500 2600
Wire Wire Line
	4500 2600 4500 2850
Wire Wire Line
	4500 2850 4300 2850
Wire Wire Line
	5000 2700 4550 2700
Wire Wire Line
	4550 2700 4550 2950
Wire Wire Line
	4550 2950 4300 2950
Wire Wire Line
	5000 2800 4600 2800
Wire Wire Line
	4600 2800 4600 3050
Wire Wire Line
	4600 3050 4300 3050
Wire Wire Line
	5000 2900 4650 2900
Wire Wire Line
	4650 2900 4650 3150
Wire Wire Line
	4650 3150 4300 3150
Wire Wire Line
	5600 4200 5600 4250
Wire Wire Line
	5600 4250 4950 4250
Wire Wire Line
	4950 4250 4950 3250
Wire Wire Line
	4950 3250 4300 3250
Wire Wire Line
	5000 3400 4850 3400
Wire Wire Line
	4850 3400 4850 3350
Wire Wire Line
	4850 3350 4300 3350
Wire Wire Line
	5000 3300 4350 3300
Wire Wire Line
	4350 3300 4350 3450
Wire Wire Line
	4350 3450 4300 3450
Wire Wire Line
	6600 3600 6650 3600
Wire Wire Line
	6650 3600 6650 4300
Wire Wire Line
	4350 4300 4350 3850
Wire Wire Line
	4350 3850 4300 3850
Wire Wire Line
	6650 4300 4350 4300
Wire Wire Line
	6600 3500 6700 3500
Wire Wire Line
	6700 3500 6700 4350
Wire Wire Line
	6700 4350 4400 4350
Wire Wire Line
	4400 4350 4400 3750
Wire Wire Line
	4400 3750 4300 3750
Wire Wire Line
	4300 3650 4450 3650
Wire Wire Line
	4450 3650 4450 4400
Wire Wire Line
	4450 4400 6750 4400
Wire Wire Line
	6750 4400 6750 3400
Wire Wire Line
	6750 3400 6600 3400
Wire Wire Line
	6600 3300 6800 3300
Wire Wire Line
	6800 3300 6800 4450
Wire Wire Line
	6800 4450 4500 4450
Wire Wire Line
	4500 4450 4500 3550
Wire Wire Line
	4500 3550 4300 3550
Wire Wire Line
	6600 3700 7300 3700
Wire Wire Line
	7300 3700 7300 3850
Wire Wire Line
	7300 3850 7350 3850
Wire Wire Line
	6600 3800 7250 3800
Wire Wire Line
	7250 3800 7250 3750
Wire Wire Line
	7250 3750 7350 3750
Wire Wire Line
	6600 3900 7200 3900
Wire Wire Line
	7200 3900 7200 3650
Wire Wire Line
	7200 3650 7350 3650
Wire Wire Line
	6600 4000 7150 4000
Wire Wire Line
	7150 4000 7150 3550
Wire Wire Line
	7150 3550 7350 3550
Wire Wire Line
	5700 4200 5700 4250
Wire Wire Line
	5700 4250 7100 4250
Wire Wire Line
	7100 4250 7100 3450
Wire Wire Line
	7100 3450 7350 3450
Wire Wire Line
	7350 3350 7300 3350
Wire Wire Line
	7300 2150 5700 2150
Wire Wire Line
	5700 2150 5700 2200
Wire Wire Line
	7300 2150 7300 3350
Wire Wire Line
	7350 3250 6650 3250
Wire Wire Line
	6650 3250 6650 2400
Wire Wire Line
	6650 2400 6600 2400
Wire Wire Line
	7350 3150 6700 3150
Wire Wire Line
	6700 3150 6700 2500
Wire Wire Line
	6700 2500 6600 2500
Wire Wire Line
	7350 3050 6750 3050
Wire Wire Line
	6750 3050 6750 2600
Wire Wire Line
	6750 2600 6600 2600
Wire Wire Line
	7350 2950 6800 2950
Wire Wire Line
	6800 2950 6800 2700
Wire Wire Line
	6800 2700 6600 2700
Wire Wire Line
	6600 2800 6850 2800
Wire Wire Line
	6850 2800 6850 2850
Wire Wire Line
	6850 2850 7350 2850
Wire Wire Line
	6600 2900 7250 2900
Wire Wire Line
	7250 2900 7250 2750
Wire Wire Line
	7250 2750 7350 2750
Wire Wire Line
	6600 3000 7200 3000
Wire Wire Line
	7200 3000 7200 2650
Wire Wire Line
	7200 2650 7350 2650
Wire Wire Line
	6600 3100 7150 3100
Wire Wire Line
	7150 3100 7150 2550
Wire Wire Line
	7150 2550 7350 2550
$EndSCHEMATC

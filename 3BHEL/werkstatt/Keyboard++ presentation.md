# Keyboard++ presentation

## Volume Dial

- hard to integrate into keymatrix in QMK firmware
- can be pushed down like a button

- uses rotary encoder
- gives 2 time delayed HIGH pulses
- first pulse pulls down the second signal
- when both are HIGH AND Gate goes to HIGH and the first pulse get's passed on

## USB hub

- uses a 4 port USB 2.0 hub
- inexpensive 25c

## prototype controller

- atmega 32u4 
- native usb support
- enough pins for our needs
- on arduino micro to simplify prototyping

## Hardware we built

- prototype
- version 2
- 
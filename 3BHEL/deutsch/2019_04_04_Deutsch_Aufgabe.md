# Deutsch Aufgabe

## Für ein Verfahren zur Amtsenthebung reicht es wohl nicht 

1) In dem Kommentar schreibt der Autor über die Aussagen von Trumps ehemaligen Anwalt Michael Cohen vor dem Kongress. 

2) Obwohl der Autor auch Aussagen von Cohen direkt übernimmt ist er auch kritisch gegenüber Cohen. Die meiste Kritik in dem Kommentar ist aber gegenüber Präsident Trump. Der Ton des Textes ist eher agressiver.



## Schule: Lernen um der Noten willen?

1) In dem Kommentar schreibt der Autor über die Abschaffung der alternativen Leistungsbeurteilung. Er sagt, dass es schwer ist das, was ein Kind gelernt hat, mit Noten von Eins bis Fünf zu bewerten. 

2) Auffällig ist, dass der Autor am Anfang über sein Enkelkind spricht. Der Ton des Artikels ist genervt, aber der Autor bleibt trotzdem ruhig, aber gegen Ende des Artikels sieht man aber, dass er mit dem Plan die individuelle Leistungsbeurteilung abzuschaffen überhaupt nicht einverstanden ist.



## Er treibt den Schund auf die Spitze

1) In dem Artikel geht es um den Debütroman vom Ärtze-Schagzeuger Bela B Felsenheimer. 

2) Der Autor macht sich durchgehend über den Roman lustig und verwendet sehr viel Sarkasmus um den Lesern klar zu machen, dass der Roman ziemlich schlecht ist. Im Kommentar bemerkt man, dass der Autor genervt ist, dass so ein schlechter Roman so viel Aufmerksamkeit bekommt. 



## Stippvisite vor großer Kulisse

1) In dem Artikel schreibt der Autor über David Guetta und seine sehr monotonen Beats. 

2) Der Autor kritisiert, dass so eine Musikrichtung so viel Aufmerksamkeit bekommt.
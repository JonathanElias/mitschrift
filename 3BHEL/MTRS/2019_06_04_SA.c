#include <stdio.h>
#include <math.h>

int main(){
    do{
	double input_number = 1234; // input_number als grosse zahl festlegen damit das while nicht uebersprungen wird
	// Aufloesung veraenderbar
	int bits = 4;
	while(input_number >= pow(2, bits) || input_number < 0)
	{
		printf("\nAnaloge Zahl eingeben:\n");
        	scanf("%lf", &input_number);
		// ueberprueft of die zahl zwischen 0 und 16 ist
        	if(input_number >= pow(2, bits))
        	{
        		printf("Zahl ist zu gross. [0;%lf[ eingeben\n", pow(2, bits));
        	}
        	else if(input_number < 0)
        	{
            		printf("Zahl ist zu klein. [0;%lf[ eingeben\n", pow(2, bits));
        	}
	}
	
	int i, reference_number = pow(2, bits) / 2; // reference_number wird auf 2^bits/2 gesetzt (bei 4 bits zb 8) 
	for(i = 0; i<bits; i++)
	{
	    if(input_number<reference_number) // eingegebene zahl wird mit reference_number verglichen
	    {
		// wenn eingegebene Zahl kleiner als reference_number wird 2^(bits-2-i) von der reference number abgezogen
		printf("eingegebene zahl: %lf ist kleiner als reference_number: %d\n", input_number, reference_number);
	        reference_number = reference_number - pow(2, (bits-2-i));
		printf("Neue reference_number: %d\n\n", reference_number);
	    }
	    else
	    {
		// wenn eingegebene Zahl groesser als reference_number wird 2^(bits-2-i) zu der reference number addiert
		printf("eingegebene zahl: %lf ist groesser als reference_number: %d\n", input_number, reference_number);
	    	reference_number = reference_number + pow(2, (bits-2-i));
		printf("Neue reference_number: %d\n\n", reference_number);
	    }
	}
	printf("Zahl ist (digital): %d", reference_number);
	}while(1);
}

/* Beispiel warum 2^(bits-2-i):
bits = 4
input_number = 5.3

1. for schleifen durchgang
5.3 < 8 --> 8 - 2^4-2-0 = 8 - 2^2 = 4
wir wissen input_number ist zwischen 0 und 8

2. for schleifen durchgang
5.3 > 4 --> 4 + 2^4-2-1 = 4 + 2^1 = 6
wir wissen input_number ist zwischen 4 und 8

3. for schleifen durchgang
5.3 < 6 --> 6 + 2^4-2-2 = 6 - 2^0 = 5
wir wissen input number ist zwischen 4 und 6

4. scheifen durchgang
5.3 > 5 -->  5 + 2^4-2-3 = 5 + 2^-1 = 5.5 (reference number ist aber int also 5)
*/ 

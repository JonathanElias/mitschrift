# CPU

## Anschlüsse

- Busleitungen: Daten-, Aderss-, Steuerbus

- Clock: Quartz oder Oszilatorbaustein

  ​	![Quartzschaltsymbol](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Crystal-oscillator-IEC-Symbol.svg/220px-Crystal-oscillator-IEC-Symbol.svg.png)

  ​	(außen: Metallisch Beläge; innen: Kristall (Schwing fähig))

  ​	Referenzfrequenz für internen Oszillator (PLL)

- Reset: CPU in definierten Zustand setzen

  - "Power on Reset", Resetgenerator mit RC-Schaltung

## ALU (Arithmetic Logic Unit)

- Kombinatorische Logik: Addition, Verschiebeoperationen, Bool'sche Operationen
- Speicherzellen: Register "Akkumulator"


# 1 Wire Bus

(von Dallas Semiconductor)

![One Wire](./Fotos/2019_04_08_OneWire.jpg)

- Struktur: Single Master/Multi Slave
- seriell, asynchron
- halbduplex
- Versorgung der Slaves wahlweie über V+ oder Data ("parasite")
- Kabellänge: ca. 100m (aktive Pullupwiderstände bis ca. 300m)
- Datenrate: 16 kB/s
- Spannungen: 5V/3.3V !!!

## Parasite Supply

![](./Fotos/2019_04_08_parasite_supply.jpg)

![startbit](./Fotos/2019_04_08_startbit.jpg)

Adressierung indiviuelle ROM-Adressen für jedes Gerät (64 Bit: 8bit "family code" 48 Bit ID+Prüfnummer)
# Systembus

- unidirektional (Adressbus)
- bidirektional (Datenbus)

![](./Fotos/2019_03_11_Schema.png)

- Funktionseinheiten parallel am Bus!
- nur ein Sender zu einem bestimmten Zeitpunk, Rest auf Empfang

![Sender Busbetrieb](./Fotos/2019_03_11_SenderBusbetrieb.jpg)

## Eingänge

- CMOS-Eingänge müssen immer auf einem definierten Potential liegen (VDD oder GND)

### Pullupwiderstand

![](./Fotos/2019_03_11_Pullup.png)

### Pulldownwiderstand

duh...

Pullup/down -widerstände können auch am Chip integriert sein (programmierbar)

Ein- und Ausgangpin kombinierbar (I/O Pin)



![](./Fotos/2019_03_11_IOPin.png)

- Active High CS (deaktiviert bei zb 5V)
- Active Low CS nicht (deaktiviert bei zu 0V)
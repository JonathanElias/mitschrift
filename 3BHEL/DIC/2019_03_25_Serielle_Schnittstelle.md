# Serielle Schnittstelle

## Shift register

![shift register](./Fotos/2019_03_25_shift_register.jpg)

Grundprinzip Parallel/Seriell-Wandler

![Serielle Verbindung](./Fotos/2019_03_25_Serial_Verbingung.jpg)

Koppelung mittel Vollduplex-Verbindung (Sender und Empfänger gleichzeitig)

Gleiches Protokoll und gleiche CLK Frequenz

## Historie:

Großrechner über eine serielle verbindung mit einem Terminal (Datenendgerät) verbunden

- Terminal Programm (zB: Putty in Windos)
- Schnittstellen unter Unix:
  - tty0 (tty = teletype)
  - ⋮
  - ttyx

## RS232 Standard

### Spannungspegel

![Rs232](./Fotos/2019_03_25_RS232.jpg)

### Handshake 

Steuerinformationen (bereit, fertig,...)

- Software: Spezielle Bytes (Xon | Xoff)
- Hardware Steuerleitungen (CTS, RTS, DRT)
- Stecker/Buchsen: 9- oder 25-ploig

Verbindung zwischen Microcontroller und Embedded Systems (zB: Raspberry Pi)

### Spannungspegel

RS232: +15V/-15V

TTL: 0V/5V

## Prinzip digitaler Eingang IC

ESD-Schutzschaltung

![Digitaler Eingang](./Fotos/2019_03_25_Digitaler_Eingang.jpg)

## UART (Universal Asynchronous Receiver transmitter)

Asynchronous: keine gemeinsame CLK-Leitung zwischen Sender und Empfänger

Übereinkunft: Gleiche Bitrate

![UART](./Fotos/2019_03_25_UART.jpg)

Rahmen (Frame) mit 11 Bits

CLK-Synchronisation innerhalb eines Bytes




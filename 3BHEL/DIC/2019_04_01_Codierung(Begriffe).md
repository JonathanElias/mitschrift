# Halbduplex vs Vollduplex

## Halbduplex

- Funkgeräte (Nur ein Kanal belegt)

## Vollduplex

- Mobiltelefon (Up-/Downlink)
- UART/RS232 (TxD, RxD)

Verbindung zweier gleicher Geräte: RxD/TxD ausgekreuzt ("Nullmodemkabel")

# Codierung (Begriffe)

![NOZ](./Fotos/2019_04_01_NOZ.jpg)

NRZ (non return to zero)

![RZ](./Fotos/2019_04_01_RZ.jpg)

RZ (return to zero)

Wenn viele 1er hintereinander kommen, ist das ein ungünstiges Bitmuster, falls relative Koppelelemente im System verwendet werden (Relative Koppelelemente: Kondensatoren, Übertrager)

## USB (Universal Serial Bus)

Leitungen:

- 2*Datenleitunge (D+, D- Differenziell (geringe Störanfälligkeit))
- 2*Versorgungsleitungen (5V, GND)

Codierung Datenleitungen: NRZ-1 (inverted)

![USB Codierung](./Fotos/2019_04_01_USB_Codierung.jpg)

Zustandswechsel bei 1

USB: asynchrone Übertragung

Vergleich: Synchron

![Synchron](./Fotos/2019_04_01_Synchron_Struktur.jpg)

Struktur: kein Bus im eigentlichen Sinn

Sterntopologie:

![Sterntopolgie](./Fotos/2019_04_01Sterntopologie.jpg)

Anmeldung am Host 

### Standarts

USB 1.1: 

- Low Speed 1,5MBit/s (Maus, Tastertur (HID: Human Interface Device))
- Full Speed 12MBit/s (Audio, Modems)

USB 2.0:

- 480MBit/s
- Übertragungsmodi:
  - Isochroner Transfer
  - Bulk Modus

USB 3.0:

- 5GBit/s

- abwärtskompatibel zu USB 2.0

- vollduplex: 2 zusätzliche Datenadernpaare

  SSRx+	SSTx+

  SSRx-	SSTx-

  Rx...Empfang

  Tx...Senden